## 开发环境：
	IntelliJ IDEA 2017.3.5
	JDK 1.8
	maven 3.3.9
	数据库 oracle 11g XE

## 下载后测试步骤：
bos_management 中的 cn.itcast.bos.web.action.base 包下有个 TestController.java文件，里面对应有四个方法，分别对应着测试四个部分，启动成功后，直接在浏览器地址栏输入方法上面的“测试路径”即可测试，如果都没有问题，表示项目搭建成功，可以开始写代码了。

## 一点小细节：
提交代码时，如果用的是idea，不要提交 .idea 文件夹里面的文件，以 .iml 格式结尾的文件也不要提交；如果用的是eclipse系列的开发工具，不要提交 .settings 文件夹里面的文件， .project 和 .classpath 文件也不要提交。这些提交时应该忽略的文件已经都写在.gitignore文件中了，理论上讲，提交时会自动忽略，我们只需要注意下就行，如果默认勾选上了，我们就把勾去掉。