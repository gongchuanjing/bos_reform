package cn.itcast.bos.domain.base;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class ArchiveExample {
    protected String orderByClause;

    protected boolean distinct;

    protected List<Criteria> oredCriteria;

    public ArchiveExample() {
        oredCriteria = new ArrayList<Criteria>();
    }

    public void setOrderByClause(String orderByClause) {
        this.orderByClause = orderByClause;
    }

    public String getOrderByClause() {
        return orderByClause;
    }

    public void setDistinct(boolean distinct) {
        this.distinct = distinct;
    }

    public boolean isDistinct() {
        return distinct;
    }

    public List<Criteria> getOredCriteria() {
        return oredCriteria;
    }

    public void or(Criteria criteria) {
        oredCriteria.add(criteria);
    }

    public Criteria or() {
        Criteria criteria = createCriteriaInternal();
        oredCriteria.add(criteria);
        return criteria;
    }

    public Criteria createCriteria() {
        Criteria criteria = createCriteriaInternal();
        if (oredCriteria.size() == 0) {
            oredCriteria.add(criteria);
        }
        return criteria;
    }

    protected Criteria createCriteriaInternal() {
        Criteria criteria = new Criteria();
        return criteria;
    }

    public void clear() {
        oredCriteria.clear();
        orderByClause = null;
        distinct = false;
    }

    protected abstract static class GeneratedCriteria {
        protected List<Criterion> criteria;

        protected GeneratedCriteria() {
            super();
            criteria = new ArrayList<Criterion>();
        }

        public boolean isValid() {
            return criteria.size() > 0;
        }

        public List<Criterion> getAllCriteria() {
            return criteria;
        }

        public List<Criterion> getCriteria() {
            return criteria;
        }

        protected void addCriterion(String condition) {
            if (condition == null) {
                throw new RuntimeException("Value for condition cannot be null");
            }
            criteria.add(new Criterion(condition));
        }

        protected void addCriterion(String condition, Object value, String property) {
            if (value == null) {
                throw new RuntimeException("Value for " + property + " cannot be null");
            }
            criteria.add(new Criterion(condition, value));
        }

        protected void addCriterion(String condition, Object value1, Object value2, String property) {
            if (value1 == null || value2 == null) {
                throw new RuntimeException("Between values for " + property + " cannot be null");
            }
            criteria.add(new Criterion(condition, value1, value2));
        }

        public Criteria andIdIsNull() {
            addCriterion("C_ID is null");
            return (Criteria) this;
        }

        public Criteria andIdIsNotNull() {
            addCriterion("C_ID is not null");
            return (Criteria) this;
        }

        public Criteria andIdEqualTo(Integer value) {
            addCriterion("C_ID =", value, "id");
            return (Criteria) this;
        }

        public Criteria andIdNotEqualTo(Integer value) {
            addCriterion("C_ID <>", value, "id");
            return (Criteria) this;
        }

        public Criteria andIdGreaterThan(Integer value) {
            addCriterion("C_ID >", value, "id");
            return (Criteria) this;
        }

        public Criteria andIdGreaterThanOrEqualTo(Integer value) {
            addCriterion("C_ID >=", value, "id");
            return (Criteria) this;
        }

        public Criteria andIdLessThan(Integer value) {
            addCriterion("C_ID <", value, "id");
            return (Criteria) this;
        }

        public Criteria andIdLessThanOrEqualTo(Integer value) {
            addCriterion("C_ID <=", value, "id");
            return (Criteria) this;
        }

        public Criteria andIdIn(List<Integer> values) {
            addCriterion("C_ID in", values, "id");
            return (Criteria) this;
        }

        public Criteria andIdNotIn(List<Integer> values) {
            addCriterion("C_ID not in", values, "id");
            return (Criteria) this;
        }

        public Criteria andIdBetween(Integer value1, Integer value2) {
            addCriterion("C_ID between", value1, value2, "id");
            return (Criteria) this;
        }

        public Criteria andIdNotBetween(Integer value1, Integer value2) {
            addCriterion("C_ID not between", value1, value2, "id");
            return (Criteria) this;
        }

        public Criteria andArchiveNameIsNull() {
            addCriterion("C_ARCHIVE_NAME is null");
            return (Criteria) this;
        }

        public Criteria andArchiveNameIsNotNull() {
            addCriterion("C_ARCHIVE_NAME is not null");
            return (Criteria) this;
        }

        public Criteria andArchiveNameEqualTo(String value) {
            addCriterion("C_ARCHIVE_NAME =", value, "archiveName");
            return (Criteria) this;
        }

        public Criteria andArchiveNameNotEqualTo(String value) {
            addCriterion("C_ARCHIVE_NAME <>", value, "archiveName");
            return (Criteria) this;
        }

        public Criteria andArchiveNameGreaterThan(String value) {
            addCriterion("C_ARCHIVE_NAME >", value, "archiveName");
            return (Criteria) this;
        }

        public Criteria andArchiveNameGreaterThanOrEqualTo(String value) {
            addCriterion("C_ARCHIVE_NAME >=", value, "archiveName");
            return (Criteria) this;
        }

        public Criteria andArchiveNameLessThan(String value) {
            addCriterion("C_ARCHIVE_NAME <", value, "archiveName");
            return (Criteria) this;
        }

        public Criteria andArchiveNameLessThanOrEqualTo(String value) {
            addCriterion("C_ARCHIVE_NAME <=", value, "archiveName");
            return (Criteria) this;
        }

        public Criteria andArchiveNameLike(String value) {
            addCriterion("C_ARCHIVE_NAME like", value, "archiveName");
            return (Criteria) this;
        }

        public Criteria andArchiveNameNotLike(String value) {
            addCriterion("C_ARCHIVE_NAME not like", value, "archiveName");
            return (Criteria) this;
        }

        public Criteria andArchiveNameIn(List<String> values) {
            addCriterion("C_ARCHIVE_NAME in", values, "archiveName");
            return (Criteria) this;
        }

        public Criteria andArchiveNameNotIn(List<String> values) {
            addCriterion("C_ARCHIVE_NAME not in", values, "archiveName");
            return (Criteria) this;
        }

        public Criteria andArchiveNameBetween(String value1, String value2) {
            addCriterion("C_ARCHIVE_NAME between", value1, value2, "archiveName");
            return (Criteria) this;
        }

        public Criteria andArchiveNameNotBetween(String value1, String value2) {
            addCriterion("C_ARCHIVE_NAME not between", value1, value2, "archiveName");
            return (Criteria) this;
        }

        public Criteria andArchiveNumIsNull() {
            addCriterion("C_ARCHIVE_NUM is null");
            return (Criteria) this;
        }

        public Criteria andArchiveNumIsNotNull() {
            addCriterion("C_ARCHIVE_NUM is not null");
            return (Criteria) this;
        }

        public Criteria andArchiveNumEqualTo(String value) {
            addCriterion("C_ARCHIVE_NUM =", value, "archiveNum");
            return (Criteria) this;
        }

        public Criteria andArchiveNumNotEqualTo(String value) {
            addCriterion("C_ARCHIVE_NUM <>", value, "archiveNum");
            return (Criteria) this;
        }

        public Criteria andArchiveNumGreaterThan(String value) {
            addCriterion("C_ARCHIVE_NUM >", value, "archiveNum");
            return (Criteria) this;
        }

        public Criteria andArchiveNumGreaterThanOrEqualTo(String value) {
            addCriterion("C_ARCHIVE_NUM >=", value, "archiveNum");
            return (Criteria) this;
        }

        public Criteria andArchiveNumLessThan(String value) {
            addCriterion("C_ARCHIVE_NUM <", value, "archiveNum");
            return (Criteria) this;
        }

        public Criteria andArchiveNumLessThanOrEqualTo(String value) {
            addCriterion("C_ARCHIVE_NUM <=", value, "archiveNum");
            return (Criteria) this;
        }

        public Criteria andArchiveNumLike(String value) {
            addCriterion("C_ARCHIVE_NUM like", value, "archiveNum");
            return (Criteria) this;
        }

        public Criteria andArchiveNumNotLike(String value) {
            addCriterion("C_ARCHIVE_NUM not like", value, "archiveNum");
            return (Criteria) this;
        }

        public Criteria andArchiveNumIn(List<String> values) {
            addCriterion("C_ARCHIVE_NUM in", values, "archiveNum");
            return (Criteria) this;
        }

        public Criteria andArchiveNumNotIn(List<String> values) {
            addCriterion("C_ARCHIVE_NUM not in", values, "archiveNum");
            return (Criteria) this;
        }

        public Criteria andArchiveNumBetween(String value1, String value2) {
            addCriterion("C_ARCHIVE_NUM between", value1, value2, "archiveNum");
            return (Criteria) this;
        }

        public Criteria andArchiveNumNotBetween(String value1, String value2) {
            addCriterion("C_ARCHIVE_NUM not between", value1, value2, "archiveNum");
            return (Criteria) this;
        }

        public Criteria andHaschildIsNull() {
            addCriterion("C_HASCHILD is null");
            return (Criteria) this;
        }

        public Criteria andHaschildIsNotNull() {
            addCriterion("C_HASCHILD is not null");
            return (Criteria) this;
        }

        public Criteria andHaschildEqualTo(Integer value) {
            addCriterion("C_HASCHILD =", value, "haschild");
            return (Criteria) this;
        }

        public Criteria andHaschildNotEqualTo(Integer value) {
            addCriterion("C_HASCHILD <>", value, "haschild");
            return (Criteria) this;
        }

        public Criteria andHaschildGreaterThan(Integer value) {
            addCriterion("C_HASCHILD >", value, "haschild");
            return (Criteria) this;
        }

        public Criteria andHaschildGreaterThanOrEqualTo(Integer value) {
            addCriterion("C_HASCHILD >=", value, "haschild");
            return (Criteria) this;
        }

        public Criteria andHaschildLessThan(Integer value) {
            addCriterion("C_HASCHILD <", value, "haschild");
            return (Criteria) this;
        }

        public Criteria andHaschildLessThanOrEqualTo(Integer value) {
            addCriterion("C_HASCHILD <=", value, "haschild");
            return (Criteria) this;
        }

        public Criteria andHaschildIn(List<Integer> values) {
            addCriterion("C_HASCHILD in", values, "haschild");
            return (Criteria) this;
        }

        public Criteria andHaschildNotIn(List<Integer> values) {
            addCriterion("C_HASCHILD not in", values, "haschild");
            return (Criteria) this;
        }

        public Criteria andHaschildBetween(Integer value1, Integer value2) {
            addCriterion("C_HASCHILD between", value1, value2, "haschild");
            return (Criteria) this;
        }

        public Criteria andHaschildNotBetween(Integer value1, Integer value2) {
            addCriterion("C_HASCHILD not between", value1, value2, "haschild");
            return (Criteria) this;
        }

        public Criteria andOperatingCompanyIsNull() {
            addCriterion("C_OPERATING_COMPANY is null");
            return (Criteria) this;
        }

        public Criteria andOperatingCompanyIsNotNull() {
            addCriterion("C_OPERATING_COMPANY is not null");
            return (Criteria) this;
        }

        public Criteria andOperatingCompanyEqualTo(String value) {
            addCriterion("C_OPERATING_COMPANY =", value, "operatingCompany");
            return (Criteria) this;
        }

        public Criteria andOperatingCompanyNotEqualTo(String value) {
            addCriterion("C_OPERATING_COMPANY <>", value, "operatingCompany");
            return (Criteria) this;
        }

        public Criteria andOperatingCompanyGreaterThan(String value) {
            addCriterion("C_OPERATING_COMPANY >", value, "operatingCompany");
            return (Criteria) this;
        }

        public Criteria andOperatingCompanyGreaterThanOrEqualTo(String value) {
            addCriterion("C_OPERATING_COMPANY >=", value, "operatingCompany");
            return (Criteria) this;
        }

        public Criteria andOperatingCompanyLessThan(String value) {
            addCriterion("C_OPERATING_COMPANY <", value, "operatingCompany");
            return (Criteria) this;
        }

        public Criteria andOperatingCompanyLessThanOrEqualTo(String value) {
            addCriterion("C_OPERATING_COMPANY <=", value, "operatingCompany");
            return (Criteria) this;
        }

        public Criteria andOperatingCompanyLike(String value) {
            addCriterion("C_OPERATING_COMPANY like", value, "operatingCompany");
            return (Criteria) this;
        }

        public Criteria andOperatingCompanyNotLike(String value) {
            addCriterion("C_OPERATING_COMPANY not like", value, "operatingCompany");
            return (Criteria) this;
        }

        public Criteria andOperatingCompanyIn(List<String> values) {
            addCriterion("C_OPERATING_COMPANY in", values, "operatingCompany");
            return (Criteria) this;
        }

        public Criteria andOperatingCompanyNotIn(List<String> values) {
            addCriterion("C_OPERATING_COMPANY not in", values, "operatingCompany");
            return (Criteria) this;
        }

        public Criteria andOperatingCompanyBetween(String value1, String value2) {
            addCriterion("C_OPERATING_COMPANY between", value1, value2, "operatingCompany");
            return (Criteria) this;
        }

        public Criteria andOperatingCompanyNotBetween(String value1, String value2) {
            addCriterion("C_OPERATING_COMPANY not between", value1, value2, "operatingCompany");
            return (Criteria) this;
        }

        public Criteria andOperatingTimeIsNull() {
            addCriterion("C_OPERATING_TIME is null");
            return (Criteria) this;
        }

        public Criteria andOperatingTimeIsNotNull() {
            addCriterion("C_OPERATING_TIME is not null");
            return (Criteria) this;
        }

        public Criteria andOperatingTimeEqualTo(Date value) {
            addCriterion("C_OPERATING_TIME =", value, "operatingTime");
            return (Criteria) this;
        }

        public Criteria andOperatingTimeNotEqualTo(Date value) {
            addCriterion("C_OPERATING_TIME <>", value, "operatingTime");
            return (Criteria) this;
        }

        public Criteria andOperatingTimeGreaterThan(Date value) {
            addCriterion("C_OPERATING_TIME >", value, "operatingTime");
            return (Criteria) this;
        }

        public Criteria andOperatingTimeGreaterThanOrEqualTo(Date value) {
            addCriterion("C_OPERATING_TIME >=", value, "operatingTime");
            return (Criteria) this;
        }

        public Criteria andOperatingTimeLessThan(Date value) {
            addCriterion("C_OPERATING_TIME <", value, "operatingTime");
            return (Criteria) this;
        }

        public Criteria andOperatingTimeLessThanOrEqualTo(Date value) {
            addCriterion("C_OPERATING_TIME <=", value, "operatingTime");
            return (Criteria) this;
        }

        public Criteria andOperatingTimeIn(List<Date> values) {
            addCriterion("C_OPERATING_TIME in", values, "operatingTime");
            return (Criteria) this;
        }

        public Criteria andOperatingTimeNotIn(List<Date> values) {
            addCriterion("C_OPERATING_TIME not in", values, "operatingTime");
            return (Criteria) this;
        }

        public Criteria andOperatingTimeBetween(Date value1, Date value2) {
            addCriterion("C_OPERATING_TIME between", value1, value2, "operatingTime");
            return (Criteria) this;
        }

        public Criteria andOperatingTimeNotBetween(Date value1, Date value2) {
            addCriterion("C_OPERATING_TIME not between", value1, value2, "operatingTime");
            return (Criteria) this;
        }

        public Criteria andOperatorIsNull() {
            addCriterion("C_OPERATOR is null");
            return (Criteria) this;
        }

        public Criteria andOperatorIsNotNull() {
            addCriterion("C_OPERATOR is not null");
            return (Criteria) this;
        }

        public Criteria andOperatorEqualTo(String value) {
            addCriterion("C_OPERATOR =", value, "operator");
            return (Criteria) this;
        }

        public Criteria andOperatorNotEqualTo(String value) {
            addCriterion("C_OPERATOR <>", value, "operator");
            return (Criteria) this;
        }

        public Criteria andOperatorGreaterThan(String value) {
            addCriterion("C_OPERATOR >", value, "operator");
            return (Criteria) this;
        }

        public Criteria andOperatorGreaterThanOrEqualTo(String value) {
            addCriterion("C_OPERATOR >=", value, "operator");
            return (Criteria) this;
        }

        public Criteria andOperatorLessThan(String value) {
            addCriterion("C_OPERATOR <", value, "operator");
            return (Criteria) this;
        }

        public Criteria andOperatorLessThanOrEqualTo(String value) {
            addCriterion("C_OPERATOR <=", value, "operator");
            return (Criteria) this;
        }

        public Criteria andOperatorLike(String value) {
            addCriterion("C_OPERATOR like", value, "operator");
            return (Criteria) this;
        }

        public Criteria andOperatorNotLike(String value) {
            addCriterion("C_OPERATOR not like", value, "operator");
            return (Criteria) this;
        }

        public Criteria andOperatorIn(List<String> values) {
            addCriterion("C_OPERATOR in", values, "operator");
            return (Criteria) this;
        }

        public Criteria andOperatorNotIn(List<String> values) {
            addCriterion("C_OPERATOR not in", values, "operator");
            return (Criteria) this;
        }

        public Criteria andOperatorBetween(String value1, String value2) {
            addCriterion("C_OPERATOR between", value1, value2, "operator");
            return (Criteria) this;
        }

        public Criteria andOperatorNotBetween(String value1, String value2) {
            addCriterion("C_OPERATOR not between", value1, value2, "operator");
            return (Criteria) this;
        }

        public Criteria andRemarkIsNull() {
            addCriterion("C_REMARK is null");
            return (Criteria) this;
        }

        public Criteria andRemarkIsNotNull() {
            addCriterion("C_REMARK is not null");
            return (Criteria) this;
        }

        public Criteria andRemarkEqualTo(String value) {
            addCriterion("C_REMARK =", value, "remark");
            return (Criteria) this;
        }

        public Criteria andRemarkNotEqualTo(String value) {
            addCriterion("C_REMARK <>", value, "remark");
            return (Criteria) this;
        }

        public Criteria andRemarkGreaterThan(String value) {
            addCriterion("C_REMARK >", value, "remark");
            return (Criteria) this;
        }

        public Criteria andRemarkGreaterThanOrEqualTo(String value) {
            addCriterion("C_REMARK >=", value, "remark");
            return (Criteria) this;
        }

        public Criteria andRemarkLessThan(String value) {
            addCriterion("C_REMARK <", value, "remark");
            return (Criteria) this;
        }

        public Criteria andRemarkLessThanOrEqualTo(String value) {
            addCriterion("C_REMARK <=", value, "remark");
            return (Criteria) this;
        }

        public Criteria andRemarkLike(String value) {
            addCriterion("C_REMARK like", value, "remark");
            return (Criteria) this;
        }

        public Criteria andRemarkNotLike(String value) {
            addCriterion("C_REMARK not like", value, "remark");
            return (Criteria) this;
        }

        public Criteria andRemarkIn(List<String> values) {
            addCriterion("C_REMARK in", values, "remark");
            return (Criteria) this;
        }

        public Criteria andRemarkNotIn(List<String> values) {
            addCriterion("C_REMARK not in", values, "remark");
            return (Criteria) this;
        }

        public Criteria andRemarkBetween(String value1, String value2) {
            addCriterion("C_REMARK between", value1, value2, "remark");
            return (Criteria) this;
        }

        public Criteria andRemarkNotBetween(String value1, String value2) {
            addCriterion("C_REMARK not between", value1, value2, "remark");
            return (Criteria) this;
        }
    }

    public static class Criteria extends GeneratedCriteria {

        protected Criteria() {
            super();
        }
    }

    public static class Criterion {
        private String condition;

        private Object value;

        private Object secondValue;

        private boolean noValue;

        private boolean singleValue;

        private boolean betweenValue;

        private boolean listValue;

        private String typeHandler;

        public String getCondition() {
            return condition;
        }

        public Object getValue() {
            return value;
        }

        public Object getSecondValue() {
            return secondValue;
        }

        public boolean isNoValue() {
            return noValue;
        }

        public boolean isSingleValue() {
            return singleValue;
        }

        public boolean isBetweenValue() {
            return betweenValue;
        }

        public boolean isListValue() {
            return listValue;
        }

        public String getTypeHandler() {
            return typeHandler;
        }

        protected Criterion(String condition) {
            super();
            this.condition = condition;
            this.typeHandler = null;
            this.noValue = true;
        }

        protected Criterion(String condition, Object value, String typeHandler) {
            super();
            this.condition = condition;
            this.value = value;
            this.typeHandler = typeHandler;
            if (value instanceof List<?>) {
                this.listValue = true;
            } else {
                this.singleValue = true;
            }
        }

        protected Criterion(String condition, Object value) {
            this(condition, value, null);
        }

        protected Criterion(String condition, Object value, Object secondValue, String typeHandler) {
            super();
            this.condition = condition;
            this.value = value;
            this.secondValue = secondValue;
            this.typeHandler = typeHandler;
            this.betweenValue = true;
        }

        protected Criterion(String condition, Object value, Object secondValue) {
            this(condition, value, secondValue, null);
        }
    }
}