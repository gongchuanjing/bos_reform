package cn.itcast.bos.domain.base;

import java.util.ArrayList;
import java.util.List;

public class CourierExample {
    protected String orderByClause;

    protected boolean distinct;

    protected List<Criteria> oredCriteria;

    public CourierExample() {
        oredCriteria = new ArrayList<Criteria>();
    }

    public void setOrderByClause(String orderByClause) {
        this.orderByClause = orderByClause;
    }

    public String getOrderByClause() {
        return orderByClause;
    }

    public void setDistinct(boolean distinct) {
        this.distinct = distinct;
    }

    public boolean isDistinct() {
        return distinct;
    }

    public List<Criteria> getOredCriteria() {
        return oredCriteria;
    }

    public void or(Criteria criteria) {
        oredCriteria.add(criteria);
    }

    public Criteria or() {
        Criteria criteria = createCriteriaInternal();
        oredCriteria.add(criteria);
        return criteria;
    }

    public Criteria createCriteria() {
        Criteria criteria = createCriteriaInternal();
        if (oredCriteria.size() == 0) {
            oredCriteria.add(criteria);
        }
        return criteria;
    }

    protected Criteria createCriteriaInternal() {
        Criteria criteria = new Criteria();
        return criteria;
    }

    public void clear() {
        oredCriteria.clear();
        orderByClause = null;
        distinct = false;
    }

    protected abstract static class GeneratedCriteria {
        protected List<Criterion> criteria;

        protected GeneratedCriteria() {
            super();
            criteria = new ArrayList<Criterion>();
        }

        public boolean isValid() {
            return criteria.size() > 0;
        }

        public List<Criterion> getAllCriteria() {
            return criteria;
        }

        public List<Criterion> getCriteria() {
            return criteria;
        }

        protected void addCriterion(String condition) {
            if (condition == null) {
                throw new RuntimeException("Value for condition cannot be null");
            }
            criteria.add(new Criterion(condition));
        }

        protected void addCriterion(String condition, Object value, String property) {
            if (value == null) {
                throw new RuntimeException("Value for " + property + " cannot be null");
            }
            criteria.add(new Criterion(condition, value));
        }

        protected void addCriterion(String condition, Object value1, Object value2, String property) {
            if (value1 == null || value2 == null) {
                throw new RuntimeException("Between values for " + property + " cannot be null");
            }
            criteria.add(new Criterion(condition, value1, value2));
        }

        public Criteria andIdIsNull() {
            addCriterion("C_ID is null");
            return (Criteria) this;
        }

        public Criteria andIdIsNotNull() {
            addCriterion("C_ID is not null");
            return (Criteria) this;
        }

        public Criteria andIdEqualTo(Integer value) {
            addCriterion("C_ID =", value, "id");
            return (Criteria) this;
        }

        public Criteria andIdNotEqualTo(Integer value) {
            addCriterion("C_ID <>", value, "id");
            return (Criteria) this;
        }

        public Criteria andIdGreaterThan(Integer value) {
            addCriterion("C_ID >", value, "id");
            return (Criteria) this;
        }

        public Criteria andIdGreaterThanOrEqualTo(Integer value) {
            addCriterion("C_ID >=", value, "id");
            return (Criteria) this;
        }

        public Criteria andIdLessThan(Integer value) {
            addCriterion("C_ID <", value, "id");
            return (Criteria) this;
        }

        public Criteria andIdLessThanOrEqualTo(Integer value) {
            addCriterion("C_ID <=", value, "id");
            return (Criteria) this;
        }

        public Criteria andIdIn(List<Integer> values) {
            addCriterion("C_ID in", values, "id");
            return (Criteria) this;
        }

        public Criteria andIdNotIn(List<Integer> values) {
            addCriterion("C_ID not in", values, "id");
            return (Criteria) this;
        }

        public Criteria andIdBetween(Integer value1, Integer value2) {
            addCriterion("C_ID between", value1, value2, "id");
            return (Criteria) this;
        }

        public Criteria andIdNotBetween(Integer value1, Integer value2) {
            addCriterion("C_ID not between", value1, value2, "id");
            return (Criteria) this;
        }

        public Criteria andCheckPwdIsNull() {
            addCriterion("C_CHECK_PWD is null");
            return (Criteria) this;
        }

        public Criteria andCheckPwdIsNotNull() {
            addCriterion("C_CHECK_PWD is not null");
            return (Criteria) this;
        }

        public Criteria andCheckPwdEqualTo(String value) {
            addCriterion("C_CHECK_PWD =", value, "checkPwd");
            return (Criteria) this;
        }

        public Criteria andCheckPwdNotEqualTo(String value) {
            addCriterion("C_CHECK_PWD <>", value, "checkPwd");
            return (Criteria) this;
        }

        public Criteria andCheckPwdGreaterThan(String value) {
            addCriterion("C_CHECK_PWD >", value, "checkPwd");
            return (Criteria) this;
        }

        public Criteria andCheckPwdGreaterThanOrEqualTo(String value) {
            addCriterion("C_CHECK_PWD >=", value, "checkPwd");
            return (Criteria) this;
        }

        public Criteria andCheckPwdLessThan(String value) {
            addCriterion("C_CHECK_PWD <", value, "checkPwd");
            return (Criteria) this;
        }

        public Criteria andCheckPwdLessThanOrEqualTo(String value) {
            addCriterion("C_CHECK_PWD <=", value, "checkPwd");
            return (Criteria) this;
        }

        public Criteria andCheckPwdLike(String value) {
            addCriterion("C_CHECK_PWD like", value, "checkPwd");
            return (Criteria) this;
        }

        public Criteria andCheckPwdNotLike(String value) {
            addCriterion("C_CHECK_PWD not like", value, "checkPwd");
            return (Criteria) this;
        }

        public Criteria andCheckPwdIn(List<String> values) {
            addCriterion("C_CHECK_PWD in", values, "checkPwd");
            return (Criteria) this;
        }

        public Criteria andCheckPwdNotIn(List<String> values) {
            addCriterion("C_CHECK_PWD not in", values, "checkPwd");
            return (Criteria) this;
        }

        public Criteria andCheckPwdBetween(String value1, String value2) {
            addCriterion("C_CHECK_PWD between", value1, value2, "checkPwd");
            return (Criteria) this;
        }

        public Criteria andCheckPwdNotBetween(String value1, String value2) {
            addCriterion("C_CHECK_PWD not between", value1, value2, "checkPwd");
            return (Criteria) this;
        }

        public Criteria andCompanyIsNull() {
            addCriterion("C_COMPANY is null");
            return (Criteria) this;
        }

        public Criteria andCompanyIsNotNull() {
            addCriterion("C_COMPANY is not null");
            return (Criteria) this;
        }

        public Criteria andCompanyEqualTo(String value) {
            addCriterion("C_COMPANY =", value, "company");
            return (Criteria) this;
        }

        public Criteria andCompanyNotEqualTo(String value) {
            addCriterion("C_COMPANY <>", value, "company");
            return (Criteria) this;
        }

        public Criteria andCompanyGreaterThan(String value) {
            addCriterion("C_COMPANY >", value, "company");
            return (Criteria) this;
        }

        public Criteria andCompanyGreaterThanOrEqualTo(String value) {
            addCriterion("C_COMPANY >=", value, "company");
            return (Criteria) this;
        }

        public Criteria andCompanyLessThan(String value) {
            addCriterion("C_COMPANY <", value, "company");
            return (Criteria) this;
        }

        public Criteria andCompanyLessThanOrEqualTo(String value) {
            addCriterion("C_COMPANY <=", value, "company");
            return (Criteria) this;
        }

        public Criteria andCompanyLike(String value) {
            addCriterion("C_COMPANY like", value, "company");
            return (Criteria) this;
        }

        public Criteria andCompanyNotLike(String value) {
            addCriterion("C_COMPANY not like", value, "company");
            return (Criteria) this;
        }

        public Criteria andCompanyIn(List<String> values) {
            addCriterion("C_COMPANY in", values, "company");
            return (Criteria) this;
        }

        public Criteria andCompanyNotIn(List<String> values) {
            addCriterion("C_COMPANY not in", values, "company");
            return (Criteria) this;
        }

        public Criteria andCompanyBetween(String value1, String value2) {
            addCriterion("C_COMPANY between", value1, value2, "company");
            return (Criteria) this;
        }

        public Criteria andCompanyNotBetween(String value1, String value2) {
            addCriterion("C_COMPANY not between", value1, value2, "company");
            return (Criteria) this;
        }

        public Criteria andCourierNumIsNull() {
            addCriterion("C_COURIER_NUM is null");
            return (Criteria) this;
        }

        public Criteria andCourierNumIsNotNull() {
            addCriterion("C_COURIER_NUM is not null");
            return (Criteria) this;
        }

        public Criteria andCourierNumEqualTo(String value) {
            addCriterion("C_COURIER_NUM =", value, "courierNum");
            return (Criteria) this;
        }

        public Criteria andCourierNumNotEqualTo(String value) {
            addCriterion("C_COURIER_NUM <>", value, "courierNum");
            return (Criteria) this;
        }

        public Criteria andCourierNumGreaterThan(String value) {
            addCriterion("C_COURIER_NUM >", value, "courierNum");
            return (Criteria) this;
        }

        public Criteria andCourierNumGreaterThanOrEqualTo(String value) {
            addCriterion("C_COURIER_NUM >=", value, "courierNum");
            return (Criteria) this;
        }

        public Criteria andCourierNumLessThan(String value) {
            addCriterion("C_COURIER_NUM <", value, "courierNum");
            return (Criteria) this;
        }

        public Criteria andCourierNumLessThanOrEqualTo(String value) {
            addCriterion("C_COURIER_NUM <=", value, "courierNum");
            return (Criteria) this;
        }

        public Criteria andCourierNumLike(String value) {
            addCriterion("C_COURIER_NUM like", value, "courierNum");
            return (Criteria) this;
        }

        public Criteria andCourierNumNotLike(String value) {
            addCriterion("C_COURIER_NUM not like", value, "courierNum");
            return (Criteria) this;
        }

        public Criteria andCourierNumIn(List<String> values) {
            addCriterion("C_COURIER_NUM in", values, "courierNum");
            return (Criteria) this;
        }

        public Criteria andCourierNumNotIn(List<String> values) {
            addCriterion("C_COURIER_NUM not in", values, "courierNum");
            return (Criteria) this;
        }

        public Criteria andCourierNumBetween(String value1, String value2) {
            addCriterion("C_COURIER_NUM between", value1, value2, "courierNum");
            return (Criteria) this;
        }

        public Criteria andCourierNumNotBetween(String value1, String value2) {
            addCriterion("C_COURIER_NUM not between", value1, value2, "courierNum");
            return (Criteria) this;
        }

        public Criteria andDeltagIsNull() {
            addCriterion("C_DELTAG is null");
            return (Criteria) this;
        }

        public Criteria andDeltagIsNotNull() {
            addCriterion("C_DELTAG is not null");
            return (Criteria) this;
        }

        public Criteria andDeltagEqualTo(String value) {
            addCriterion("C_DELTAG =", value, "deltag");
            return (Criteria) this;
        }

        public Criteria andDeltagNotEqualTo(String value) {
            addCriterion("C_DELTAG <>", value, "deltag");
            return (Criteria) this;
        }

        public Criteria andDeltagGreaterThan(String value) {
            addCriterion("C_DELTAG >", value, "deltag");
            return (Criteria) this;
        }

        public Criteria andDeltagGreaterThanOrEqualTo(String value) {
            addCriterion("C_DELTAG >=", value, "deltag");
            return (Criteria) this;
        }

        public Criteria andDeltagLessThan(String value) {
            addCriterion("C_DELTAG <", value, "deltag");
            return (Criteria) this;
        }

        public Criteria andDeltagLessThanOrEqualTo(String value) {
            addCriterion("C_DELTAG <=", value, "deltag");
            return (Criteria) this;
        }

        public Criteria andDeltagLike(String value) {
            addCriterion("C_DELTAG like", value, "deltag");
            return (Criteria) this;
        }

        public Criteria andDeltagNotLike(String value) {
            addCriterion("C_DELTAG not like", value, "deltag");
            return (Criteria) this;
        }

        public Criteria andDeltagIn(List<String> values) {
            addCriterion("C_DELTAG in", values, "deltag");
            return (Criteria) this;
        }

        public Criteria andDeltagNotIn(List<String> values) {
            addCriterion("C_DELTAG not in", values, "deltag");
            return (Criteria) this;
        }

        public Criteria andDeltagBetween(String value1, String value2) {
            addCriterion("C_DELTAG between", value1, value2, "deltag");
            return (Criteria) this;
        }

        public Criteria andDeltagNotBetween(String value1, String value2) {
            addCriterion("C_DELTAG not between", value1, value2, "deltag");
            return (Criteria) this;
        }

        public Criteria andNameIsNull() {
            addCriterion("C_NAME is null");
            return (Criteria) this;
        }

        public Criteria andNameIsNotNull() {
            addCriterion("C_NAME is not null");
            return (Criteria) this;
        }

        public Criteria andNameEqualTo(String value) {
            addCriterion("C_NAME =", value, "name");
            return (Criteria) this;
        }

        public Criteria andNameNotEqualTo(String value) {
            addCriterion("C_NAME <>", value, "name");
            return (Criteria) this;
        }

        public Criteria andNameGreaterThan(String value) {
            addCriterion("C_NAME >", value, "name");
            return (Criteria) this;
        }

        public Criteria andNameGreaterThanOrEqualTo(String value) {
            addCriterion("C_NAME >=", value, "name");
            return (Criteria) this;
        }

        public Criteria andNameLessThan(String value) {
            addCriterion("C_NAME <", value, "name");
            return (Criteria) this;
        }

        public Criteria andNameLessThanOrEqualTo(String value) {
            addCriterion("C_NAME <=", value, "name");
            return (Criteria) this;
        }

        public Criteria andNameLike(String value) {
            addCriterion("C_NAME like", value, "name");
            return (Criteria) this;
        }

        public Criteria andNameNotLike(String value) {
            addCriterion("C_NAME not like", value, "name");
            return (Criteria) this;
        }

        public Criteria andNameIn(List<String> values) {
            addCriterion("C_NAME in", values, "name");
            return (Criteria) this;
        }

        public Criteria andNameNotIn(List<String> values) {
            addCriterion("C_NAME not in", values, "name");
            return (Criteria) this;
        }

        public Criteria andNameBetween(String value1, String value2) {
            addCriterion("C_NAME between", value1, value2, "name");
            return (Criteria) this;
        }

        public Criteria andNameNotBetween(String value1, String value2) {
            addCriterion("C_NAME not between", value1, value2, "name");
            return (Criteria) this;
        }

        public Criteria andPdaIsNull() {
            addCriterion("C_PDA is null");
            return (Criteria) this;
        }

        public Criteria andPdaIsNotNull() {
            addCriterion("C_PDA is not null");
            return (Criteria) this;
        }

        public Criteria andPdaEqualTo(String value) {
            addCriterion("C_PDA =", value, "pda");
            return (Criteria) this;
        }

        public Criteria andPdaNotEqualTo(String value) {
            addCriterion("C_PDA <>", value, "pda");
            return (Criteria) this;
        }

        public Criteria andPdaGreaterThan(String value) {
            addCriterion("C_PDA >", value, "pda");
            return (Criteria) this;
        }

        public Criteria andPdaGreaterThanOrEqualTo(String value) {
            addCriterion("C_PDA >=", value, "pda");
            return (Criteria) this;
        }

        public Criteria andPdaLessThan(String value) {
            addCriterion("C_PDA <", value, "pda");
            return (Criteria) this;
        }

        public Criteria andPdaLessThanOrEqualTo(String value) {
            addCriterion("C_PDA <=", value, "pda");
            return (Criteria) this;
        }

        public Criteria andPdaLike(String value) {
            addCriterion("C_PDA like", value, "pda");
            return (Criteria) this;
        }

        public Criteria andPdaNotLike(String value) {
            addCriterion("C_PDA not like", value, "pda");
            return (Criteria) this;
        }

        public Criteria andPdaIn(List<String> values) {
            addCriterion("C_PDA in", values, "pda");
            return (Criteria) this;
        }

        public Criteria andPdaNotIn(List<String> values) {
            addCriterion("C_PDA not in", values, "pda");
            return (Criteria) this;
        }

        public Criteria andPdaBetween(String value1, String value2) {
            addCriterion("C_PDA between", value1, value2, "pda");
            return (Criteria) this;
        }

        public Criteria andPdaNotBetween(String value1, String value2) {
            addCriterion("C_PDA not between", value1, value2, "pda");
            return (Criteria) this;
        }

        public Criteria andTelephoneIsNull() {
            addCriterion("C_TELEPHONE is null");
            return (Criteria) this;
        }

        public Criteria andTelephoneIsNotNull() {
            addCriterion("C_TELEPHONE is not null");
            return (Criteria) this;
        }

        public Criteria andTelephoneEqualTo(String value) {
            addCriterion("C_TELEPHONE =", value, "telephone");
            return (Criteria) this;
        }

        public Criteria andTelephoneNotEqualTo(String value) {
            addCriterion("C_TELEPHONE <>", value, "telephone");
            return (Criteria) this;
        }

        public Criteria andTelephoneGreaterThan(String value) {
            addCriterion("C_TELEPHONE >", value, "telephone");
            return (Criteria) this;
        }

        public Criteria andTelephoneGreaterThanOrEqualTo(String value) {
            addCriterion("C_TELEPHONE >=", value, "telephone");
            return (Criteria) this;
        }

        public Criteria andTelephoneLessThan(String value) {
            addCriterion("C_TELEPHONE <", value, "telephone");
            return (Criteria) this;
        }

        public Criteria andTelephoneLessThanOrEqualTo(String value) {
            addCriterion("C_TELEPHONE <=", value, "telephone");
            return (Criteria) this;
        }

        public Criteria andTelephoneLike(String value) {
            addCriterion("C_TELEPHONE like", value, "telephone");
            return (Criteria) this;
        }

        public Criteria andTelephoneNotLike(String value) {
            addCriterion("C_TELEPHONE not like", value, "telephone");
            return (Criteria) this;
        }

        public Criteria andTelephoneIn(List<String> values) {
            addCriterion("C_TELEPHONE in", values, "telephone");
            return (Criteria) this;
        }

        public Criteria andTelephoneNotIn(List<String> values) {
            addCriterion("C_TELEPHONE not in", values, "telephone");
            return (Criteria) this;
        }

        public Criteria andTelephoneBetween(String value1, String value2) {
            addCriterion("C_TELEPHONE between", value1, value2, "telephone");
            return (Criteria) this;
        }

        public Criteria andTelephoneNotBetween(String value1, String value2) {
            addCriterion("C_TELEPHONE not between", value1, value2, "telephone");
            return (Criteria) this;
        }

        public Criteria andTypeIsNull() {
            addCriterion("C_TYPE is null");
            return (Criteria) this;
        }

        public Criteria andTypeIsNotNull() {
            addCriterion("C_TYPE is not null");
            return (Criteria) this;
        }

        public Criteria andTypeEqualTo(String value) {
            addCriterion("C_TYPE =", value, "type");
            return (Criteria) this;
        }

        public Criteria andTypeNotEqualTo(String value) {
            addCriterion("C_TYPE <>", value, "type");
            return (Criteria) this;
        }

        public Criteria andTypeGreaterThan(String value) {
            addCriterion("C_TYPE >", value, "type");
            return (Criteria) this;
        }

        public Criteria andTypeGreaterThanOrEqualTo(String value) {
            addCriterion("C_TYPE >=", value, "type");
            return (Criteria) this;
        }

        public Criteria andTypeLessThan(String value) {
            addCriterion("C_TYPE <", value, "type");
            return (Criteria) this;
        }

        public Criteria andTypeLessThanOrEqualTo(String value) {
            addCriterion("C_TYPE <=", value, "type");
            return (Criteria) this;
        }

        public Criteria andTypeLike(String value) {
            addCriterion("C_TYPE like", value, "type");
            return (Criteria) this;
        }

        public Criteria andTypeNotLike(String value) {
            addCriterion("C_TYPE not like", value, "type");
            return (Criteria) this;
        }

        public Criteria andTypeIn(List<String> values) {
            addCriterion("C_TYPE in", values, "type");
            return (Criteria) this;
        }

        public Criteria andTypeNotIn(List<String> values) {
            addCriterion("C_TYPE not in", values, "type");
            return (Criteria) this;
        }

        public Criteria andTypeBetween(String value1, String value2) {
            addCriterion("C_TYPE between", value1, value2, "type");
            return (Criteria) this;
        }

        public Criteria andTypeNotBetween(String value1, String value2) {
            addCriterion("C_TYPE not between", value1, value2, "type");
            return (Criteria) this;
        }

        public Criteria andVehicleNumIsNull() {
            addCriterion("C_VEHICLE_NUM is null");
            return (Criteria) this;
        }

        public Criteria andVehicleNumIsNotNull() {
            addCriterion("C_VEHICLE_NUM is not null");
            return (Criteria) this;
        }

        public Criteria andVehicleNumEqualTo(String value) {
            addCriterion("C_VEHICLE_NUM =", value, "vehicleNum");
            return (Criteria) this;
        }

        public Criteria andVehicleNumNotEqualTo(String value) {
            addCriterion("C_VEHICLE_NUM <>", value, "vehicleNum");
            return (Criteria) this;
        }

        public Criteria andVehicleNumGreaterThan(String value) {
            addCriterion("C_VEHICLE_NUM >", value, "vehicleNum");
            return (Criteria) this;
        }

        public Criteria andVehicleNumGreaterThanOrEqualTo(String value) {
            addCriterion("C_VEHICLE_NUM >=", value, "vehicleNum");
            return (Criteria) this;
        }

        public Criteria andVehicleNumLessThan(String value) {
            addCriterion("C_VEHICLE_NUM <", value, "vehicleNum");
            return (Criteria) this;
        }

        public Criteria andVehicleNumLessThanOrEqualTo(String value) {
            addCriterion("C_VEHICLE_NUM <=", value, "vehicleNum");
            return (Criteria) this;
        }

        public Criteria andVehicleNumLike(String value) {
            addCriterion("C_VEHICLE_NUM like", value, "vehicleNum");
            return (Criteria) this;
        }

        public Criteria andVehicleNumNotLike(String value) {
            addCriterion("C_VEHICLE_NUM not like", value, "vehicleNum");
            return (Criteria) this;
        }

        public Criteria andVehicleNumIn(List<String> values) {
            addCriterion("C_VEHICLE_NUM in", values, "vehicleNum");
            return (Criteria) this;
        }

        public Criteria andVehicleNumNotIn(List<String> values) {
            addCriterion("C_VEHICLE_NUM not in", values, "vehicleNum");
            return (Criteria) this;
        }

        public Criteria andVehicleNumBetween(String value1, String value2) {
            addCriterion("C_VEHICLE_NUM between", value1, value2, "vehicleNum");
            return (Criteria) this;
        }

        public Criteria andVehicleNumNotBetween(String value1, String value2) {
            addCriterion("C_VEHICLE_NUM not between", value1, value2, "vehicleNum");
            return (Criteria) this;
        }

        public Criteria andVehicleTypeIsNull() {
            addCriterion("C_VEHICLE_TYPE is null");
            return (Criteria) this;
        }

        public Criteria andVehicleTypeIsNotNull() {
            addCriterion("C_VEHICLE_TYPE is not null");
            return (Criteria) this;
        }

        public Criteria andVehicleTypeEqualTo(String value) {
            addCriterion("C_VEHICLE_TYPE =", value, "vehicleType");
            return (Criteria) this;
        }

        public Criteria andVehicleTypeNotEqualTo(String value) {
            addCriterion("C_VEHICLE_TYPE <>", value, "vehicleType");
            return (Criteria) this;
        }

        public Criteria andVehicleTypeGreaterThan(String value) {
            addCriterion("C_VEHICLE_TYPE >", value, "vehicleType");
            return (Criteria) this;
        }

        public Criteria andVehicleTypeGreaterThanOrEqualTo(String value) {
            addCriterion("C_VEHICLE_TYPE >=", value, "vehicleType");
            return (Criteria) this;
        }

        public Criteria andVehicleTypeLessThan(String value) {
            addCriterion("C_VEHICLE_TYPE <", value, "vehicleType");
            return (Criteria) this;
        }

        public Criteria andVehicleTypeLessThanOrEqualTo(String value) {
            addCriterion("C_VEHICLE_TYPE <=", value, "vehicleType");
            return (Criteria) this;
        }

        public Criteria andVehicleTypeLike(String value) {
            addCriterion("C_VEHICLE_TYPE like", value, "vehicleType");
            return (Criteria) this;
        }

        public Criteria andVehicleTypeNotLike(String value) {
            addCriterion("C_VEHICLE_TYPE not like", value, "vehicleType");
            return (Criteria) this;
        }

        public Criteria andVehicleTypeIn(List<String> values) {
            addCriterion("C_VEHICLE_TYPE in", values, "vehicleType");
            return (Criteria) this;
        }

        public Criteria andVehicleTypeNotIn(List<String> values) {
            addCriterion("C_VEHICLE_TYPE not in", values, "vehicleType");
            return (Criteria) this;
        }

        public Criteria andVehicleTypeBetween(String value1, String value2) {
            addCriterion("C_VEHICLE_TYPE between", value1, value2, "vehicleType");
            return (Criteria) this;
        }

        public Criteria andVehicleTypeNotBetween(String value1, String value2) {
            addCriterion("C_VEHICLE_TYPE not between", value1, value2, "vehicleType");
            return (Criteria) this;
        }

        public Criteria andStandardIdIsNull() {
            addCriterion("C_STANDARD_ID is null");
            return (Criteria) this;
        }

        public Criteria andStandardIdIsNotNull() {
            addCriterion("C_STANDARD_ID is not null");
            return (Criteria) this;
        }

        public Criteria andStandardIdEqualTo(Integer value) {
            addCriterion("C_STANDARD_ID =", value, "standardId");
            return (Criteria) this;
        }

        public Criteria andStandardIdNotEqualTo(Integer value) {
            addCriterion("C_STANDARD_ID <>", value, "standardId");
            return (Criteria) this;
        }

        public Criteria andStandardIdGreaterThan(Integer value) {
            addCriterion("C_STANDARD_ID >", value, "standardId");
            return (Criteria) this;
        }

        public Criteria andStandardIdGreaterThanOrEqualTo(Integer value) {
            addCriterion("C_STANDARD_ID >=", value, "standardId");
            return (Criteria) this;
        }

        public Criteria andStandardIdLessThan(Integer value) {
            addCriterion("C_STANDARD_ID <", value, "standardId");
            return (Criteria) this;
        }

        public Criteria andStandardIdLessThanOrEqualTo(Integer value) {
            addCriterion("C_STANDARD_ID <=", value, "standardId");
            return (Criteria) this;
        }

        public Criteria andStandardIdIn(List<Integer> values) {
            addCriterion("C_STANDARD_ID in", values, "standardId");
            return (Criteria) this;
        }

        public Criteria andStandardIdNotIn(List<Integer> values) {
            addCriterion("C_STANDARD_ID not in", values, "standardId");
            return (Criteria) this;
        }

        public Criteria andStandardIdBetween(Integer value1, Integer value2) {
            addCriterion("C_STANDARD_ID between", value1, value2, "standardId");
            return (Criteria) this;
        }

        public Criteria andStandardIdNotBetween(Integer value1, Integer value2) {
            addCriterion("C_STANDARD_ID not between", value1, value2, "standardId");
            return (Criteria) this;
        }

        public Criteria andTaketimeIdIsNull() {
            addCriterion("C_TAKETIME_ID is null");
            return (Criteria) this;
        }

        public Criteria andTaketimeIdIsNotNull() {
            addCriterion("C_TAKETIME_ID is not null");
            return (Criteria) this;
        }

        public Criteria andTaketimeIdEqualTo(Integer value) {
            addCriterion("C_TAKETIME_ID =", value, "taketimeId");
            return (Criteria) this;
        }

        public Criteria andTaketimeIdNotEqualTo(Integer value) {
            addCriterion("C_TAKETIME_ID <>", value, "taketimeId");
            return (Criteria) this;
        }

        public Criteria andTaketimeIdGreaterThan(Integer value) {
            addCriterion("C_TAKETIME_ID >", value, "taketimeId");
            return (Criteria) this;
        }

        public Criteria andTaketimeIdGreaterThanOrEqualTo(Integer value) {
            addCriterion("C_TAKETIME_ID >=", value, "taketimeId");
            return (Criteria) this;
        }

        public Criteria andTaketimeIdLessThan(Integer value) {
            addCriterion("C_TAKETIME_ID <", value, "taketimeId");
            return (Criteria) this;
        }

        public Criteria andTaketimeIdLessThanOrEqualTo(Integer value) {
            addCriterion("C_TAKETIME_ID <=", value, "taketimeId");
            return (Criteria) this;
        }

        public Criteria andTaketimeIdIn(List<Integer> values) {
            addCriterion("C_TAKETIME_ID in", values, "taketimeId");
            return (Criteria) this;
        }

        public Criteria andTaketimeIdNotIn(List<Integer> values) {
            addCriterion("C_TAKETIME_ID not in", values, "taketimeId");
            return (Criteria) this;
        }

        public Criteria andTaketimeIdBetween(Integer value1, Integer value2) {
            addCriterion("C_TAKETIME_ID between", value1, value2, "taketimeId");
            return (Criteria) this;
        }

        public Criteria andTaketimeIdNotBetween(Integer value1, Integer value2) {
            addCriterion("C_TAKETIME_ID not between", value1, value2, "taketimeId");
            return (Criteria) this;
        }
    }

    public static class Criteria extends GeneratedCriteria {

        protected Criteria() {
            super();
        }
    }

    public static class Criterion {
        private String condition;

        private Object value;

        private Object secondValue;

        private boolean noValue;

        private boolean singleValue;

        private boolean betweenValue;

        private boolean listValue;

        private String typeHandler;

        public String getCondition() {
            return condition;
        }

        public Object getValue() {
            return value;
        }

        public Object getSecondValue() {
            return secondValue;
        }

        public boolean isNoValue() {
            return noValue;
        }

        public boolean isSingleValue() {
            return singleValue;
        }

        public boolean isBetweenValue() {
            return betweenValue;
        }

        public boolean isListValue() {
            return listValue;
        }

        public String getTypeHandler() {
            return typeHandler;
        }

        protected Criterion(String condition) {
            super();
            this.condition = condition;
            this.typeHandler = null;
            this.noValue = true;
        }

        protected Criterion(String condition, Object value, String typeHandler) {
            super();
            this.condition = condition;
            this.value = value;
            this.typeHandler = typeHandler;
            if (value instanceof List<?>) {
                this.listValue = true;
            } else {
                this.singleValue = true;
            }
        }

        protected Criterion(String condition, Object value) {
            this(condition, value, null);
        }

        protected Criterion(String condition, Object value, Object secondValue, String typeHandler) {
            super();
            this.condition = condition;
            this.value = value;
            this.secondValue = secondValue;
            this.typeHandler = typeHandler;
            this.betweenValue = true;
        }

        protected Criterion(String condition, Object value, Object secondValue) {
            this(condition, value, secondValue, null);
        }
    }
}