package cn.itcast.bos.domain.base;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class FixedAreaExample {
    protected String orderByClause;

    protected boolean distinct;

    protected List<Criteria> oredCriteria;

    public FixedAreaExample() {
        oredCriteria = new ArrayList<Criteria>();
    }

    public void setOrderByClause(String orderByClause) {
        this.orderByClause = orderByClause;
    }

    public String getOrderByClause() {
        return orderByClause;
    }

    public void setDistinct(boolean distinct) {
        this.distinct = distinct;
    }

    public boolean isDistinct() {
        return distinct;
    }

    public List<Criteria> getOredCriteria() {
        return oredCriteria;
    }

    public void or(Criteria criteria) {
        oredCriteria.add(criteria);
    }

    public Criteria or() {
        Criteria criteria = createCriteriaInternal();
        oredCriteria.add(criteria);
        return criteria;
    }

    public Criteria createCriteria() {
        Criteria criteria = createCriteriaInternal();
        if (oredCriteria.size() == 0) {
            oredCriteria.add(criteria);
        }
        return criteria;
    }

    protected Criteria createCriteriaInternal() {
        Criteria criteria = new Criteria();
        return criteria;
    }

    public void clear() {
        oredCriteria.clear();
        orderByClause = null;
        distinct = false;
    }

    protected abstract static class GeneratedCriteria {
        protected List<Criterion> criteria;

        protected GeneratedCriteria() {
            super();
            criteria = new ArrayList<Criterion>();
        }

        public boolean isValid() {
            return criteria.size() > 0;
        }

        public List<Criterion> getAllCriteria() {
            return criteria;
        }

        public List<Criterion> getCriteria() {
            return criteria;
        }

        protected void addCriterion(String condition) {
            if (condition == null) {
                throw new RuntimeException("Value for condition cannot be null");
            }
            criteria.add(new Criterion(condition));
        }

        protected void addCriterion(String condition, Object value, String property) {
            if (value == null) {
                throw new RuntimeException("Value for " + property + " cannot be null");
            }
            criteria.add(new Criterion(condition, value));
        }

        protected void addCriterion(String condition, Object value1, Object value2, String property) {
            if (value1 == null || value2 == null) {
                throw new RuntimeException("Between values for " + property + " cannot be null");
            }
            criteria.add(new Criterion(condition, value1, value2));
        }

        public Criteria andIdIsNull() {
            addCriterion("C_ID is null");
            return (Criteria) this;
        }

        public Criteria andIdIsNotNull() {
            addCriterion("C_ID is not null");
            return (Criteria) this;
        }

        public Criteria andIdEqualTo(String value) {
            addCriterion("C_ID =", value, "id");
            return (Criteria) this;
        }

        public Criteria andIdNotEqualTo(String value) {
            addCriterion("C_ID <>", value, "id");
            return (Criteria) this;
        }

        public Criteria andIdGreaterThan(String value) {
            addCriterion("C_ID >", value, "id");
            return (Criteria) this;
        }

        public Criteria andIdGreaterThanOrEqualTo(String value) {
            addCriterion("C_ID >=", value, "id");
            return (Criteria) this;
        }

        public Criteria andIdLessThan(String value) {
            addCriterion("C_ID <", value, "id");
            return (Criteria) this;
        }

        public Criteria andIdLessThanOrEqualTo(String value) {
            addCriterion("C_ID <=", value, "id");
            return (Criteria) this;
        }

        public Criteria andIdLike(String value) {
            addCriterion("C_ID like", value, "id");
            return (Criteria) this;
        }

        public Criteria andIdNotLike(String value) {
            addCriterion("C_ID not like", value, "id");
            return (Criteria) this;
        }

        public Criteria andIdIn(List<String> values) {
            addCriterion("C_ID in", values, "id");
            return (Criteria) this;
        }

        public Criteria andIdNotIn(List<String> values) {
            addCriterion("C_ID not in", values, "id");
            return (Criteria) this;
        }

        public Criteria andIdBetween(String value1, String value2) {
            addCriterion("C_ID between", value1, value2, "id");
            return (Criteria) this;
        }

        public Criteria andIdNotBetween(String value1, String value2) {
            addCriterion("C_ID not between", value1, value2, "id");
            return (Criteria) this;
        }

        public Criteria andCompanyIsNull() {
            addCriterion("C_COMPANY is null");
            return (Criteria) this;
        }

        public Criteria andCompanyIsNotNull() {
            addCriterion("C_COMPANY is not null");
            return (Criteria) this;
        }

        public Criteria andCompanyEqualTo(String value) {
            addCriterion("C_COMPANY =", value, "company");
            return (Criteria) this;
        }

        public Criteria andCompanyNotEqualTo(String value) {
            addCriterion("C_COMPANY <>", value, "company");
            return (Criteria) this;
        }

        public Criteria andCompanyGreaterThan(String value) {
            addCriterion("C_COMPANY >", value, "company");
            return (Criteria) this;
        }

        public Criteria andCompanyGreaterThanOrEqualTo(String value) {
            addCriterion("C_COMPANY >=", value, "company");
            return (Criteria) this;
        }

        public Criteria andCompanyLessThan(String value) {
            addCriterion("C_COMPANY <", value, "company");
            return (Criteria) this;
        }

        public Criteria andCompanyLessThanOrEqualTo(String value) {
            addCriterion("C_COMPANY <=", value, "company");
            return (Criteria) this;
        }

        public Criteria andCompanyLike(String value) {
            addCriterion("C_COMPANY like", value, "company");
            return (Criteria) this;
        }

        public Criteria andCompanyNotLike(String value) {
            addCriterion("C_COMPANY not like", value, "company");
            return (Criteria) this;
        }

        public Criteria andCompanyIn(List<String> values) {
            addCriterion("C_COMPANY in", values, "company");
            return (Criteria) this;
        }

        public Criteria andCompanyNotIn(List<String> values) {
            addCriterion("C_COMPANY not in", values, "company");
            return (Criteria) this;
        }

        public Criteria andCompanyBetween(String value1, String value2) {
            addCriterion("C_COMPANY between", value1, value2, "company");
            return (Criteria) this;
        }

        public Criteria andCompanyNotBetween(String value1, String value2) {
            addCriterion("C_COMPANY not between", value1, value2, "company");
            return (Criteria) this;
        }

        public Criteria andFixedAreaLeaderIsNull() {
            addCriterion("C_FIXED_AREA_LEADER is null");
            return (Criteria) this;
        }

        public Criteria andFixedAreaLeaderIsNotNull() {
            addCriterion("C_FIXED_AREA_LEADER is not null");
            return (Criteria) this;
        }

        public Criteria andFixedAreaLeaderEqualTo(String value) {
            addCriterion("C_FIXED_AREA_LEADER =", value, "fixedAreaLeader");
            return (Criteria) this;
        }

        public Criteria andFixedAreaLeaderNotEqualTo(String value) {
            addCriterion("C_FIXED_AREA_LEADER <>", value, "fixedAreaLeader");
            return (Criteria) this;
        }

        public Criteria andFixedAreaLeaderGreaterThan(String value) {
            addCriterion("C_FIXED_AREA_LEADER >", value, "fixedAreaLeader");
            return (Criteria) this;
        }

        public Criteria andFixedAreaLeaderGreaterThanOrEqualTo(String value) {
            addCriterion("C_FIXED_AREA_LEADER >=", value, "fixedAreaLeader");
            return (Criteria) this;
        }

        public Criteria andFixedAreaLeaderLessThan(String value) {
            addCriterion("C_FIXED_AREA_LEADER <", value, "fixedAreaLeader");
            return (Criteria) this;
        }

        public Criteria andFixedAreaLeaderLessThanOrEqualTo(String value) {
            addCriterion("C_FIXED_AREA_LEADER <=", value, "fixedAreaLeader");
            return (Criteria) this;
        }

        public Criteria andFixedAreaLeaderLike(String value) {
            addCriterion("C_FIXED_AREA_LEADER like", value, "fixedAreaLeader");
            return (Criteria) this;
        }

        public Criteria andFixedAreaLeaderNotLike(String value) {
            addCriterion("C_FIXED_AREA_LEADER not like", value, "fixedAreaLeader");
            return (Criteria) this;
        }

        public Criteria andFixedAreaLeaderIn(List<String> values) {
            addCriterion("C_FIXED_AREA_LEADER in", values, "fixedAreaLeader");
            return (Criteria) this;
        }

        public Criteria andFixedAreaLeaderNotIn(List<String> values) {
            addCriterion("C_FIXED_AREA_LEADER not in", values, "fixedAreaLeader");
            return (Criteria) this;
        }

        public Criteria andFixedAreaLeaderBetween(String value1, String value2) {
            addCriterion("C_FIXED_AREA_LEADER between", value1, value2, "fixedAreaLeader");
            return (Criteria) this;
        }

        public Criteria andFixedAreaLeaderNotBetween(String value1, String value2) {
            addCriterion("C_FIXED_AREA_LEADER not between", value1, value2, "fixedAreaLeader");
            return (Criteria) this;
        }

        public Criteria andFixedAreaNameIsNull() {
            addCriterion("C_FIXED_AREA_NAME is null");
            return (Criteria) this;
        }

        public Criteria andFixedAreaNameIsNotNull() {
            addCriterion("C_FIXED_AREA_NAME is not null");
            return (Criteria) this;
        }

        public Criteria andFixedAreaNameEqualTo(String value) {
            addCriterion("C_FIXED_AREA_NAME =", value, "fixedAreaName");
            return (Criteria) this;
        }

        public Criteria andFixedAreaNameNotEqualTo(String value) {
            addCriterion("C_FIXED_AREA_NAME <>", value, "fixedAreaName");
            return (Criteria) this;
        }

        public Criteria andFixedAreaNameGreaterThan(String value) {
            addCriterion("C_FIXED_AREA_NAME >", value, "fixedAreaName");
            return (Criteria) this;
        }

        public Criteria andFixedAreaNameGreaterThanOrEqualTo(String value) {
            addCriterion("C_FIXED_AREA_NAME >=", value, "fixedAreaName");
            return (Criteria) this;
        }

        public Criteria andFixedAreaNameLessThan(String value) {
            addCriterion("C_FIXED_AREA_NAME <", value, "fixedAreaName");
            return (Criteria) this;
        }

        public Criteria andFixedAreaNameLessThanOrEqualTo(String value) {
            addCriterion("C_FIXED_AREA_NAME <=", value, "fixedAreaName");
            return (Criteria) this;
        }

        public Criteria andFixedAreaNameLike(String value) {
            addCriterion("C_FIXED_AREA_NAME like", value, "fixedAreaName");
            return (Criteria) this;
        }

        public Criteria andFixedAreaNameNotLike(String value) {
            addCriterion("C_FIXED_AREA_NAME not like", value, "fixedAreaName");
            return (Criteria) this;
        }

        public Criteria andFixedAreaNameIn(List<String> values) {
            addCriterion("C_FIXED_AREA_NAME in", values, "fixedAreaName");
            return (Criteria) this;
        }

        public Criteria andFixedAreaNameNotIn(List<String> values) {
            addCriterion("C_FIXED_AREA_NAME not in", values, "fixedAreaName");
            return (Criteria) this;
        }

        public Criteria andFixedAreaNameBetween(String value1, String value2) {
            addCriterion("C_FIXED_AREA_NAME between", value1, value2, "fixedAreaName");
            return (Criteria) this;
        }

        public Criteria andFixedAreaNameNotBetween(String value1, String value2) {
            addCriterion("C_FIXED_AREA_NAME not between", value1, value2, "fixedAreaName");
            return (Criteria) this;
        }

        public Criteria andOperatingCompanyIsNull() {
            addCriterion("C_OPERATING_COMPANY is null");
            return (Criteria) this;
        }

        public Criteria andOperatingCompanyIsNotNull() {
            addCriterion("C_OPERATING_COMPANY is not null");
            return (Criteria) this;
        }

        public Criteria andOperatingCompanyEqualTo(String value) {
            addCriterion("C_OPERATING_COMPANY =", value, "operatingCompany");
            return (Criteria) this;
        }

        public Criteria andOperatingCompanyNotEqualTo(String value) {
            addCriterion("C_OPERATING_COMPANY <>", value, "operatingCompany");
            return (Criteria) this;
        }

        public Criteria andOperatingCompanyGreaterThan(String value) {
            addCriterion("C_OPERATING_COMPANY >", value, "operatingCompany");
            return (Criteria) this;
        }

        public Criteria andOperatingCompanyGreaterThanOrEqualTo(String value) {
            addCriterion("C_OPERATING_COMPANY >=", value, "operatingCompany");
            return (Criteria) this;
        }

        public Criteria andOperatingCompanyLessThan(String value) {
            addCriterion("C_OPERATING_COMPANY <", value, "operatingCompany");
            return (Criteria) this;
        }

        public Criteria andOperatingCompanyLessThanOrEqualTo(String value) {
            addCriterion("C_OPERATING_COMPANY <=", value, "operatingCompany");
            return (Criteria) this;
        }

        public Criteria andOperatingCompanyLike(String value) {
            addCriterion("C_OPERATING_COMPANY like", value, "operatingCompany");
            return (Criteria) this;
        }

        public Criteria andOperatingCompanyNotLike(String value) {
            addCriterion("C_OPERATING_COMPANY not like", value, "operatingCompany");
            return (Criteria) this;
        }

        public Criteria andOperatingCompanyIn(List<String> values) {
            addCriterion("C_OPERATING_COMPANY in", values, "operatingCompany");
            return (Criteria) this;
        }

        public Criteria andOperatingCompanyNotIn(List<String> values) {
            addCriterion("C_OPERATING_COMPANY not in", values, "operatingCompany");
            return (Criteria) this;
        }

        public Criteria andOperatingCompanyBetween(String value1, String value2) {
            addCriterion("C_OPERATING_COMPANY between", value1, value2, "operatingCompany");
            return (Criteria) this;
        }

        public Criteria andOperatingCompanyNotBetween(String value1, String value2) {
            addCriterion("C_OPERATING_COMPANY not between", value1, value2, "operatingCompany");
            return (Criteria) this;
        }

        public Criteria andOperatingTimeIsNull() {
            addCriterion("C_OPERATING_TIME is null");
            return (Criteria) this;
        }

        public Criteria andOperatingTimeIsNotNull() {
            addCriterion("C_OPERATING_TIME is not null");
            return (Criteria) this;
        }

        public Criteria andOperatingTimeEqualTo(Date value) {
            addCriterion("C_OPERATING_TIME =", value, "operatingTime");
            return (Criteria) this;
        }

        public Criteria andOperatingTimeNotEqualTo(Date value) {
            addCriterion("C_OPERATING_TIME <>", value, "operatingTime");
            return (Criteria) this;
        }

        public Criteria andOperatingTimeGreaterThan(Date value) {
            addCriterion("C_OPERATING_TIME >", value, "operatingTime");
            return (Criteria) this;
        }

        public Criteria andOperatingTimeGreaterThanOrEqualTo(Date value) {
            addCriterion("C_OPERATING_TIME >=", value, "operatingTime");
            return (Criteria) this;
        }

        public Criteria andOperatingTimeLessThan(Date value) {
            addCriterion("C_OPERATING_TIME <", value, "operatingTime");
            return (Criteria) this;
        }

        public Criteria andOperatingTimeLessThanOrEqualTo(Date value) {
            addCriterion("C_OPERATING_TIME <=", value, "operatingTime");
            return (Criteria) this;
        }

        public Criteria andOperatingTimeIn(List<Date> values) {
            addCriterion("C_OPERATING_TIME in", values, "operatingTime");
            return (Criteria) this;
        }

        public Criteria andOperatingTimeNotIn(List<Date> values) {
            addCriterion("C_OPERATING_TIME not in", values, "operatingTime");
            return (Criteria) this;
        }

        public Criteria andOperatingTimeBetween(Date value1, Date value2) {
            addCriterion("C_OPERATING_TIME between", value1, value2, "operatingTime");
            return (Criteria) this;
        }

        public Criteria andOperatingTimeNotBetween(Date value1, Date value2) {
            addCriterion("C_OPERATING_TIME not between", value1, value2, "operatingTime");
            return (Criteria) this;
        }

        public Criteria andOperatorIsNull() {
            addCriterion("C_OPERATOR is null");
            return (Criteria) this;
        }

        public Criteria andOperatorIsNotNull() {
            addCriterion("C_OPERATOR is not null");
            return (Criteria) this;
        }

        public Criteria andOperatorEqualTo(String value) {
            addCriterion("C_OPERATOR =", value, "operator");
            return (Criteria) this;
        }

        public Criteria andOperatorNotEqualTo(String value) {
            addCriterion("C_OPERATOR <>", value, "operator");
            return (Criteria) this;
        }

        public Criteria andOperatorGreaterThan(String value) {
            addCriterion("C_OPERATOR >", value, "operator");
            return (Criteria) this;
        }

        public Criteria andOperatorGreaterThanOrEqualTo(String value) {
            addCriterion("C_OPERATOR >=", value, "operator");
            return (Criteria) this;
        }

        public Criteria andOperatorLessThan(String value) {
            addCriterion("C_OPERATOR <", value, "operator");
            return (Criteria) this;
        }

        public Criteria andOperatorLessThanOrEqualTo(String value) {
            addCriterion("C_OPERATOR <=", value, "operator");
            return (Criteria) this;
        }

        public Criteria andOperatorLike(String value) {
            addCriterion("C_OPERATOR like", value, "operator");
            return (Criteria) this;
        }

        public Criteria andOperatorNotLike(String value) {
            addCriterion("C_OPERATOR not like", value, "operator");
            return (Criteria) this;
        }

        public Criteria andOperatorIn(List<String> values) {
            addCriterion("C_OPERATOR in", values, "operator");
            return (Criteria) this;
        }

        public Criteria andOperatorNotIn(List<String> values) {
            addCriterion("C_OPERATOR not in", values, "operator");
            return (Criteria) this;
        }

        public Criteria andOperatorBetween(String value1, String value2) {
            addCriterion("C_OPERATOR between", value1, value2, "operator");
            return (Criteria) this;
        }

        public Criteria andOperatorNotBetween(String value1, String value2) {
            addCriterion("C_OPERATOR not between", value1, value2, "operator");
            return (Criteria) this;
        }

        public Criteria andTelephoneIsNull() {
            addCriterion("C_TELEPHONE is null");
            return (Criteria) this;
        }

        public Criteria andTelephoneIsNotNull() {
            addCriterion("C_TELEPHONE is not null");
            return (Criteria) this;
        }

        public Criteria andTelephoneEqualTo(String value) {
            addCriterion("C_TELEPHONE =", value, "telephone");
            return (Criteria) this;
        }

        public Criteria andTelephoneNotEqualTo(String value) {
            addCriterion("C_TELEPHONE <>", value, "telephone");
            return (Criteria) this;
        }

        public Criteria andTelephoneGreaterThan(String value) {
            addCriterion("C_TELEPHONE >", value, "telephone");
            return (Criteria) this;
        }

        public Criteria andTelephoneGreaterThanOrEqualTo(String value) {
            addCriterion("C_TELEPHONE >=", value, "telephone");
            return (Criteria) this;
        }

        public Criteria andTelephoneLessThan(String value) {
            addCriterion("C_TELEPHONE <", value, "telephone");
            return (Criteria) this;
        }

        public Criteria andTelephoneLessThanOrEqualTo(String value) {
            addCriterion("C_TELEPHONE <=", value, "telephone");
            return (Criteria) this;
        }

        public Criteria andTelephoneLike(String value) {
            addCriterion("C_TELEPHONE like", value, "telephone");
            return (Criteria) this;
        }

        public Criteria andTelephoneNotLike(String value) {
            addCriterion("C_TELEPHONE not like", value, "telephone");
            return (Criteria) this;
        }

        public Criteria andTelephoneIn(List<String> values) {
            addCriterion("C_TELEPHONE in", values, "telephone");
            return (Criteria) this;
        }

        public Criteria andTelephoneNotIn(List<String> values) {
            addCriterion("C_TELEPHONE not in", values, "telephone");
            return (Criteria) this;
        }

        public Criteria andTelephoneBetween(String value1, String value2) {
            addCriterion("C_TELEPHONE between", value1, value2, "telephone");
            return (Criteria) this;
        }

        public Criteria andTelephoneNotBetween(String value1, String value2) {
            addCriterion("C_TELEPHONE not between", value1, value2, "telephone");
            return (Criteria) this;
        }
    }

    public static class Criteria extends GeneratedCriteria {

        protected Criteria() {
            super();
        }
    }

    public static class Criterion {
        private String condition;

        private Object value;

        private Object secondValue;

        private boolean noValue;

        private boolean singleValue;

        private boolean betweenValue;

        private boolean listValue;

        private String typeHandler;

        public String getCondition() {
            return condition;
        }

        public Object getValue() {
            return value;
        }

        public Object getSecondValue() {
            return secondValue;
        }

        public boolean isNoValue() {
            return noValue;
        }

        public boolean isSingleValue() {
            return singleValue;
        }

        public boolean isBetweenValue() {
            return betweenValue;
        }

        public boolean isListValue() {
            return listValue;
        }

        public String getTypeHandler() {
            return typeHandler;
        }

        protected Criterion(String condition) {
            super();
            this.condition = condition;
            this.typeHandler = null;
            this.noValue = true;
        }

        protected Criterion(String condition, Object value, String typeHandler) {
            super();
            this.condition = condition;
            this.value = value;
            this.typeHandler = typeHandler;
            if (value instanceof List<?>) {
                this.listValue = true;
            } else {
                this.singleValue = true;
            }
        }

        protected Criterion(String condition, Object value) {
            this(condition, value, null);
        }

        protected Criterion(String condition, Object value, Object secondValue, String typeHandler) {
            super();
            this.condition = condition;
            this.value = value;
            this.secondValue = secondValue;
            this.typeHandler = typeHandler;
            this.betweenValue = true;
        }

        protected Criterion(String condition, Object value, Object secondValue) {
            this(condition, value, secondValue, null);
        }
    }
}