package cn.itcast.bos.domain.base;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class StandardExample {
    protected String orderByClause;

    protected boolean distinct;

    protected List<Criteria> oredCriteria;

    public StandardExample() {
        oredCriteria = new ArrayList<Criteria>();
    }

    public void setOrderByClause(String orderByClause) {
        this.orderByClause = orderByClause;
    }

    public String getOrderByClause() {
        return orderByClause;
    }

    public void setDistinct(boolean distinct) {
        this.distinct = distinct;
    }

    public boolean isDistinct() {
        return distinct;
    }

    public List<Criteria> getOredCriteria() {
        return oredCriteria;
    }

    public void or(Criteria criteria) {
        oredCriteria.add(criteria);
    }

    public Criteria or() {
        Criteria criteria = createCriteriaInternal();
        oredCriteria.add(criteria);
        return criteria;
    }

    public Criteria createCriteria() {
        Criteria criteria = createCriteriaInternal();
        if (oredCriteria.size() == 0) {
            oredCriteria.add(criteria);
        }
        return criteria;
    }

    protected Criteria createCriteriaInternal() {
        Criteria criteria = new Criteria();
        return criteria;
    }

    public void clear() {
        oredCriteria.clear();
        orderByClause = null;
        distinct = false;
    }

    protected abstract static class GeneratedCriteria {
        protected List<Criterion> criteria;

        protected GeneratedCriteria() {
            super();
            criteria = new ArrayList<Criterion>();
        }

        public boolean isValid() {
            return criteria.size() > 0;
        }

        public List<Criterion> getAllCriteria() {
            return criteria;
        }

        public List<Criterion> getCriteria() {
            return criteria;
        }

        protected void addCriterion(String condition) {
            if (condition == null) {
                throw new RuntimeException("Value for condition cannot be null");
            }
            criteria.add(new Criterion(condition));
        }

        protected void addCriterion(String condition, Object value, String property) {
            if (value == null) {
                throw new RuntimeException("Value for " + property + " cannot be null");
            }
            criteria.add(new Criterion(condition, value));
        }

        protected void addCriterion(String condition, Object value1, Object value2, String property) {
            if (value1 == null || value2 == null) {
                throw new RuntimeException("Between values for " + property + " cannot be null");
            }
            criteria.add(new Criterion(condition, value1, value2));
        }

        public Criteria andIdIsNull() {
            addCriterion("C_ID is null");
            return (Criteria) this;
        }

        public Criteria andIdIsNotNull() {
            addCriterion("C_ID is not null");
            return (Criteria) this;
        }

        public Criteria andIdEqualTo(Integer value) {
            addCriterion("C_ID =", value, "id");
            return (Criteria) this;
        }

        public Criteria andIdNotEqualTo(Integer value) {
            addCriterion("C_ID <>", value, "id");
            return (Criteria) this;
        }

        public Criteria andIdGreaterThan(Integer value) {
            addCriterion("C_ID >", value, "id");
            return (Criteria) this;
        }

        public Criteria andIdGreaterThanOrEqualTo(Integer value) {
            addCriterion("C_ID >=", value, "id");
            return (Criteria) this;
        }

        public Criteria andIdLessThan(Integer value) {
            addCriterion("C_ID <", value, "id");
            return (Criteria) this;
        }

        public Criteria andIdLessThanOrEqualTo(Integer value) {
            addCriterion("C_ID <=", value, "id");
            return (Criteria) this;
        }

        public Criteria andIdIn(List<Integer> values) {
            addCriterion("C_ID in", values, "id");
            return (Criteria) this;
        }

        public Criteria andIdNotIn(List<Integer> values) {
            addCriterion("C_ID not in", values, "id");
            return (Criteria) this;
        }

        public Criteria andIdBetween(Integer value1, Integer value2) {
            addCriterion("C_ID between", value1, value2, "id");
            return (Criteria) this;
        }

        public Criteria andIdNotBetween(Integer value1, Integer value2) {
            addCriterion("C_ID not between", value1, value2, "id");
            return (Criteria) this;
        }

        public Criteria andMaxLengthIsNull() {
            addCriterion("C_MAX_LENGTH is null");
            return (Criteria) this;
        }

        public Criteria andMaxLengthIsNotNull() {
            addCriterion("C_MAX_LENGTH is not null");
            return (Criteria) this;
        }

        public Criteria andMaxLengthEqualTo(Integer value) {
            addCriterion("C_MAX_LENGTH =", value, "maxLength");
            return (Criteria) this;
        }

        public Criteria andMaxLengthNotEqualTo(Integer value) {
            addCriterion("C_MAX_LENGTH <>", value, "maxLength");
            return (Criteria) this;
        }

        public Criteria andMaxLengthGreaterThan(Integer value) {
            addCriterion("C_MAX_LENGTH >", value, "maxLength");
            return (Criteria) this;
        }

        public Criteria andMaxLengthGreaterThanOrEqualTo(Integer value) {
            addCriterion("C_MAX_LENGTH >=", value, "maxLength");
            return (Criteria) this;
        }

        public Criteria andMaxLengthLessThan(Integer value) {
            addCriterion("C_MAX_LENGTH <", value, "maxLength");
            return (Criteria) this;
        }

        public Criteria andMaxLengthLessThanOrEqualTo(Integer value) {
            addCriterion("C_MAX_LENGTH <=", value, "maxLength");
            return (Criteria) this;
        }

        public Criteria andMaxLengthIn(List<Integer> values) {
            addCriterion("C_MAX_LENGTH in", values, "maxLength");
            return (Criteria) this;
        }

        public Criteria andMaxLengthNotIn(List<Integer> values) {
            addCriterion("C_MAX_LENGTH not in", values, "maxLength");
            return (Criteria) this;
        }

        public Criteria andMaxLengthBetween(Integer value1, Integer value2) {
            addCriterion("C_MAX_LENGTH between", value1, value2, "maxLength");
            return (Criteria) this;
        }

        public Criteria andMaxLengthNotBetween(Integer value1, Integer value2) {
            addCriterion("C_MAX_LENGTH not between", value1, value2, "maxLength");
            return (Criteria) this;
        }

        public Criteria andMaxWeightIsNull() {
            addCriterion("C_MAX_WEIGHT is null");
            return (Criteria) this;
        }

        public Criteria andMaxWeightIsNotNull() {
            addCriterion("C_MAX_WEIGHT is not null");
            return (Criteria) this;
        }

        public Criteria andMaxWeightEqualTo(Integer value) {
            addCriterion("C_MAX_WEIGHT =", value, "maxWeight");
            return (Criteria) this;
        }

        public Criteria andMaxWeightNotEqualTo(Integer value) {
            addCriterion("C_MAX_WEIGHT <>", value, "maxWeight");
            return (Criteria) this;
        }

        public Criteria andMaxWeightGreaterThan(Integer value) {
            addCriterion("C_MAX_WEIGHT >", value, "maxWeight");
            return (Criteria) this;
        }

        public Criteria andMaxWeightGreaterThanOrEqualTo(Integer value) {
            addCriterion("C_MAX_WEIGHT >=", value, "maxWeight");
            return (Criteria) this;
        }

        public Criteria andMaxWeightLessThan(Integer value) {
            addCriterion("C_MAX_WEIGHT <", value, "maxWeight");
            return (Criteria) this;
        }

        public Criteria andMaxWeightLessThanOrEqualTo(Integer value) {
            addCriterion("C_MAX_WEIGHT <=", value, "maxWeight");
            return (Criteria) this;
        }

        public Criteria andMaxWeightIn(List<Integer> values) {
            addCriterion("C_MAX_WEIGHT in", values, "maxWeight");
            return (Criteria) this;
        }

        public Criteria andMaxWeightNotIn(List<Integer> values) {
            addCriterion("C_MAX_WEIGHT not in", values, "maxWeight");
            return (Criteria) this;
        }

        public Criteria andMaxWeightBetween(Integer value1, Integer value2) {
            addCriterion("C_MAX_WEIGHT between", value1, value2, "maxWeight");
            return (Criteria) this;
        }

        public Criteria andMaxWeightNotBetween(Integer value1, Integer value2) {
            addCriterion("C_MAX_WEIGHT not between", value1, value2, "maxWeight");
            return (Criteria) this;
        }

        public Criteria andMinLengthIsNull() {
            addCriterion("C_MIN_LENGTH is null");
            return (Criteria) this;
        }

        public Criteria andMinLengthIsNotNull() {
            addCriterion("C_MIN_LENGTH is not null");
            return (Criteria) this;
        }

        public Criteria andMinLengthEqualTo(Integer value) {
            addCriterion("C_MIN_LENGTH =", value, "minLength");
            return (Criteria) this;
        }

        public Criteria andMinLengthNotEqualTo(Integer value) {
            addCriterion("C_MIN_LENGTH <>", value, "minLength");
            return (Criteria) this;
        }

        public Criteria andMinLengthGreaterThan(Integer value) {
            addCriterion("C_MIN_LENGTH >", value, "minLength");
            return (Criteria) this;
        }

        public Criteria andMinLengthGreaterThanOrEqualTo(Integer value) {
            addCriterion("C_MIN_LENGTH >=", value, "minLength");
            return (Criteria) this;
        }

        public Criteria andMinLengthLessThan(Integer value) {
            addCriterion("C_MIN_LENGTH <", value, "minLength");
            return (Criteria) this;
        }

        public Criteria andMinLengthLessThanOrEqualTo(Integer value) {
            addCriterion("C_MIN_LENGTH <=", value, "minLength");
            return (Criteria) this;
        }

        public Criteria andMinLengthIn(List<Integer> values) {
            addCriterion("C_MIN_LENGTH in", values, "minLength");
            return (Criteria) this;
        }

        public Criteria andMinLengthNotIn(List<Integer> values) {
            addCriterion("C_MIN_LENGTH not in", values, "minLength");
            return (Criteria) this;
        }

        public Criteria andMinLengthBetween(Integer value1, Integer value2) {
            addCriterion("C_MIN_LENGTH between", value1, value2, "minLength");
            return (Criteria) this;
        }

        public Criteria andMinLengthNotBetween(Integer value1, Integer value2) {
            addCriterion("C_MIN_LENGTH not between", value1, value2, "minLength");
            return (Criteria) this;
        }

        public Criteria andMinWeightIsNull() {
            addCriterion("C_MIN_WEIGHT is null");
            return (Criteria) this;
        }

        public Criteria andMinWeightIsNotNull() {
            addCriterion("C_MIN_WEIGHT is not null");
            return (Criteria) this;
        }

        public Criteria andMinWeightEqualTo(Integer value) {
            addCriterion("C_MIN_WEIGHT =", value, "minWeight");
            return (Criteria) this;
        }

        public Criteria andMinWeightNotEqualTo(Integer value) {
            addCriterion("C_MIN_WEIGHT <>", value, "minWeight");
            return (Criteria) this;
        }

        public Criteria andMinWeightGreaterThan(Integer value) {
            addCriterion("C_MIN_WEIGHT >", value, "minWeight");
            return (Criteria) this;
        }

        public Criteria andMinWeightGreaterThanOrEqualTo(Integer value) {
            addCriterion("C_MIN_WEIGHT >=", value, "minWeight");
            return (Criteria) this;
        }

        public Criteria andMinWeightLessThan(Integer value) {
            addCriterion("C_MIN_WEIGHT <", value, "minWeight");
            return (Criteria) this;
        }

        public Criteria andMinWeightLessThanOrEqualTo(Integer value) {
            addCriterion("C_MIN_WEIGHT <=", value, "minWeight");
            return (Criteria) this;
        }

        public Criteria andMinWeightIn(List<Integer> values) {
            addCriterion("C_MIN_WEIGHT in", values, "minWeight");
            return (Criteria) this;
        }

        public Criteria andMinWeightNotIn(List<Integer> values) {
            addCriterion("C_MIN_WEIGHT not in", values, "minWeight");
            return (Criteria) this;
        }

        public Criteria andMinWeightBetween(Integer value1, Integer value2) {
            addCriterion("C_MIN_WEIGHT between", value1, value2, "minWeight");
            return (Criteria) this;
        }

        public Criteria andMinWeightNotBetween(Integer value1, Integer value2) {
            addCriterion("C_MIN_WEIGHT not between", value1, value2, "minWeight");
            return (Criteria) this;
        }

        public Criteria andNameIsNull() {
            addCriterion("C_NAME is null");
            return (Criteria) this;
        }

        public Criteria andNameIsNotNull() {
            addCriterion("C_NAME is not null");
            return (Criteria) this;
        }

        public Criteria andNameEqualTo(String value) {
            addCriterion("C_NAME =", value, "name");
            return (Criteria) this;
        }

        public Criteria andNameNotEqualTo(String value) {
            addCriterion("C_NAME <>", value, "name");
            return (Criteria) this;
        }

        public Criteria andNameGreaterThan(String value) {
            addCriterion("C_NAME >", value, "name");
            return (Criteria) this;
        }

        public Criteria andNameGreaterThanOrEqualTo(String value) {
            addCriterion("C_NAME >=", value, "name");
            return (Criteria) this;
        }

        public Criteria andNameLessThan(String value) {
            addCriterion("C_NAME <", value, "name");
            return (Criteria) this;
        }

        public Criteria andNameLessThanOrEqualTo(String value) {
            addCriterion("C_NAME <=", value, "name");
            return (Criteria) this;
        }

        public Criteria andNameLike(String value) {
            addCriterion("C_NAME like", value, "name");
            return (Criteria) this;
        }

        public Criteria andNameNotLike(String value) {
            addCriterion("C_NAME not like", value, "name");
            return (Criteria) this;
        }

        public Criteria andNameIn(List<String> values) {
            addCriterion("C_NAME in", values, "name");
            return (Criteria) this;
        }

        public Criteria andNameNotIn(List<String> values) {
            addCriterion("C_NAME not in", values, "name");
            return (Criteria) this;
        }

        public Criteria andNameBetween(String value1, String value2) {
            addCriterion("C_NAME between", value1, value2, "name");
            return (Criteria) this;
        }

        public Criteria andNameNotBetween(String value1, String value2) {
            addCriterion("C_NAME not between", value1, value2, "name");
            return (Criteria) this;
        }

        public Criteria andOperatingCompanyIsNull() {
            addCriterion("C_OPERATING_COMPANY is null");
            return (Criteria) this;
        }

        public Criteria andOperatingCompanyIsNotNull() {
            addCriterion("C_OPERATING_COMPANY is not null");
            return (Criteria) this;
        }

        public Criteria andOperatingCompanyEqualTo(String value) {
            addCriterion("C_OPERATING_COMPANY =", value, "operatingCompany");
            return (Criteria) this;
        }

        public Criteria andOperatingCompanyNotEqualTo(String value) {
            addCriterion("C_OPERATING_COMPANY <>", value, "operatingCompany");
            return (Criteria) this;
        }

        public Criteria andOperatingCompanyGreaterThan(String value) {
            addCriterion("C_OPERATING_COMPANY >", value, "operatingCompany");
            return (Criteria) this;
        }

        public Criteria andOperatingCompanyGreaterThanOrEqualTo(String value) {
            addCriterion("C_OPERATING_COMPANY >=", value, "operatingCompany");
            return (Criteria) this;
        }

        public Criteria andOperatingCompanyLessThan(String value) {
            addCriterion("C_OPERATING_COMPANY <", value, "operatingCompany");
            return (Criteria) this;
        }

        public Criteria andOperatingCompanyLessThanOrEqualTo(String value) {
            addCriterion("C_OPERATING_COMPANY <=", value, "operatingCompany");
            return (Criteria) this;
        }

        public Criteria andOperatingCompanyLike(String value) {
            addCriterion("C_OPERATING_COMPANY like", value, "operatingCompany");
            return (Criteria) this;
        }

        public Criteria andOperatingCompanyNotLike(String value) {
            addCriterion("C_OPERATING_COMPANY not like", value, "operatingCompany");
            return (Criteria) this;
        }

        public Criteria andOperatingCompanyIn(List<String> values) {
            addCriterion("C_OPERATING_COMPANY in", values, "operatingCompany");
            return (Criteria) this;
        }

        public Criteria andOperatingCompanyNotIn(List<String> values) {
            addCriterion("C_OPERATING_COMPANY not in", values, "operatingCompany");
            return (Criteria) this;
        }

        public Criteria andOperatingCompanyBetween(String value1, String value2) {
            addCriterion("C_OPERATING_COMPANY between", value1, value2, "operatingCompany");
            return (Criteria) this;
        }

        public Criteria andOperatingCompanyNotBetween(String value1, String value2) {
            addCriterion("C_OPERATING_COMPANY not between", value1, value2, "operatingCompany");
            return (Criteria) this;
        }

        public Criteria andOperatingTimeIsNull() {
            addCriterion("C_OPERATING_TIME is null");
            return (Criteria) this;
        }

        public Criteria andOperatingTimeIsNotNull() {
            addCriterion("C_OPERATING_TIME is not null");
            return (Criteria) this;
        }

        public Criteria andOperatingTimeEqualTo(Date value) {
            addCriterion("C_OPERATING_TIME =", value, "operatingTime");
            return (Criteria) this;
        }

        public Criteria andOperatingTimeNotEqualTo(Date value) {
            addCriterion("C_OPERATING_TIME <>", value, "operatingTime");
            return (Criteria) this;
        }

        public Criteria andOperatingTimeGreaterThan(Date value) {
            addCriterion("C_OPERATING_TIME >", value, "operatingTime");
            return (Criteria) this;
        }

        public Criteria andOperatingTimeGreaterThanOrEqualTo(Date value) {
            addCriterion("C_OPERATING_TIME >=", value, "operatingTime");
            return (Criteria) this;
        }

        public Criteria andOperatingTimeLessThan(Date value) {
            addCriterion("C_OPERATING_TIME <", value, "operatingTime");
            return (Criteria) this;
        }

        public Criteria andOperatingTimeLessThanOrEqualTo(Date value) {
            addCriterion("C_OPERATING_TIME <=", value, "operatingTime");
            return (Criteria) this;
        }

        public Criteria andOperatingTimeIn(List<Date> values) {
            addCriterion("C_OPERATING_TIME in", values, "operatingTime");
            return (Criteria) this;
        }

        public Criteria andOperatingTimeNotIn(List<Date> values) {
            addCriterion("C_OPERATING_TIME not in", values, "operatingTime");
            return (Criteria) this;
        }

        public Criteria andOperatingTimeBetween(Date value1, Date value2) {
            addCriterion("C_OPERATING_TIME between", value1, value2, "operatingTime");
            return (Criteria) this;
        }

        public Criteria andOperatingTimeNotBetween(Date value1, Date value2) {
            addCriterion("C_OPERATING_TIME not between", value1, value2, "operatingTime");
            return (Criteria) this;
        }

        public Criteria andOperatorIsNull() {
            addCriterion("C_OPERATOR is null");
            return (Criteria) this;
        }

        public Criteria andOperatorIsNotNull() {
            addCriterion("C_OPERATOR is not null");
            return (Criteria) this;
        }

        public Criteria andOperatorEqualTo(String value) {
            addCriterion("C_OPERATOR =", value, "operator");
            return (Criteria) this;
        }

        public Criteria andOperatorNotEqualTo(String value) {
            addCriterion("C_OPERATOR <>", value, "operator");
            return (Criteria) this;
        }

        public Criteria andOperatorGreaterThan(String value) {
            addCriterion("C_OPERATOR >", value, "operator");
            return (Criteria) this;
        }

        public Criteria andOperatorGreaterThanOrEqualTo(String value) {
            addCriterion("C_OPERATOR >=", value, "operator");
            return (Criteria) this;
        }

        public Criteria andOperatorLessThan(String value) {
            addCriterion("C_OPERATOR <", value, "operator");
            return (Criteria) this;
        }

        public Criteria andOperatorLessThanOrEqualTo(String value) {
            addCriterion("C_OPERATOR <=", value, "operator");
            return (Criteria) this;
        }

        public Criteria andOperatorLike(String value) {
            addCriterion("C_OPERATOR like", value, "operator");
            return (Criteria) this;
        }

        public Criteria andOperatorNotLike(String value) {
            addCriterion("C_OPERATOR not like", value, "operator");
            return (Criteria) this;
        }

        public Criteria andOperatorIn(List<String> values) {
            addCriterion("C_OPERATOR in", values, "operator");
            return (Criteria) this;
        }

        public Criteria andOperatorNotIn(List<String> values) {
            addCriterion("C_OPERATOR not in", values, "operator");
            return (Criteria) this;
        }

        public Criteria andOperatorBetween(String value1, String value2) {
            addCriterion("C_OPERATOR between", value1, value2, "operator");
            return (Criteria) this;
        }

        public Criteria andOperatorNotBetween(String value1, String value2) {
            addCriterion("C_OPERATOR not between", value1, value2, "operator");
            return (Criteria) this;
        }
    }

    public static class Criteria extends GeneratedCriteria {

        protected Criteria() {
            super();
        }
    }

    public static class Criterion {
        private String condition;

        private Object value;

        private Object secondValue;

        private boolean noValue;

        private boolean singleValue;

        private boolean betweenValue;

        private boolean listValue;

        private String typeHandler;

        public String getCondition() {
            return condition;
        }

        public Object getValue() {
            return value;
        }

        public Object getSecondValue() {
            return secondValue;
        }

        public boolean isNoValue() {
            return noValue;
        }

        public boolean isSingleValue() {
            return singleValue;
        }

        public boolean isBetweenValue() {
            return betweenValue;
        }

        public boolean isListValue() {
            return listValue;
        }

        public String getTypeHandler() {
            return typeHandler;
        }

        protected Criterion(String condition) {
            super();
            this.condition = condition;
            this.typeHandler = null;
            this.noValue = true;
        }

        protected Criterion(String condition, Object value, String typeHandler) {
            super();
            this.condition = condition;
            this.value = value;
            this.typeHandler = typeHandler;
            if (value instanceof List<?>) {
                this.listValue = true;
            } else {
                this.singleValue = true;
            }
        }

        protected Criterion(String condition, Object value) {
            this(condition, value, null);
        }

        protected Criterion(String condition, Object value, Object secondValue, String typeHandler) {
            super();
            this.condition = condition;
            this.value = value;
            this.secondValue = secondValue;
            this.typeHandler = typeHandler;
            this.betweenValue = true;
        }

        protected Criterion(String condition, Object value, Object secondValue) {
            this(condition, value, secondValue, null);
        }
    }
}