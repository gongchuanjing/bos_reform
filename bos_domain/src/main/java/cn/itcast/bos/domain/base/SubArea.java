package cn.itcast.bos.domain.base;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import javax.persistence.*;


/**
 * @description:分区
 */
@Entity
@Table(name = "T_SUB_AREA")
@JsonIgnoreProperties(value = {"fixedArea"})//去掉属性 解决json死循环
public class SubArea {

	@Id
	@Column(name = "C_ID")
	private String id;

	@Column(name = "C_START_NUM")
	private String startNum; // 起始号

	@Column(name = "C_ENDNUM")
	private String endNum; // 终止号

	@Column(name = "C_SINGLE")
	private Character single; // 单双号

	@Column(name = "C_KEY_WORDS")
	private String keyWords; // 关键字

	@Column(name = "C_ASSIST_KEY_WORDS")
	private String assistKeyWords; // 辅助关键字

	@ManyToOne
	@JoinColumn(name = "C_AREA_ID")
	private Area area; // 区域

	@ManyToOne
	@JoinColumn(name = "C_FIXEDAREA_ID")
	private FixedArea fixedArea; // 定区

	//设置区域的名称:
	@Transient//不生成表字段
	private String areaName;

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public Character getSingle() {
		return single;
	}

	public void setSingle(Character single) {
		this.single = single;
	}

	public String getStartNum() {
		return startNum;
	}

	public void setStartNum(String startNum) {
		this.startNum = startNum;
	}

	public String getEndNum() {
		return endNum;
	}

	public void setEndNum(String endNum) {
		this.endNum = endNum;
	}

	public String getKeyWords() {
		return keyWords;
	}

	public void setKeyWords(String keyWords) {
		this.keyWords = keyWords;
	}

	public String getAssistKeyWords() {
		return assistKeyWords;
	}

	public void setAssistKeyWords(String assistKeyWords) {
		this.assistKeyWords = assistKeyWords;
	}

	public Area getArea() {
		return area;
	}

	public void setArea(Area area) {
		this.area = area;
	}

	public FixedArea getFixedArea() {
		return fixedArea;
	}

	public void setFixedArea(FixedArea fixedArea) {
		this.fixedArea = fixedArea;
	}


	public String getAreaName() {
		return areaName;
	}

	public void setAreaName(String areaName) {
		this.areaName = areaName;
	}

	@Override
	public String toString() {
		return "SubArea{" +
				"id='" + id + '\'' +
				", startNum='" + startNum + '\'' +
				", endNum='" + endNum + '\'' +
				", single=" + single +
				", keyWords='" + keyWords + '\'' +
				", assistKeyWords='" + assistKeyWords + '\'' +
				", area=" + area +
				", fixedArea=" + fixedArea +
				", areaName='" + areaName + '\'' +
				'}';
	}
}
