package cn.itcast.bos.domain.base;

import java.util.ArrayList;
import java.util.List;

public class SubAreaExample {
    protected String orderByClause;

    protected boolean distinct;

    protected List<Criteria> oredCriteria;

    public SubAreaExample() {
        oredCriteria = new ArrayList<Criteria>();
    }

    public void setOrderByClause(String orderByClause) {
        this.orderByClause = orderByClause;
    }

    public String getOrderByClause() {
        return orderByClause;
    }

    public void setDistinct(boolean distinct) {
        this.distinct = distinct;
    }

    public boolean isDistinct() {
        return distinct;
    }

    public List<Criteria> getOredCriteria() {
        return oredCriteria;
    }

    public void or(Criteria criteria) {
        oredCriteria.add(criteria);
    }

    public Criteria or() {
        Criteria criteria = createCriteriaInternal();
        oredCriteria.add(criteria);
        return criteria;
    }

    public Criteria createCriteria() {
        Criteria criteria = createCriteriaInternal();
        if (oredCriteria.size() == 0) {
            oredCriteria.add(criteria);
        }
        return criteria;
    }

    protected Criteria createCriteriaInternal() {
        Criteria criteria = new Criteria();
        return criteria;
    }

    public void clear() {
        oredCriteria.clear();
        orderByClause = null;
        distinct = false;
    }

    protected abstract static class GeneratedCriteria {
        protected List<Criterion> criteria;

        protected GeneratedCriteria() {
            super();
            criteria = new ArrayList<Criterion>();
        }

        public boolean isValid() {
            return criteria.size() > 0;
        }

        public List<Criterion> getAllCriteria() {
            return criteria;
        }

        public List<Criterion> getCriteria() {
            return criteria;
        }

        protected void addCriterion(String condition) {
            if (condition == null) {
                throw new RuntimeException("Value for condition cannot be null");
            }
            criteria.add(new Criterion(condition));
        }

        protected void addCriterion(String condition, Object value, String property) {
            if (value == null) {
                throw new RuntimeException("Value for " + property + " cannot be null");
            }
            criteria.add(new Criterion(condition, value));
        }

        protected void addCriterion(String condition, Object value1, Object value2, String property) {
            if (value1 == null || value2 == null) {
                throw new RuntimeException("Between values for " + property + " cannot be null");
            }
            criteria.add(new Criterion(condition, value1, value2));
        }

        public Criteria andIdIsNull() {
            addCriterion("C_ID is null");
            return (Criteria) this;
        }

        public Criteria andIdIsNotNull() {
            addCriterion("C_ID is not null");
            return (Criteria) this;
        }

        public Criteria andIdEqualTo(String value) {
            addCriterion("C_ID =", value, "id");
            return (Criteria) this;
        }

        public Criteria andIdNotEqualTo(String value) {
            addCriterion("C_ID <>", value, "id");
            return (Criteria) this;
        }

        public Criteria andIdGreaterThan(String value) {
            addCriterion("C_ID >", value, "id");
            return (Criteria) this;
        }

        public Criteria andIdGreaterThanOrEqualTo(String value) {
            addCriterion("C_ID >=", value, "id");
            return (Criteria) this;
        }

        public Criteria andIdLessThan(String value) {
            addCriterion("C_ID <", value, "id");
            return (Criteria) this;
        }

        public Criteria andIdLessThanOrEqualTo(String value) {
            addCriterion("C_ID <=", value, "id");
            return (Criteria) this;
        }

        public Criteria andIdLike(String value) {
            addCriterion("C_ID like", value, "id");
            return (Criteria) this;
        }

        public Criteria andIdNotLike(String value) {
            addCriterion("C_ID not like", value, "id");
            return (Criteria) this;
        }

        public Criteria andIdIn(List<String> values) {
            addCriterion("C_ID in", values, "id");
            return (Criteria) this;
        }

        public Criteria andIdNotIn(List<String> values) {
            addCriterion("C_ID not in", values, "id");
            return (Criteria) this;
        }

        public Criteria andIdBetween(String value1, String value2) {
            addCriterion("C_ID between", value1, value2, "id");
            return (Criteria) this;
        }

        public Criteria andIdNotBetween(String value1, String value2) {
            addCriterion("C_ID not between", value1, value2, "id");
            return (Criteria) this;
        }

        public Criteria andAssistKeyWordsIsNull() {
            addCriterion("C_ASSIST_KEY_WORDS is null");
            return (Criteria) this;
        }

        public Criteria andAssistKeyWordsIsNotNull() {
            addCriterion("C_ASSIST_KEY_WORDS is not null");
            return (Criteria) this;
        }

        public Criteria andAssistKeyWordsEqualTo(String value) {
            addCriterion("C_ASSIST_KEY_WORDS =", value, "assistKeyWords");
            return (Criteria) this;
        }

        public Criteria andAssistKeyWordsNotEqualTo(String value) {
            addCriterion("C_ASSIST_KEY_WORDS <>", value, "assistKeyWords");
            return (Criteria) this;
        }

        public Criteria andAssistKeyWordsGreaterThan(String value) {
            addCriterion("C_ASSIST_KEY_WORDS >", value, "assistKeyWords");
            return (Criteria) this;
        }

        public Criteria andAssistKeyWordsGreaterThanOrEqualTo(String value) {
            addCriterion("C_ASSIST_KEY_WORDS >=", value, "assistKeyWords");
            return (Criteria) this;
        }

        public Criteria andAssistKeyWordsLessThan(String value) {
            addCriterion("C_ASSIST_KEY_WORDS <", value, "assistKeyWords");
            return (Criteria) this;
        }

        public Criteria andAssistKeyWordsLessThanOrEqualTo(String value) {
            addCriterion("C_ASSIST_KEY_WORDS <=", value, "assistKeyWords");
            return (Criteria) this;
        }

        public Criteria andAssistKeyWordsLike(String value) {
            addCriterion("C_ASSIST_KEY_WORDS like", value, "assistKeyWords");
            return (Criteria) this;
        }

        public Criteria andAssistKeyWordsNotLike(String value) {
            addCriterion("C_ASSIST_KEY_WORDS not like", value, "assistKeyWords");
            return (Criteria) this;
        }

        public Criteria andAssistKeyWordsIn(List<String> values) {
            addCriterion("C_ASSIST_KEY_WORDS in", values, "assistKeyWords");
            return (Criteria) this;
        }

        public Criteria andAssistKeyWordsNotIn(List<String> values) {
            addCriterion("C_ASSIST_KEY_WORDS not in", values, "assistKeyWords");
            return (Criteria) this;
        }

        public Criteria andAssistKeyWordsBetween(String value1, String value2) {
            addCriterion("C_ASSIST_KEY_WORDS between", value1, value2, "assistKeyWords");
            return (Criteria) this;
        }

        public Criteria andAssistKeyWordsNotBetween(String value1, String value2) {
            addCriterion("C_ASSIST_KEY_WORDS not between", value1, value2, "assistKeyWords");
            return (Criteria) this;
        }

        public Criteria andEndnumIsNull() {
            addCriterion("C_ENDNUM is null");
            return (Criteria) this;
        }

        public Criteria andEndnumIsNotNull() {
            addCriterion("C_ENDNUM is not null");
            return (Criteria) this;
        }

        public Criteria andEndnumEqualTo(String value) {
            addCriterion("C_ENDNUM =", value, "endnum");
            return (Criteria) this;
        }

        public Criteria andEndnumNotEqualTo(String value) {
            addCriterion("C_ENDNUM <>", value, "endnum");
            return (Criteria) this;
        }

        public Criteria andEndnumGreaterThan(String value) {
            addCriterion("C_ENDNUM >", value, "endnum");
            return (Criteria) this;
        }

        public Criteria andEndnumGreaterThanOrEqualTo(String value) {
            addCriterion("C_ENDNUM >=", value, "endnum");
            return (Criteria) this;
        }

        public Criteria andEndnumLessThan(String value) {
            addCriterion("C_ENDNUM <", value, "endnum");
            return (Criteria) this;
        }

        public Criteria andEndnumLessThanOrEqualTo(String value) {
            addCriterion("C_ENDNUM <=", value, "endnum");
            return (Criteria) this;
        }

        public Criteria andEndnumLike(String value) {
            addCriterion("C_ENDNUM like", value, "endnum");
            return (Criteria) this;
        }

        public Criteria andEndnumNotLike(String value) {
            addCriterion("C_ENDNUM not like", value, "endnum");
            return (Criteria) this;
        }

        public Criteria andEndnumIn(List<String> values) {
            addCriterion("C_ENDNUM in", values, "endnum");
            return (Criteria) this;
        }

        public Criteria andEndnumNotIn(List<String> values) {
            addCriterion("C_ENDNUM not in", values, "endnum");
            return (Criteria) this;
        }

        public Criteria andEndnumBetween(String value1, String value2) {
            addCriterion("C_ENDNUM between", value1, value2, "endnum");
            return (Criteria) this;
        }

        public Criteria andEndnumNotBetween(String value1, String value2) {
            addCriterion("C_ENDNUM not between", value1, value2, "endnum");
            return (Criteria) this;
        }

        public Criteria andKeyWordsIsNull() {
            addCriterion("C_KEY_WORDS is null");
            return (Criteria) this;
        }

        public Criteria andKeyWordsIsNotNull() {
            addCriterion("C_KEY_WORDS is not null");
            return (Criteria) this;
        }

        public Criteria andKeyWordsEqualTo(String value) {
            addCriterion("C_KEY_WORDS =", value, "keyWords");
            return (Criteria) this;
        }

        public Criteria andKeyWordsNotEqualTo(String value) {
            addCriterion("C_KEY_WORDS <>", value, "keyWords");
            return (Criteria) this;
        }

        public Criteria andKeyWordsGreaterThan(String value) {
            addCriterion("C_KEY_WORDS >", value, "keyWords");
            return (Criteria) this;
        }

        public Criteria andKeyWordsGreaterThanOrEqualTo(String value) {
            addCriterion("C_KEY_WORDS >=", value, "keyWords");
            return (Criteria) this;
        }

        public Criteria andKeyWordsLessThan(String value) {
            addCriterion("C_KEY_WORDS <", value, "keyWords");
            return (Criteria) this;
        }

        public Criteria andKeyWordsLessThanOrEqualTo(String value) {
            addCriterion("C_KEY_WORDS <=", value, "keyWords");
            return (Criteria) this;
        }

        public Criteria andKeyWordsLike(String value) {
            addCriterion("C_KEY_WORDS like", value, "keyWords");
            return (Criteria) this;
        }

        public Criteria andKeyWordsNotLike(String value) {
            addCriterion("C_KEY_WORDS not like", value, "keyWords");
            return (Criteria) this;
        }

        public Criteria andKeyWordsIn(List<String> values) {
            addCriterion("C_KEY_WORDS in", values, "keyWords");
            return (Criteria) this;
        }

        public Criteria andKeyWordsNotIn(List<String> values) {
            addCriterion("C_KEY_WORDS not in", values, "keyWords");
            return (Criteria) this;
        }

        public Criteria andKeyWordsBetween(String value1, String value2) {
            addCriterion("C_KEY_WORDS between", value1, value2, "keyWords");
            return (Criteria) this;
        }

        public Criteria andKeyWordsNotBetween(String value1, String value2) {
            addCriterion("C_KEY_WORDS not between", value1, value2, "keyWords");
            return (Criteria) this;
        }

        public Criteria andSingleIsNull() {
            addCriterion("C_SINGLE is null");
            return (Criteria) this;
        }

        public Criteria andSingleIsNotNull() {
            addCriterion("C_SINGLE is not null");
            return (Criteria) this;
        }

        public Criteria andSingleEqualTo(String value) {
            addCriterion("C_SINGLE =", value, "single");
            return (Criteria) this;
        }

        public Criteria andSingleNotEqualTo(String value) {
            addCriterion("C_SINGLE <>", value, "single");
            return (Criteria) this;
        }

        public Criteria andSingleGreaterThan(String value) {
            addCriterion("C_SINGLE >", value, "single");
            return (Criteria) this;
        }

        public Criteria andSingleGreaterThanOrEqualTo(String value) {
            addCriterion("C_SINGLE >=", value, "single");
            return (Criteria) this;
        }

        public Criteria andSingleLessThan(String value) {
            addCriterion("C_SINGLE <", value, "single");
            return (Criteria) this;
        }

        public Criteria andSingleLessThanOrEqualTo(String value) {
            addCriterion("C_SINGLE <=", value, "single");
            return (Criteria) this;
        }

        public Criteria andSingleLike(String value) {
            addCriterion("C_SINGLE like", value, "single");
            return (Criteria) this;
        }

        public Criteria andSingleNotLike(String value) {
            addCriterion("C_SINGLE not like", value, "single");
            return (Criteria) this;
        }

        public Criteria andSingleIn(List<String> values) {
            addCriterion("C_SINGLE in", values, "single");
            return (Criteria) this;
        }

        public Criteria andSingleNotIn(List<String> values) {
            addCriterion("C_SINGLE not in", values, "single");
            return (Criteria) this;
        }

        public Criteria andSingleBetween(String value1, String value2) {
            addCriterion("C_SINGLE between", value1, value2, "single");
            return (Criteria) this;
        }

        public Criteria andSingleNotBetween(String value1, String value2) {
            addCriterion("C_SINGLE not between", value1, value2, "single");
            return (Criteria) this;
        }

        public Criteria andStartNumIsNull() {
            addCriterion("C_START_NUM is null");
            return (Criteria) this;
        }

        public Criteria andStartNumIsNotNull() {
            addCriterion("C_START_NUM is not null");
            return (Criteria) this;
        }

        public Criteria andStartNumEqualTo(String value) {
            addCriterion("C_START_NUM =", value, "startNum");
            return (Criteria) this;
        }

        public Criteria andStartNumNotEqualTo(String value) {
            addCriterion("C_START_NUM <>", value, "startNum");
            return (Criteria) this;
        }

        public Criteria andStartNumGreaterThan(String value) {
            addCriterion("C_START_NUM >", value, "startNum");
            return (Criteria) this;
        }

        public Criteria andStartNumGreaterThanOrEqualTo(String value) {
            addCriterion("C_START_NUM >=", value, "startNum");
            return (Criteria) this;
        }

        public Criteria andStartNumLessThan(String value) {
            addCriterion("C_START_NUM <", value, "startNum");
            return (Criteria) this;
        }

        public Criteria andStartNumLessThanOrEqualTo(String value) {
            addCriterion("C_START_NUM <=", value, "startNum");
            return (Criteria) this;
        }

        public Criteria andStartNumLike(String value) {
            addCriterion("C_START_NUM like", value, "startNum");
            return (Criteria) this;
        }

        public Criteria andStartNumNotLike(String value) {
            addCriterion("C_START_NUM not like", value, "startNum");
            return (Criteria) this;
        }

        public Criteria andStartNumIn(List<String> values) {
            addCriterion("C_START_NUM in", values, "startNum");
            return (Criteria) this;
        }

        public Criteria andStartNumNotIn(List<String> values) {
            addCriterion("C_START_NUM not in", values, "startNum");
            return (Criteria) this;
        }

        public Criteria andStartNumBetween(String value1, String value2) {
            addCriterion("C_START_NUM between", value1, value2, "startNum");
            return (Criteria) this;
        }

        public Criteria andStartNumNotBetween(String value1, String value2) {
            addCriterion("C_START_NUM not between", value1, value2, "startNum");
            return (Criteria) this;
        }

        public Criteria andAreaIdIsNull() {
            addCriterion("C_AREA_ID is null");
            return (Criteria) this;
        }

        public Criteria andAreaIdIsNotNull() {
            addCriterion("C_AREA_ID is not null");
            return (Criteria) this;
        }

        public Criteria andAreaIdEqualTo(String value) {
            addCriterion("C_AREA_ID =", value, "areaId");
            return (Criteria) this;
        }

        public Criteria andAreaIdNotEqualTo(String value) {
            addCriterion("C_AREA_ID <>", value, "areaId");
            return (Criteria) this;
        }

        public Criteria andAreaIdGreaterThan(String value) {
            addCriterion("C_AREA_ID >", value, "areaId");
            return (Criteria) this;
        }

        public Criteria andAreaIdGreaterThanOrEqualTo(String value) {
            addCriterion("C_AREA_ID >=", value, "areaId");
            return (Criteria) this;
        }

        public Criteria andAreaIdLessThan(String value) {
            addCriterion("C_AREA_ID <", value, "areaId");
            return (Criteria) this;
        }

        public Criteria andAreaIdLessThanOrEqualTo(String value) {
            addCriterion("C_AREA_ID <=", value, "areaId");
            return (Criteria) this;
        }

        public Criteria andAreaIdLike(String value) {
            addCriterion("C_AREA_ID like", value, "areaId");
            return (Criteria) this;
        }

        public Criteria andAreaIdNotLike(String value) {
            addCriterion("C_AREA_ID not like", value, "areaId");
            return (Criteria) this;
        }

        public Criteria andAreaIdIn(List<String> values) {
            addCriterion("C_AREA_ID in", values, "areaId");
            return (Criteria) this;
        }

        public Criteria andAreaIdNotIn(List<String> values) {
            addCriterion("C_AREA_ID not in", values, "areaId");
            return (Criteria) this;
        }

        public Criteria andAreaIdBetween(String value1, String value2) {
            addCriterion("C_AREA_ID between", value1, value2, "areaId");
            return (Criteria) this;
        }

        public Criteria andAreaIdNotBetween(String value1, String value2) {
            addCriterion("C_AREA_ID not between", value1, value2, "areaId");
            return (Criteria) this;
        }

        public Criteria andFixedareaIdIsNull() {
            addCriterion("C_FIXEDAREA_ID is null");
            return (Criteria) this;
        }

        public Criteria andFixedareaIdIsNotNull() {
            addCriterion("C_FIXEDAREA_ID is not null");
            return (Criteria) this;
        }

        public Criteria andFixedareaIdEqualTo(String value) {
            addCriterion("C_FIXEDAREA_ID =", value, "fixedareaId");
            return (Criteria) this;
        }

        public Criteria andFixedareaIdNotEqualTo(String value) {
            addCriterion("C_FIXEDAREA_ID <>", value, "fixedareaId");
            return (Criteria) this;
        }

        public Criteria andFixedareaIdGreaterThan(String value) {
            addCriterion("C_FIXEDAREA_ID >", value, "fixedareaId");
            return (Criteria) this;
        }

        public Criteria andFixedareaIdGreaterThanOrEqualTo(String value) {
            addCriterion("C_FIXEDAREA_ID >=", value, "fixedareaId");
            return (Criteria) this;
        }

        public Criteria andFixedareaIdLessThan(String value) {
            addCriterion("C_FIXEDAREA_ID <", value, "fixedareaId");
            return (Criteria) this;
        }

        public Criteria andFixedareaIdLessThanOrEqualTo(String value) {
            addCriterion("C_FIXEDAREA_ID <=", value, "fixedareaId");
            return (Criteria) this;
        }

        public Criteria andFixedareaIdLike(String value) {
            addCriterion("C_FIXEDAREA_ID like", value, "fixedareaId");
            return (Criteria) this;
        }

        public Criteria andFixedareaIdNotLike(String value) {
            addCriterion("C_FIXEDAREA_ID not like", value, "fixedareaId");
            return (Criteria) this;
        }

        public Criteria andFixedareaIdIn(List<String> values) {
            addCriterion("C_FIXEDAREA_ID in", values, "fixedareaId");
            return (Criteria) this;
        }

        public Criteria andFixedareaIdNotIn(List<String> values) {
            addCriterion("C_FIXEDAREA_ID not in", values, "fixedareaId");
            return (Criteria) this;
        }

        public Criteria andFixedareaIdBetween(String value1, String value2) {
            addCriterion("C_FIXEDAREA_ID between", value1, value2, "fixedareaId");
            return (Criteria) this;
        }

        public Criteria andFixedareaIdNotBetween(String value1, String value2) {
            addCriterion("C_FIXEDAREA_ID not between", value1, value2, "fixedareaId");
            return (Criteria) this;
        }
    }

    public static class Criteria extends GeneratedCriteria {

        protected Criteria() {
            super();
        }
    }

    public static class Criterion {
        private String condition;

        private Object value;

        private Object secondValue;

        private boolean noValue;

        private boolean singleValue;

        private boolean betweenValue;

        private boolean listValue;

        private String typeHandler;

        public String getCondition() {
            return condition;
        }

        public Object getValue() {
            return value;
        }

        public Object getSecondValue() {
            return secondValue;
        }

        public boolean isNoValue() {
            return noValue;
        }

        public boolean isSingleValue() {
            return singleValue;
        }

        public boolean isBetweenValue() {
            return betweenValue;
        }

        public boolean isListValue() {
            return listValue;
        }

        public String getTypeHandler() {
            return typeHandler;
        }

        protected Criterion(String condition) {
            super();
            this.condition = condition;
            this.typeHandler = null;
            this.noValue = true;
        }

        protected Criterion(String condition, Object value, String typeHandler) {
            super();
            this.condition = condition;
            this.value = value;
            this.typeHandler = typeHandler;
            if (value instanceof List<?>) {
                this.listValue = true;
            } else {
                this.singleValue = true;
            }
        }

        protected Criterion(String condition, Object value) {
            this(condition, value, null);
        }

        protected Criterion(String condition, Object value, Object secondValue, String typeHandler) {
            super();
            this.condition = condition;
            this.value = value;
            this.secondValue = secondValue;
            this.typeHandler = typeHandler;
            this.betweenValue = true;
        }

        protected Criterion(String condition, Object value, Object secondValue) {
            this(condition, value, secondValue, null);
        }
    }
}