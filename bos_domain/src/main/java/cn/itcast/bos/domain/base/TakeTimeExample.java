package cn.itcast.bos.domain.base;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class TakeTimeExample {
    protected String orderByClause;

    protected boolean distinct;

    protected List<Criteria> oredCriteria;

    public TakeTimeExample() {
        oredCriteria = new ArrayList<Criteria>();
    }

    public void setOrderByClause(String orderByClause) {
        this.orderByClause = orderByClause;
    }

    public String getOrderByClause() {
        return orderByClause;
    }

    public void setDistinct(boolean distinct) {
        this.distinct = distinct;
    }

    public boolean isDistinct() {
        return distinct;
    }

    public List<Criteria> getOredCriteria() {
        return oredCriteria;
    }

    public void or(Criteria criteria) {
        oredCriteria.add(criteria);
    }

    public Criteria or() {
        Criteria criteria = createCriteriaInternal();
        oredCriteria.add(criteria);
        return criteria;
    }

    public Criteria createCriteria() {
        Criteria criteria = createCriteriaInternal();
        if (oredCriteria.size() == 0) {
            oredCriteria.add(criteria);
        }
        return criteria;
    }

    protected Criteria createCriteriaInternal() {
        Criteria criteria = new Criteria();
        return criteria;
    }

    public void clear() {
        oredCriteria.clear();
        orderByClause = null;
        distinct = false;
    }

    protected abstract static class GeneratedCriteria {
        protected List<Criterion> criteria;

        protected GeneratedCriteria() {
            super();
            criteria = new ArrayList<Criterion>();
        }

        public boolean isValid() {
            return criteria.size() > 0;
        }

        public List<Criterion> getAllCriteria() {
            return criteria;
        }

        public List<Criterion> getCriteria() {
            return criteria;
        }

        protected void addCriterion(String condition) {
            if (condition == null) {
                throw new RuntimeException("Value for condition cannot be null");
            }
            criteria.add(new Criterion(condition));
        }

        protected void addCriterion(String condition, Object value, String property) {
            if (value == null) {
                throw new RuntimeException("Value for " + property + " cannot be null");
            }
            criteria.add(new Criterion(condition, value));
        }

        protected void addCriterion(String condition, Object value1, Object value2, String property) {
            if (value1 == null || value2 == null) {
                throw new RuntimeException("Between values for " + property + " cannot be null");
            }
            criteria.add(new Criterion(condition, value1, value2));
        }

        public Criteria andIdIsNull() {
            addCriterion("C_ID is null");
            return (Criteria) this;
        }

        public Criteria andIdIsNotNull() {
            addCriterion("C_ID is not null");
            return (Criteria) this;
        }

        public Criteria andIdEqualTo(Integer value) {
            addCriterion("C_ID =", value, "id");
            return (Criteria) this;
        }

        public Criteria andIdNotEqualTo(Integer value) {
            addCriterion("C_ID <>", value, "id");
            return (Criteria) this;
        }

        public Criteria andIdGreaterThan(Integer value) {
            addCriterion("C_ID >", value, "id");
            return (Criteria) this;
        }

        public Criteria andIdGreaterThanOrEqualTo(Integer value) {
            addCriterion("C_ID >=", value, "id");
            return (Criteria) this;
        }

        public Criteria andIdLessThan(Integer value) {
            addCriterion("C_ID <", value, "id");
            return (Criteria) this;
        }

        public Criteria andIdLessThanOrEqualTo(Integer value) {
            addCriterion("C_ID <=", value, "id");
            return (Criteria) this;
        }

        public Criteria andIdIn(List<Integer> values) {
            addCriterion("C_ID in", values, "id");
            return (Criteria) this;
        }

        public Criteria andIdNotIn(List<Integer> values) {
            addCriterion("C_ID not in", values, "id");
            return (Criteria) this;
        }

        public Criteria andIdBetween(Integer value1, Integer value2) {
            addCriterion("C_ID between", value1, value2, "id");
            return (Criteria) this;
        }

        public Criteria andIdNotBetween(Integer value1, Integer value2) {
            addCriterion("C_ID not between", value1, value2, "id");
            return (Criteria) this;
        }

        public Criteria andCompanyIsNull() {
            addCriterion("C_COMPANY is null");
            return (Criteria) this;
        }

        public Criteria andCompanyIsNotNull() {
            addCriterion("C_COMPANY is not null");
            return (Criteria) this;
        }

        public Criteria andCompanyEqualTo(String value) {
            addCriterion("C_COMPANY =", value, "company");
            return (Criteria) this;
        }

        public Criteria andCompanyNotEqualTo(String value) {
            addCriterion("C_COMPANY <>", value, "company");
            return (Criteria) this;
        }

        public Criteria andCompanyGreaterThan(String value) {
            addCriterion("C_COMPANY >", value, "company");
            return (Criteria) this;
        }

        public Criteria andCompanyGreaterThanOrEqualTo(String value) {
            addCriterion("C_COMPANY >=", value, "company");
            return (Criteria) this;
        }

        public Criteria andCompanyLessThan(String value) {
            addCriterion("C_COMPANY <", value, "company");
            return (Criteria) this;
        }

        public Criteria andCompanyLessThanOrEqualTo(String value) {
            addCriterion("C_COMPANY <=", value, "company");
            return (Criteria) this;
        }

        public Criteria andCompanyLike(String value) {
            addCriterion("C_COMPANY like", value, "company");
            return (Criteria) this;
        }

        public Criteria andCompanyNotLike(String value) {
            addCriterion("C_COMPANY not like", value, "company");
            return (Criteria) this;
        }

        public Criteria andCompanyIn(List<String> values) {
            addCriterion("C_COMPANY in", values, "company");
            return (Criteria) this;
        }

        public Criteria andCompanyNotIn(List<String> values) {
            addCriterion("C_COMPANY not in", values, "company");
            return (Criteria) this;
        }

        public Criteria andCompanyBetween(String value1, String value2) {
            addCriterion("C_COMPANY between", value1, value2, "company");
            return (Criteria) this;
        }

        public Criteria andCompanyNotBetween(String value1, String value2) {
            addCriterion("C_COMPANY not between", value1, value2, "company");
            return (Criteria) this;
        }

        public Criteria andNameIsNull() {
            addCriterion("C_NAME is null");
            return (Criteria) this;
        }

        public Criteria andNameIsNotNull() {
            addCriterion("C_NAME is not null");
            return (Criteria) this;
        }

        public Criteria andNameEqualTo(String value) {
            addCriterion("C_NAME =", value, "name");
            return (Criteria) this;
        }

        public Criteria andNameNotEqualTo(String value) {
            addCriterion("C_NAME <>", value, "name");
            return (Criteria) this;
        }

        public Criteria andNameGreaterThan(String value) {
            addCriterion("C_NAME >", value, "name");
            return (Criteria) this;
        }

        public Criteria andNameGreaterThanOrEqualTo(String value) {
            addCriterion("C_NAME >=", value, "name");
            return (Criteria) this;
        }

        public Criteria andNameLessThan(String value) {
            addCriterion("C_NAME <", value, "name");
            return (Criteria) this;
        }

        public Criteria andNameLessThanOrEqualTo(String value) {
            addCriterion("C_NAME <=", value, "name");
            return (Criteria) this;
        }

        public Criteria andNameLike(String value) {
            addCriterion("C_NAME like", value, "name");
            return (Criteria) this;
        }

        public Criteria andNameNotLike(String value) {
            addCriterion("C_NAME not like", value, "name");
            return (Criteria) this;
        }

        public Criteria andNameIn(List<String> values) {
            addCriterion("C_NAME in", values, "name");
            return (Criteria) this;
        }

        public Criteria andNameNotIn(List<String> values) {
            addCriterion("C_NAME not in", values, "name");
            return (Criteria) this;
        }

        public Criteria andNameBetween(String value1, String value2) {
            addCriterion("C_NAME between", value1, value2, "name");
            return (Criteria) this;
        }

        public Criteria andNameNotBetween(String value1, String value2) {
            addCriterion("C_NAME not between", value1, value2, "name");
            return (Criteria) this;
        }

        public Criteria andNormalDutyTimeIsNull() {
            addCriterion("C_NORMAL_DUTY_TIME is null");
            return (Criteria) this;
        }

        public Criteria andNormalDutyTimeIsNotNull() {
            addCriterion("C_NORMAL_DUTY_TIME is not null");
            return (Criteria) this;
        }

        public Criteria andNormalDutyTimeEqualTo(String value) {
            addCriterion("C_NORMAL_DUTY_TIME =", value, "normalDutyTime");
            return (Criteria) this;
        }

        public Criteria andNormalDutyTimeNotEqualTo(String value) {
            addCriterion("C_NORMAL_DUTY_TIME <>", value, "normalDutyTime");
            return (Criteria) this;
        }

        public Criteria andNormalDutyTimeGreaterThan(String value) {
            addCriterion("C_NORMAL_DUTY_TIME >", value, "normalDutyTime");
            return (Criteria) this;
        }

        public Criteria andNormalDutyTimeGreaterThanOrEqualTo(String value) {
            addCriterion("C_NORMAL_DUTY_TIME >=", value, "normalDutyTime");
            return (Criteria) this;
        }

        public Criteria andNormalDutyTimeLessThan(String value) {
            addCriterion("C_NORMAL_DUTY_TIME <", value, "normalDutyTime");
            return (Criteria) this;
        }

        public Criteria andNormalDutyTimeLessThanOrEqualTo(String value) {
            addCriterion("C_NORMAL_DUTY_TIME <=", value, "normalDutyTime");
            return (Criteria) this;
        }

        public Criteria andNormalDutyTimeLike(String value) {
            addCriterion("C_NORMAL_DUTY_TIME like", value, "normalDutyTime");
            return (Criteria) this;
        }

        public Criteria andNormalDutyTimeNotLike(String value) {
            addCriterion("C_NORMAL_DUTY_TIME not like", value, "normalDutyTime");
            return (Criteria) this;
        }

        public Criteria andNormalDutyTimeIn(List<String> values) {
            addCriterion("C_NORMAL_DUTY_TIME in", values, "normalDutyTime");
            return (Criteria) this;
        }

        public Criteria andNormalDutyTimeNotIn(List<String> values) {
            addCriterion("C_NORMAL_DUTY_TIME not in", values, "normalDutyTime");
            return (Criteria) this;
        }

        public Criteria andNormalDutyTimeBetween(String value1, String value2) {
            addCriterion("C_NORMAL_DUTY_TIME between", value1, value2, "normalDutyTime");
            return (Criteria) this;
        }

        public Criteria andNormalDutyTimeNotBetween(String value1, String value2) {
            addCriterion("C_NORMAL_DUTY_TIME not between", value1, value2, "normalDutyTime");
            return (Criteria) this;
        }

        public Criteria andNormalWorkTimeIsNull() {
            addCriterion("C_NORMAL_WORK_TIME is null");
            return (Criteria) this;
        }

        public Criteria andNormalWorkTimeIsNotNull() {
            addCriterion("C_NORMAL_WORK_TIME is not null");
            return (Criteria) this;
        }

        public Criteria andNormalWorkTimeEqualTo(String value) {
            addCriterion("C_NORMAL_WORK_TIME =", value, "normalWorkTime");
            return (Criteria) this;
        }

        public Criteria andNormalWorkTimeNotEqualTo(String value) {
            addCriterion("C_NORMAL_WORK_TIME <>", value, "normalWorkTime");
            return (Criteria) this;
        }

        public Criteria andNormalWorkTimeGreaterThan(String value) {
            addCriterion("C_NORMAL_WORK_TIME >", value, "normalWorkTime");
            return (Criteria) this;
        }

        public Criteria andNormalWorkTimeGreaterThanOrEqualTo(String value) {
            addCriterion("C_NORMAL_WORK_TIME >=", value, "normalWorkTime");
            return (Criteria) this;
        }

        public Criteria andNormalWorkTimeLessThan(String value) {
            addCriterion("C_NORMAL_WORK_TIME <", value, "normalWorkTime");
            return (Criteria) this;
        }

        public Criteria andNormalWorkTimeLessThanOrEqualTo(String value) {
            addCriterion("C_NORMAL_WORK_TIME <=", value, "normalWorkTime");
            return (Criteria) this;
        }

        public Criteria andNormalWorkTimeLike(String value) {
            addCriterion("C_NORMAL_WORK_TIME like", value, "normalWorkTime");
            return (Criteria) this;
        }

        public Criteria andNormalWorkTimeNotLike(String value) {
            addCriterion("C_NORMAL_WORK_TIME not like", value, "normalWorkTime");
            return (Criteria) this;
        }

        public Criteria andNormalWorkTimeIn(List<String> values) {
            addCriterion("C_NORMAL_WORK_TIME in", values, "normalWorkTime");
            return (Criteria) this;
        }

        public Criteria andNormalWorkTimeNotIn(List<String> values) {
            addCriterion("C_NORMAL_WORK_TIME not in", values, "normalWorkTime");
            return (Criteria) this;
        }

        public Criteria andNormalWorkTimeBetween(String value1, String value2) {
            addCriterion("C_NORMAL_WORK_TIME between", value1, value2, "normalWorkTime");
            return (Criteria) this;
        }

        public Criteria andNormalWorkTimeNotBetween(String value1, String value2) {
            addCriterion("C_NORMAL_WORK_TIME not between", value1, value2, "normalWorkTime");
            return (Criteria) this;
        }

        public Criteria andOperatingCompanyIsNull() {
            addCriterion("C_OPERATING_COMPANY is null");
            return (Criteria) this;
        }

        public Criteria andOperatingCompanyIsNotNull() {
            addCriterion("C_OPERATING_COMPANY is not null");
            return (Criteria) this;
        }

        public Criteria andOperatingCompanyEqualTo(String value) {
            addCriterion("C_OPERATING_COMPANY =", value, "operatingCompany");
            return (Criteria) this;
        }

        public Criteria andOperatingCompanyNotEqualTo(String value) {
            addCriterion("C_OPERATING_COMPANY <>", value, "operatingCompany");
            return (Criteria) this;
        }

        public Criteria andOperatingCompanyGreaterThan(String value) {
            addCriterion("C_OPERATING_COMPANY >", value, "operatingCompany");
            return (Criteria) this;
        }

        public Criteria andOperatingCompanyGreaterThanOrEqualTo(String value) {
            addCriterion("C_OPERATING_COMPANY >=", value, "operatingCompany");
            return (Criteria) this;
        }

        public Criteria andOperatingCompanyLessThan(String value) {
            addCriterion("C_OPERATING_COMPANY <", value, "operatingCompany");
            return (Criteria) this;
        }

        public Criteria andOperatingCompanyLessThanOrEqualTo(String value) {
            addCriterion("C_OPERATING_COMPANY <=", value, "operatingCompany");
            return (Criteria) this;
        }

        public Criteria andOperatingCompanyLike(String value) {
            addCriterion("C_OPERATING_COMPANY like", value, "operatingCompany");
            return (Criteria) this;
        }

        public Criteria andOperatingCompanyNotLike(String value) {
            addCriterion("C_OPERATING_COMPANY not like", value, "operatingCompany");
            return (Criteria) this;
        }

        public Criteria andOperatingCompanyIn(List<String> values) {
            addCriterion("C_OPERATING_COMPANY in", values, "operatingCompany");
            return (Criteria) this;
        }

        public Criteria andOperatingCompanyNotIn(List<String> values) {
            addCriterion("C_OPERATING_COMPANY not in", values, "operatingCompany");
            return (Criteria) this;
        }

        public Criteria andOperatingCompanyBetween(String value1, String value2) {
            addCriterion("C_OPERATING_COMPANY between", value1, value2, "operatingCompany");
            return (Criteria) this;
        }

        public Criteria andOperatingCompanyNotBetween(String value1, String value2) {
            addCriterion("C_OPERATING_COMPANY not between", value1, value2, "operatingCompany");
            return (Criteria) this;
        }

        public Criteria andOperatingTimeIsNull() {
            addCriterion("C_OPERATING_TIME is null");
            return (Criteria) this;
        }

        public Criteria andOperatingTimeIsNotNull() {
            addCriterion("C_OPERATING_TIME is not null");
            return (Criteria) this;
        }

        public Criteria andOperatingTimeEqualTo(Date value) {
            addCriterion("C_OPERATING_TIME =", value, "operatingTime");
            return (Criteria) this;
        }

        public Criteria andOperatingTimeNotEqualTo(Date value) {
            addCriterion("C_OPERATING_TIME <>", value, "operatingTime");
            return (Criteria) this;
        }

        public Criteria andOperatingTimeGreaterThan(Date value) {
            addCriterion("C_OPERATING_TIME >", value, "operatingTime");
            return (Criteria) this;
        }

        public Criteria andOperatingTimeGreaterThanOrEqualTo(Date value) {
            addCriterion("C_OPERATING_TIME >=", value, "operatingTime");
            return (Criteria) this;
        }

        public Criteria andOperatingTimeLessThan(Date value) {
            addCriterion("C_OPERATING_TIME <", value, "operatingTime");
            return (Criteria) this;
        }

        public Criteria andOperatingTimeLessThanOrEqualTo(Date value) {
            addCriterion("C_OPERATING_TIME <=", value, "operatingTime");
            return (Criteria) this;
        }

        public Criteria andOperatingTimeIn(List<Date> values) {
            addCriterion("C_OPERATING_TIME in", values, "operatingTime");
            return (Criteria) this;
        }

        public Criteria andOperatingTimeNotIn(List<Date> values) {
            addCriterion("C_OPERATING_TIME not in", values, "operatingTime");
            return (Criteria) this;
        }

        public Criteria andOperatingTimeBetween(Date value1, Date value2) {
            addCriterion("C_OPERATING_TIME between", value1, value2, "operatingTime");
            return (Criteria) this;
        }

        public Criteria andOperatingTimeNotBetween(Date value1, Date value2) {
            addCriterion("C_OPERATING_TIME not between", value1, value2, "operatingTime");
            return (Criteria) this;
        }

        public Criteria andOperatorIsNull() {
            addCriterion("C_OPERATOR is null");
            return (Criteria) this;
        }

        public Criteria andOperatorIsNotNull() {
            addCriterion("C_OPERATOR is not null");
            return (Criteria) this;
        }

        public Criteria andOperatorEqualTo(String value) {
            addCriterion("C_OPERATOR =", value, "operator");
            return (Criteria) this;
        }

        public Criteria andOperatorNotEqualTo(String value) {
            addCriterion("C_OPERATOR <>", value, "operator");
            return (Criteria) this;
        }

        public Criteria andOperatorGreaterThan(String value) {
            addCriterion("C_OPERATOR >", value, "operator");
            return (Criteria) this;
        }

        public Criteria andOperatorGreaterThanOrEqualTo(String value) {
            addCriterion("C_OPERATOR >=", value, "operator");
            return (Criteria) this;
        }

        public Criteria andOperatorLessThan(String value) {
            addCriterion("C_OPERATOR <", value, "operator");
            return (Criteria) this;
        }

        public Criteria andOperatorLessThanOrEqualTo(String value) {
            addCriterion("C_OPERATOR <=", value, "operator");
            return (Criteria) this;
        }

        public Criteria andOperatorLike(String value) {
            addCriterion("C_OPERATOR like", value, "operator");
            return (Criteria) this;
        }

        public Criteria andOperatorNotLike(String value) {
            addCriterion("C_OPERATOR not like", value, "operator");
            return (Criteria) this;
        }

        public Criteria andOperatorIn(List<String> values) {
            addCriterion("C_OPERATOR in", values, "operator");
            return (Criteria) this;
        }

        public Criteria andOperatorNotIn(List<String> values) {
            addCriterion("C_OPERATOR not in", values, "operator");
            return (Criteria) this;
        }

        public Criteria andOperatorBetween(String value1, String value2) {
            addCriterion("C_OPERATOR between", value1, value2, "operator");
            return (Criteria) this;
        }

        public Criteria andOperatorNotBetween(String value1, String value2) {
            addCriterion("C_OPERATOR not between", value1, value2, "operator");
            return (Criteria) this;
        }

        public Criteria andSatDutyTimeIsNull() {
            addCriterion("C_SAT_DUTY_TIME is null");
            return (Criteria) this;
        }

        public Criteria andSatDutyTimeIsNotNull() {
            addCriterion("C_SAT_DUTY_TIME is not null");
            return (Criteria) this;
        }

        public Criteria andSatDutyTimeEqualTo(String value) {
            addCriterion("C_SAT_DUTY_TIME =", value, "satDutyTime");
            return (Criteria) this;
        }

        public Criteria andSatDutyTimeNotEqualTo(String value) {
            addCriterion("C_SAT_DUTY_TIME <>", value, "satDutyTime");
            return (Criteria) this;
        }

        public Criteria andSatDutyTimeGreaterThan(String value) {
            addCriterion("C_SAT_DUTY_TIME >", value, "satDutyTime");
            return (Criteria) this;
        }

        public Criteria andSatDutyTimeGreaterThanOrEqualTo(String value) {
            addCriterion("C_SAT_DUTY_TIME >=", value, "satDutyTime");
            return (Criteria) this;
        }

        public Criteria andSatDutyTimeLessThan(String value) {
            addCriterion("C_SAT_DUTY_TIME <", value, "satDutyTime");
            return (Criteria) this;
        }

        public Criteria andSatDutyTimeLessThanOrEqualTo(String value) {
            addCriterion("C_SAT_DUTY_TIME <=", value, "satDutyTime");
            return (Criteria) this;
        }

        public Criteria andSatDutyTimeLike(String value) {
            addCriterion("C_SAT_DUTY_TIME like", value, "satDutyTime");
            return (Criteria) this;
        }

        public Criteria andSatDutyTimeNotLike(String value) {
            addCriterion("C_SAT_DUTY_TIME not like", value, "satDutyTime");
            return (Criteria) this;
        }

        public Criteria andSatDutyTimeIn(List<String> values) {
            addCriterion("C_SAT_DUTY_TIME in", values, "satDutyTime");
            return (Criteria) this;
        }

        public Criteria andSatDutyTimeNotIn(List<String> values) {
            addCriterion("C_SAT_DUTY_TIME not in", values, "satDutyTime");
            return (Criteria) this;
        }

        public Criteria andSatDutyTimeBetween(String value1, String value2) {
            addCriterion("C_SAT_DUTY_TIME between", value1, value2, "satDutyTime");
            return (Criteria) this;
        }

        public Criteria andSatDutyTimeNotBetween(String value1, String value2) {
            addCriterion("C_SAT_DUTY_TIME not between", value1, value2, "satDutyTime");
            return (Criteria) this;
        }

        public Criteria andSatWorkTimeIsNull() {
            addCriterion("C_SAT_WORK_TIME is null");
            return (Criteria) this;
        }

        public Criteria andSatWorkTimeIsNotNull() {
            addCriterion("C_SAT_WORK_TIME is not null");
            return (Criteria) this;
        }

        public Criteria andSatWorkTimeEqualTo(String value) {
            addCriterion("C_SAT_WORK_TIME =", value, "satWorkTime");
            return (Criteria) this;
        }

        public Criteria andSatWorkTimeNotEqualTo(String value) {
            addCriterion("C_SAT_WORK_TIME <>", value, "satWorkTime");
            return (Criteria) this;
        }

        public Criteria andSatWorkTimeGreaterThan(String value) {
            addCriterion("C_SAT_WORK_TIME >", value, "satWorkTime");
            return (Criteria) this;
        }

        public Criteria andSatWorkTimeGreaterThanOrEqualTo(String value) {
            addCriterion("C_SAT_WORK_TIME >=", value, "satWorkTime");
            return (Criteria) this;
        }

        public Criteria andSatWorkTimeLessThan(String value) {
            addCriterion("C_SAT_WORK_TIME <", value, "satWorkTime");
            return (Criteria) this;
        }

        public Criteria andSatWorkTimeLessThanOrEqualTo(String value) {
            addCriterion("C_SAT_WORK_TIME <=", value, "satWorkTime");
            return (Criteria) this;
        }

        public Criteria andSatWorkTimeLike(String value) {
            addCriterion("C_SAT_WORK_TIME like", value, "satWorkTime");
            return (Criteria) this;
        }

        public Criteria andSatWorkTimeNotLike(String value) {
            addCriterion("C_SAT_WORK_TIME not like", value, "satWorkTime");
            return (Criteria) this;
        }

        public Criteria andSatWorkTimeIn(List<String> values) {
            addCriterion("C_SAT_WORK_TIME in", values, "satWorkTime");
            return (Criteria) this;
        }

        public Criteria andSatWorkTimeNotIn(List<String> values) {
            addCriterion("C_SAT_WORK_TIME not in", values, "satWorkTime");
            return (Criteria) this;
        }

        public Criteria andSatWorkTimeBetween(String value1, String value2) {
            addCriterion("C_SAT_WORK_TIME between", value1, value2, "satWorkTime");
            return (Criteria) this;
        }

        public Criteria andSatWorkTimeNotBetween(String value1, String value2) {
            addCriterion("C_SAT_WORK_TIME not between", value1, value2, "satWorkTime");
            return (Criteria) this;
        }

        public Criteria andStatusIsNull() {
            addCriterion("C_STATUS is null");
            return (Criteria) this;
        }

        public Criteria andStatusIsNotNull() {
            addCriterion("C_STATUS is not null");
            return (Criteria) this;
        }

        public Criteria andStatusEqualTo(String value) {
            addCriterion("C_STATUS =", value, "status");
            return (Criteria) this;
        }

        public Criteria andStatusNotEqualTo(String value) {
            addCriterion("C_STATUS <>", value, "status");
            return (Criteria) this;
        }

        public Criteria andStatusGreaterThan(String value) {
            addCriterion("C_STATUS >", value, "status");
            return (Criteria) this;
        }

        public Criteria andStatusGreaterThanOrEqualTo(String value) {
            addCriterion("C_STATUS >=", value, "status");
            return (Criteria) this;
        }

        public Criteria andStatusLessThan(String value) {
            addCriterion("C_STATUS <", value, "status");
            return (Criteria) this;
        }

        public Criteria andStatusLessThanOrEqualTo(String value) {
            addCriterion("C_STATUS <=", value, "status");
            return (Criteria) this;
        }

        public Criteria andStatusLike(String value) {
            addCriterion("C_STATUS like", value, "status");
            return (Criteria) this;
        }

        public Criteria andStatusNotLike(String value) {
            addCriterion("C_STATUS not like", value, "status");
            return (Criteria) this;
        }

        public Criteria andStatusIn(List<String> values) {
            addCriterion("C_STATUS in", values, "status");
            return (Criteria) this;
        }

        public Criteria andStatusNotIn(List<String> values) {
            addCriterion("C_STATUS not in", values, "status");
            return (Criteria) this;
        }

        public Criteria andStatusBetween(String value1, String value2) {
            addCriterion("C_STATUS between", value1, value2, "status");
            return (Criteria) this;
        }

        public Criteria andStatusNotBetween(String value1, String value2) {
            addCriterion("C_STATUS not between", value1, value2, "status");
            return (Criteria) this;
        }

        public Criteria andSunDutyTimeIsNull() {
            addCriterion("C_SUN_DUTY_TIME is null");
            return (Criteria) this;
        }

        public Criteria andSunDutyTimeIsNotNull() {
            addCriterion("C_SUN_DUTY_TIME is not null");
            return (Criteria) this;
        }

        public Criteria andSunDutyTimeEqualTo(String value) {
            addCriterion("C_SUN_DUTY_TIME =", value, "sunDutyTime");
            return (Criteria) this;
        }

        public Criteria andSunDutyTimeNotEqualTo(String value) {
            addCriterion("C_SUN_DUTY_TIME <>", value, "sunDutyTime");
            return (Criteria) this;
        }

        public Criteria andSunDutyTimeGreaterThan(String value) {
            addCriterion("C_SUN_DUTY_TIME >", value, "sunDutyTime");
            return (Criteria) this;
        }

        public Criteria andSunDutyTimeGreaterThanOrEqualTo(String value) {
            addCriterion("C_SUN_DUTY_TIME >=", value, "sunDutyTime");
            return (Criteria) this;
        }

        public Criteria andSunDutyTimeLessThan(String value) {
            addCriterion("C_SUN_DUTY_TIME <", value, "sunDutyTime");
            return (Criteria) this;
        }

        public Criteria andSunDutyTimeLessThanOrEqualTo(String value) {
            addCriterion("C_SUN_DUTY_TIME <=", value, "sunDutyTime");
            return (Criteria) this;
        }

        public Criteria andSunDutyTimeLike(String value) {
            addCriterion("C_SUN_DUTY_TIME like", value, "sunDutyTime");
            return (Criteria) this;
        }

        public Criteria andSunDutyTimeNotLike(String value) {
            addCriterion("C_SUN_DUTY_TIME not like", value, "sunDutyTime");
            return (Criteria) this;
        }

        public Criteria andSunDutyTimeIn(List<String> values) {
            addCriterion("C_SUN_DUTY_TIME in", values, "sunDutyTime");
            return (Criteria) this;
        }

        public Criteria andSunDutyTimeNotIn(List<String> values) {
            addCriterion("C_SUN_DUTY_TIME not in", values, "sunDutyTime");
            return (Criteria) this;
        }

        public Criteria andSunDutyTimeBetween(String value1, String value2) {
            addCriterion("C_SUN_DUTY_TIME between", value1, value2, "sunDutyTime");
            return (Criteria) this;
        }

        public Criteria andSunDutyTimeNotBetween(String value1, String value2) {
            addCriterion("C_SUN_DUTY_TIME not between", value1, value2, "sunDutyTime");
            return (Criteria) this;
        }

        public Criteria andSunWorkTimeIsNull() {
            addCriterion("C_SUN_WORK_TIME is null");
            return (Criteria) this;
        }

        public Criteria andSunWorkTimeIsNotNull() {
            addCriterion("C_SUN_WORK_TIME is not null");
            return (Criteria) this;
        }

        public Criteria andSunWorkTimeEqualTo(String value) {
            addCriterion("C_SUN_WORK_TIME =", value, "sunWorkTime");
            return (Criteria) this;
        }

        public Criteria andSunWorkTimeNotEqualTo(String value) {
            addCriterion("C_SUN_WORK_TIME <>", value, "sunWorkTime");
            return (Criteria) this;
        }

        public Criteria andSunWorkTimeGreaterThan(String value) {
            addCriterion("C_SUN_WORK_TIME >", value, "sunWorkTime");
            return (Criteria) this;
        }

        public Criteria andSunWorkTimeGreaterThanOrEqualTo(String value) {
            addCriterion("C_SUN_WORK_TIME >=", value, "sunWorkTime");
            return (Criteria) this;
        }

        public Criteria andSunWorkTimeLessThan(String value) {
            addCriterion("C_SUN_WORK_TIME <", value, "sunWorkTime");
            return (Criteria) this;
        }

        public Criteria andSunWorkTimeLessThanOrEqualTo(String value) {
            addCriterion("C_SUN_WORK_TIME <=", value, "sunWorkTime");
            return (Criteria) this;
        }

        public Criteria andSunWorkTimeLike(String value) {
            addCriterion("C_SUN_WORK_TIME like", value, "sunWorkTime");
            return (Criteria) this;
        }

        public Criteria andSunWorkTimeNotLike(String value) {
            addCriterion("C_SUN_WORK_TIME not like", value, "sunWorkTime");
            return (Criteria) this;
        }

        public Criteria andSunWorkTimeIn(List<String> values) {
            addCriterion("C_SUN_WORK_TIME in", values, "sunWorkTime");
            return (Criteria) this;
        }

        public Criteria andSunWorkTimeNotIn(List<String> values) {
            addCriterion("C_SUN_WORK_TIME not in", values, "sunWorkTime");
            return (Criteria) this;
        }

        public Criteria andSunWorkTimeBetween(String value1, String value2) {
            addCriterion("C_SUN_WORK_TIME between", value1, value2, "sunWorkTime");
            return (Criteria) this;
        }

        public Criteria andSunWorkTimeNotBetween(String value1, String value2) {
            addCriterion("C_SUN_WORK_TIME not between", value1, value2, "sunWorkTime");
            return (Criteria) this;
        }
    }

    public static class Criteria extends GeneratedCriteria {

        protected Criteria() {
            super();
        }
    }

    public static class Criterion {
        private String condition;

        private Object value;

        private Object secondValue;

        private boolean noValue;

        private boolean singleValue;

        private boolean betweenValue;

        private boolean listValue;

        private String typeHandler;

        public String getCondition() {
            return condition;
        }

        public Object getValue() {
            return value;
        }

        public Object getSecondValue() {
            return secondValue;
        }

        public boolean isNoValue() {
            return noValue;
        }

        public boolean isSingleValue() {
            return singleValue;
        }

        public boolean isBetweenValue() {
            return betweenValue;
        }

        public boolean isListValue() {
            return listValue;
        }

        public String getTypeHandler() {
            return typeHandler;
        }

        protected Criterion(String condition) {
            super();
            this.condition = condition;
            this.typeHandler = null;
            this.noValue = true;
        }

        protected Criterion(String condition, Object value, String typeHandler) {
            super();
            this.condition = condition;
            this.value = value;
            this.typeHandler = typeHandler;
            if (value instanceof List<?>) {
                this.listValue = true;
            } else {
                this.singleValue = true;
            }
        }

        protected Criterion(String condition, Object value) {
            this(condition, value, null);
        }

        protected Criterion(String condition, Object value, Object secondValue, String typeHandler) {
            super();
            this.condition = condition;
            this.value = value;
            this.secondValue = secondValue;
            this.typeHandler = typeHandler;
            this.betweenValue = true;
        }

        protected Criterion(String condition, Object value, Object secondValue) {
            this(condition, value, secondValue, null);
        }
    }
}