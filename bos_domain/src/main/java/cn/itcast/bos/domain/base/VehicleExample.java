package cn.itcast.bos.domain.base;

import java.util.ArrayList;
import java.util.List;

public class VehicleExample {
    protected String orderByClause;

    protected boolean distinct;

    protected List<Criteria> oredCriteria;

    public VehicleExample() {
        oredCriteria = new ArrayList<Criteria>();
    }

    public void setOrderByClause(String orderByClause) {
        this.orderByClause = orderByClause;
    }

    public String getOrderByClause() {
        return orderByClause;
    }

    public void setDistinct(boolean distinct) {
        this.distinct = distinct;
    }

    public boolean isDistinct() {
        return distinct;
    }

    public List<Criteria> getOredCriteria() {
        return oredCriteria;
    }

    public void or(Criteria criteria) {
        oredCriteria.add(criteria);
    }

    public Criteria or() {
        Criteria criteria = createCriteriaInternal();
        oredCriteria.add(criteria);
        return criteria;
    }

    public Criteria createCriteria() {
        Criteria criteria = createCriteriaInternal();
        if (oredCriteria.size() == 0) {
            oredCriteria.add(criteria);
        }
        return criteria;
    }

    protected Criteria createCriteriaInternal() {
        Criteria criteria = new Criteria();
        return criteria;
    }

    public void clear() {
        oredCriteria.clear();
        orderByClause = null;
        distinct = false;
    }

    protected abstract static class GeneratedCriteria {
        protected List<Criterion> criteria;

        protected GeneratedCriteria() {
            super();
            criteria = new ArrayList<Criterion>();
        }

        public boolean isValid() {
            return criteria.size() > 0;
        }

        public List<Criterion> getAllCriteria() {
            return criteria;
        }

        public List<Criterion> getCriteria() {
            return criteria;
        }

        protected void addCriterion(String condition) {
            if (condition == null) {
                throw new RuntimeException("Value for condition cannot be null");
            }
            criteria.add(new Criterion(condition));
        }

        protected void addCriterion(String condition, Object value, String property) {
            if (value == null) {
                throw new RuntimeException("Value for " + property + " cannot be null");
            }
            criteria.add(new Criterion(condition, value));
        }

        protected void addCriterion(String condition, Object value1, Object value2, String property) {
            if (value1 == null || value2 == null) {
                throw new RuntimeException("Between values for " + property + " cannot be null");
            }
            criteria.add(new Criterion(condition, value1, value2));
        }

        public Criteria andIdIsNull() {
            addCriterion("C_ID is null");
            return (Criteria) this;
        }

        public Criteria andIdIsNotNull() {
            addCriterion("C_ID is not null");
            return (Criteria) this;
        }

        public Criteria andIdEqualTo(Integer value) {
            addCriterion("C_ID =", value, "id");
            return (Criteria) this;
        }

        public Criteria andIdNotEqualTo(Integer value) {
            addCriterion("C_ID <>", value, "id");
            return (Criteria) this;
        }

        public Criteria andIdGreaterThan(Integer value) {
            addCriterion("C_ID >", value, "id");
            return (Criteria) this;
        }

        public Criteria andIdGreaterThanOrEqualTo(Integer value) {
            addCriterion("C_ID >=", value, "id");
            return (Criteria) this;
        }

        public Criteria andIdLessThan(Integer value) {
            addCriterion("C_ID <", value, "id");
            return (Criteria) this;
        }

        public Criteria andIdLessThanOrEqualTo(Integer value) {
            addCriterion("C_ID <=", value, "id");
            return (Criteria) this;
        }

        public Criteria andIdIn(List<Integer> values) {
            addCriterion("C_ID in", values, "id");
            return (Criteria) this;
        }

        public Criteria andIdNotIn(List<Integer> values) {
            addCriterion("C_ID not in", values, "id");
            return (Criteria) this;
        }

        public Criteria andIdBetween(Integer value1, Integer value2) {
            addCriterion("C_ID between", value1, value2, "id");
            return (Criteria) this;
        }

        public Criteria andIdNotBetween(Integer value1, Integer value2) {
            addCriterion("C_ID not between", value1, value2, "id");
            return (Criteria) this;
        }

        public Criteria andDriverIsNull() {
            addCriterion("C_DRIVER is null");
            return (Criteria) this;
        }

        public Criteria andDriverIsNotNull() {
            addCriterion("C_DRIVER is not null");
            return (Criteria) this;
        }

        public Criteria andDriverEqualTo(String value) {
            addCriterion("C_DRIVER =", value, "driver");
            return (Criteria) this;
        }

        public Criteria andDriverNotEqualTo(String value) {
            addCriterion("C_DRIVER <>", value, "driver");
            return (Criteria) this;
        }

        public Criteria andDriverGreaterThan(String value) {
            addCriterion("C_DRIVER >", value, "driver");
            return (Criteria) this;
        }

        public Criteria andDriverGreaterThanOrEqualTo(String value) {
            addCriterion("C_DRIVER >=", value, "driver");
            return (Criteria) this;
        }

        public Criteria andDriverLessThan(String value) {
            addCriterion("C_DRIVER <", value, "driver");
            return (Criteria) this;
        }

        public Criteria andDriverLessThanOrEqualTo(String value) {
            addCriterion("C_DRIVER <=", value, "driver");
            return (Criteria) this;
        }

        public Criteria andDriverLike(String value) {
            addCriterion("C_DRIVER like", value, "driver");
            return (Criteria) this;
        }

        public Criteria andDriverNotLike(String value) {
            addCriterion("C_DRIVER not like", value, "driver");
            return (Criteria) this;
        }

        public Criteria andDriverIn(List<String> values) {
            addCriterion("C_DRIVER in", values, "driver");
            return (Criteria) this;
        }

        public Criteria andDriverNotIn(List<String> values) {
            addCriterion("C_DRIVER not in", values, "driver");
            return (Criteria) this;
        }

        public Criteria andDriverBetween(String value1, String value2) {
            addCriterion("C_DRIVER between", value1, value2, "driver");
            return (Criteria) this;
        }

        public Criteria andDriverNotBetween(String value1, String value2) {
            addCriterion("C_DRIVER not between", value1, value2, "driver");
            return (Criteria) this;
        }

        public Criteria andRemarkIsNull() {
            addCriterion("C_REMARK is null");
            return (Criteria) this;
        }

        public Criteria andRemarkIsNotNull() {
            addCriterion("C_REMARK is not null");
            return (Criteria) this;
        }

        public Criteria andRemarkEqualTo(String value) {
            addCriterion("C_REMARK =", value, "remark");
            return (Criteria) this;
        }

        public Criteria andRemarkNotEqualTo(String value) {
            addCriterion("C_REMARK <>", value, "remark");
            return (Criteria) this;
        }

        public Criteria andRemarkGreaterThan(String value) {
            addCriterion("C_REMARK >", value, "remark");
            return (Criteria) this;
        }

        public Criteria andRemarkGreaterThanOrEqualTo(String value) {
            addCriterion("C_REMARK >=", value, "remark");
            return (Criteria) this;
        }

        public Criteria andRemarkLessThan(String value) {
            addCriterion("C_REMARK <", value, "remark");
            return (Criteria) this;
        }

        public Criteria andRemarkLessThanOrEqualTo(String value) {
            addCriterion("C_REMARK <=", value, "remark");
            return (Criteria) this;
        }

        public Criteria andRemarkLike(String value) {
            addCriterion("C_REMARK like", value, "remark");
            return (Criteria) this;
        }

        public Criteria andRemarkNotLike(String value) {
            addCriterion("C_REMARK not like", value, "remark");
            return (Criteria) this;
        }

        public Criteria andRemarkIn(List<String> values) {
            addCriterion("C_REMARK in", values, "remark");
            return (Criteria) this;
        }

        public Criteria andRemarkNotIn(List<String> values) {
            addCriterion("C_REMARK not in", values, "remark");
            return (Criteria) this;
        }

        public Criteria andRemarkBetween(String value1, String value2) {
            addCriterion("C_REMARK between", value1, value2, "remark");
            return (Criteria) this;
        }

        public Criteria andRemarkNotBetween(String value1, String value2) {
            addCriterion("C_REMARK not between", value1, value2, "remark");
            return (Criteria) this;
        }

        public Criteria andRouteNameIsNull() {
            addCriterion("C_ROUTE_NAME is null");
            return (Criteria) this;
        }

        public Criteria andRouteNameIsNotNull() {
            addCriterion("C_ROUTE_NAME is not null");
            return (Criteria) this;
        }

        public Criteria andRouteNameEqualTo(String value) {
            addCriterion("C_ROUTE_NAME =", value, "routeName");
            return (Criteria) this;
        }

        public Criteria andRouteNameNotEqualTo(String value) {
            addCriterion("C_ROUTE_NAME <>", value, "routeName");
            return (Criteria) this;
        }

        public Criteria andRouteNameGreaterThan(String value) {
            addCriterion("C_ROUTE_NAME >", value, "routeName");
            return (Criteria) this;
        }

        public Criteria andRouteNameGreaterThanOrEqualTo(String value) {
            addCriterion("C_ROUTE_NAME >=", value, "routeName");
            return (Criteria) this;
        }

        public Criteria andRouteNameLessThan(String value) {
            addCriterion("C_ROUTE_NAME <", value, "routeName");
            return (Criteria) this;
        }

        public Criteria andRouteNameLessThanOrEqualTo(String value) {
            addCriterion("C_ROUTE_NAME <=", value, "routeName");
            return (Criteria) this;
        }

        public Criteria andRouteNameLike(String value) {
            addCriterion("C_ROUTE_NAME like", value, "routeName");
            return (Criteria) this;
        }

        public Criteria andRouteNameNotLike(String value) {
            addCriterion("C_ROUTE_NAME not like", value, "routeName");
            return (Criteria) this;
        }

        public Criteria andRouteNameIn(List<String> values) {
            addCriterion("C_ROUTE_NAME in", values, "routeName");
            return (Criteria) this;
        }

        public Criteria andRouteNameNotIn(List<String> values) {
            addCriterion("C_ROUTE_NAME not in", values, "routeName");
            return (Criteria) this;
        }

        public Criteria andRouteNameBetween(String value1, String value2) {
            addCriterion("C_ROUTE_NAME between", value1, value2, "routeName");
            return (Criteria) this;
        }

        public Criteria andRouteNameNotBetween(String value1, String value2) {
            addCriterion("C_ROUTE_NAME not between", value1, value2, "routeName");
            return (Criteria) this;
        }

        public Criteria andRouteTypeIsNull() {
            addCriterion("C_ROUTE_TYPE is null");
            return (Criteria) this;
        }

        public Criteria andRouteTypeIsNotNull() {
            addCriterion("C_ROUTE_TYPE is not null");
            return (Criteria) this;
        }

        public Criteria andRouteTypeEqualTo(String value) {
            addCriterion("C_ROUTE_TYPE =", value, "routeType");
            return (Criteria) this;
        }

        public Criteria andRouteTypeNotEqualTo(String value) {
            addCriterion("C_ROUTE_TYPE <>", value, "routeType");
            return (Criteria) this;
        }

        public Criteria andRouteTypeGreaterThan(String value) {
            addCriterion("C_ROUTE_TYPE >", value, "routeType");
            return (Criteria) this;
        }

        public Criteria andRouteTypeGreaterThanOrEqualTo(String value) {
            addCriterion("C_ROUTE_TYPE >=", value, "routeType");
            return (Criteria) this;
        }

        public Criteria andRouteTypeLessThan(String value) {
            addCriterion("C_ROUTE_TYPE <", value, "routeType");
            return (Criteria) this;
        }

        public Criteria andRouteTypeLessThanOrEqualTo(String value) {
            addCriterion("C_ROUTE_TYPE <=", value, "routeType");
            return (Criteria) this;
        }

        public Criteria andRouteTypeLike(String value) {
            addCriterion("C_ROUTE_TYPE like", value, "routeType");
            return (Criteria) this;
        }

        public Criteria andRouteTypeNotLike(String value) {
            addCriterion("C_ROUTE_TYPE not like", value, "routeType");
            return (Criteria) this;
        }

        public Criteria andRouteTypeIn(List<String> values) {
            addCriterion("C_ROUTE_TYPE in", values, "routeType");
            return (Criteria) this;
        }

        public Criteria andRouteTypeNotIn(List<String> values) {
            addCriterion("C_ROUTE_TYPE not in", values, "routeType");
            return (Criteria) this;
        }

        public Criteria andRouteTypeBetween(String value1, String value2) {
            addCriterion("C_ROUTE_TYPE between", value1, value2, "routeType");
            return (Criteria) this;
        }

        public Criteria andRouteTypeNotBetween(String value1, String value2) {
            addCriterion("C_ROUTE_TYPE not between", value1, value2, "routeType");
            return (Criteria) this;
        }

        public Criteria andSnipperIsNull() {
            addCriterion("C_SNIPPER is null");
            return (Criteria) this;
        }

        public Criteria andSnipperIsNotNull() {
            addCriterion("C_SNIPPER is not null");
            return (Criteria) this;
        }

        public Criteria andSnipperEqualTo(String value) {
            addCriterion("C_SNIPPER =", value, "snipper");
            return (Criteria) this;
        }

        public Criteria andSnipperNotEqualTo(String value) {
            addCriterion("C_SNIPPER <>", value, "snipper");
            return (Criteria) this;
        }

        public Criteria andSnipperGreaterThan(String value) {
            addCriterion("C_SNIPPER >", value, "snipper");
            return (Criteria) this;
        }

        public Criteria andSnipperGreaterThanOrEqualTo(String value) {
            addCriterion("C_SNIPPER >=", value, "snipper");
            return (Criteria) this;
        }

        public Criteria andSnipperLessThan(String value) {
            addCriterion("C_SNIPPER <", value, "snipper");
            return (Criteria) this;
        }

        public Criteria andSnipperLessThanOrEqualTo(String value) {
            addCriterion("C_SNIPPER <=", value, "snipper");
            return (Criteria) this;
        }

        public Criteria andSnipperLike(String value) {
            addCriterion("C_SNIPPER like", value, "snipper");
            return (Criteria) this;
        }

        public Criteria andSnipperNotLike(String value) {
            addCriterion("C_SNIPPER not like", value, "snipper");
            return (Criteria) this;
        }

        public Criteria andSnipperIn(List<String> values) {
            addCriterion("C_SNIPPER in", values, "snipper");
            return (Criteria) this;
        }

        public Criteria andSnipperNotIn(List<String> values) {
            addCriterion("C_SNIPPER not in", values, "snipper");
            return (Criteria) this;
        }

        public Criteria andSnipperBetween(String value1, String value2) {
            addCriterion("C_SNIPPER between", value1, value2, "snipper");
            return (Criteria) this;
        }

        public Criteria andSnipperNotBetween(String value1, String value2) {
            addCriterion("C_SNIPPER not between", value1, value2, "snipper");
            return (Criteria) this;
        }

        public Criteria andTelephoneIsNull() {
            addCriterion("C_TELEPHONE is null");
            return (Criteria) this;
        }

        public Criteria andTelephoneIsNotNull() {
            addCriterion("C_TELEPHONE is not null");
            return (Criteria) this;
        }

        public Criteria andTelephoneEqualTo(String value) {
            addCriterion("C_TELEPHONE =", value, "telephone");
            return (Criteria) this;
        }

        public Criteria andTelephoneNotEqualTo(String value) {
            addCriterion("C_TELEPHONE <>", value, "telephone");
            return (Criteria) this;
        }

        public Criteria andTelephoneGreaterThan(String value) {
            addCriterion("C_TELEPHONE >", value, "telephone");
            return (Criteria) this;
        }

        public Criteria andTelephoneGreaterThanOrEqualTo(String value) {
            addCriterion("C_TELEPHONE >=", value, "telephone");
            return (Criteria) this;
        }

        public Criteria andTelephoneLessThan(String value) {
            addCriterion("C_TELEPHONE <", value, "telephone");
            return (Criteria) this;
        }

        public Criteria andTelephoneLessThanOrEqualTo(String value) {
            addCriterion("C_TELEPHONE <=", value, "telephone");
            return (Criteria) this;
        }

        public Criteria andTelephoneLike(String value) {
            addCriterion("C_TELEPHONE like", value, "telephone");
            return (Criteria) this;
        }

        public Criteria andTelephoneNotLike(String value) {
            addCriterion("C_TELEPHONE not like", value, "telephone");
            return (Criteria) this;
        }

        public Criteria andTelephoneIn(List<String> values) {
            addCriterion("C_TELEPHONE in", values, "telephone");
            return (Criteria) this;
        }

        public Criteria andTelephoneNotIn(List<String> values) {
            addCriterion("C_TELEPHONE not in", values, "telephone");
            return (Criteria) this;
        }

        public Criteria andTelephoneBetween(String value1, String value2) {
            addCriterion("C_TELEPHONE between", value1, value2, "telephone");
            return (Criteria) this;
        }

        public Criteria andTelephoneNotBetween(String value1, String value2) {
            addCriterion("C_TELEPHONE not between", value1, value2, "telephone");
            return (Criteria) this;
        }

        public Criteria andTonIsNull() {
            addCriterion("C_TON is null");
            return (Criteria) this;
        }

        public Criteria andTonIsNotNull() {
            addCriterion("C_TON is not null");
            return (Criteria) this;
        }

        public Criteria andTonEqualTo(Integer value) {
            addCriterion("C_TON =", value, "ton");
            return (Criteria) this;
        }

        public Criteria andTonNotEqualTo(Integer value) {
            addCriterion("C_TON <>", value, "ton");
            return (Criteria) this;
        }

        public Criteria andTonGreaterThan(Integer value) {
            addCriterion("C_TON >", value, "ton");
            return (Criteria) this;
        }

        public Criteria andTonGreaterThanOrEqualTo(Integer value) {
            addCriterion("C_TON >=", value, "ton");
            return (Criteria) this;
        }

        public Criteria andTonLessThan(Integer value) {
            addCriterion("C_TON <", value, "ton");
            return (Criteria) this;
        }

        public Criteria andTonLessThanOrEqualTo(Integer value) {
            addCriterion("C_TON <=", value, "ton");
            return (Criteria) this;
        }

        public Criteria andTonIn(List<Integer> values) {
            addCriterion("C_TON in", values, "ton");
            return (Criteria) this;
        }

        public Criteria andTonNotIn(List<Integer> values) {
            addCriterion("C_TON not in", values, "ton");
            return (Criteria) this;
        }

        public Criteria andTonBetween(Integer value1, Integer value2) {
            addCriterion("C_TON between", value1, value2, "ton");
            return (Criteria) this;
        }

        public Criteria andTonNotBetween(Integer value1, Integer value2) {
            addCriterion("C_TON not between", value1, value2, "ton");
            return (Criteria) this;
        }

        public Criteria andVehicleNumIsNull() {
            addCriterion("C_VEHICLE_NUM is null");
            return (Criteria) this;
        }

        public Criteria andVehicleNumIsNotNull() {
            addCriterion("C_VEHICLE_NUM is not null");
            return (Criteria) this;
        }

        public Criteria andVehicleNumEqualTo(String value) {
            addCriterion("C_VEHICLE_NUM =", value, "vehicleNum");
            return (Criteria) this;
        }

        public Criteria andVehicleNumNotEqualTo(String value) {
            addCriterion("C_VEHICLE_NUM <>", value, "vehicleNum");
            return (Criteria) this;
        }

        public Criteria andVehicleNumGreaterThan(String value) {
            addCriterion("C_VEHICLE_NUM >", value, "vehicleNum");
            return (Criteria) this;
        }

        public Criteria andVehicleNumGreaterThanOrEqualTo(String value) {
            addCriterion("C_VEHICLE_NUM >=", value, "vehicleNum");
            return (Criteria) this;
        }

        public Criteria andVehicleNumLessThan(String value) {
            addCriterion("C_VEHICLE_NUM <", value, "vehicleNum");
            return (Criteria) this;
        }

        public Criteria andVehicleNumLessThanOrEqualTo(String value) {
            addCriterion("C_VEHICLE_NUM <=", value, "vehicleNum");
            return (Criteria) this;
        }

        public Criteria andVehicleNumLike(String value) {
            addCriterion("C_VEHICLE_NUM like", value, "vehicleNum");
            return (Criteria) this;
        }

        public Criteria andVehicleNumNotLike(String value) {
            addCriterion("C_VEHICLE_NUM not like", value, "vehicleNum");
            return (Criteria) this;
        }

        public Criteria andVehicleNumIn(List<String> values) {
            addCriterion("C_VEHICLE_NUM in", values, "vehicleNum");
            return (Criteria) this;
        }

        public Criteria andVehicleNumNotIn(List<String> values) {
            addCriterion("C_VEHICLE_NUM not in", values, "vehicleNum");
            return (Criteria) this;
        }

        public Criteria andVehicleNumBetween(String value1, String value2) {
            addCriterion("C_VEHICLE_NUM between", value1, value2, "vehicleNum");
            return (Criteria) this;
        }

        public Criteria andVehicleNumNotBetween(String value1, String value2) {
            addCriterion("C_VEHICLE_NUM not between", value1, value2, "vehicleNum");
            return (Criteria) this;
        }

        public Criteria andVehicleTypeIsNull() {
            addCriterion("C_VEHICLE_TYPE is null");
            return (Criteria) this;
        }

        public Criteria andVehicleTypeIsNotNull() {
            addCriterion("C_VEHICLE_TYPE is not null");
            return (Criteria) this;
        }

        public Criteria andVehicleTypeEqualTo(String value) {
            addCriterion("C_VEHICLE_TYPE =", value, "vehicleType");
            return (Criteria) this;
        }

        public Criteria andVehicleTypeNotEqualTo(String value) {
            addCriterion("C_VEHICLE_TYPE <>", value, "vehicleType");
            return (Criteria) this;
        }

        public Criteria andVehicleTypeGreaterThan(String value) {
            addCriterion("C_VEHICLE_TYPE >", value, "vehicleType");
            return (Criteria) this;
        }

        public Criteria andVehicleTypeGreaterThanOrEqualTo(String value) {
            addCriterion("C_VEHICLE_TYPE >=", value, "vehicleType");
            return (Criteria) this;
        }

        public Criteria andVehicleTypeLessThan(String value) {
            addCriterion("C_VEHICLE_TYPE <", value, "vehicleType");
            return (Criteria) this;
        }

        public Criteria andVehicleTypeLessThanOrEqualTo(String value) {
            addCriterion("C_VEHICLE_TYPE <=", value, "vehicleType");
            return (Criteria) this;
        }

        public Criteria andVehicleTypeLike(String value) {
            addCriterion("C_VEHICLE_TYPE like", value, "vehicleType");
            return (Criteria) this;
        }

        public Criteria andVehicleTypeNotLike(String value) {
            addCriterion("C_VEHICLE_TYPE not like", value, "vehicleType");
            return (Criteria) this;
        }

        public Criteria andVehicleTypeIn(List<String> values) {
            addCriterion("C_VEHICLE_TYPE in", values, "vehicleType");
            return (Criteria) this;
        }

        public Criteria andVehicleTypeNotIn(List<String> values) {
            addCriterion("C_VEHICLE_TYPE not in", values, "vehicleType");
            return (Criteria) this;
        }

        public Criteria andVehicleTypeBetween(String value1, String value2) {
            addCriterion("C_VEHICLE_TYPE between", value1, value2, "vehicleType");
            return (Criteria) this;
        }

        public Criteria andVehicleTypeNotBetween(String value1, String value2) {
            addCriterion("C_VEHICLE_TYPE not between", value1, value2, "vehicleType");
            return (Criteria) this;
        }
    }

    public static class Criteria extends GeneratedCriteria {

        protected Criteria() {
            super();
        }
    }

    public static class Criterion {
        private String condition;

        private Object value;

        private Object secondValue;

        private boolean noValue;

        private boolean singleValue;

        private boolean betweenValue;

        private boolean listValue;

        private String typeHandler;

        public String getCondition() {
            return condition;
        }

        public Object getValue() {
            return value;
        }

        public Object getSecondValue() {
            return secondValue;
        }

        public boolean isNoValue() {
            return noValue;
        }

        public boolean isSingleValue() {
            return singleValue;
        }

        public boolean isBetweenValue() {
            return betweenValue;
        }

        public boolean isListValue() {
            return listValue;
        }

        public String getTypeHandler() {
            return typeHandler;
        }

        protected Criterion(String condition) {
            super();
            this.condition = condition;
            this.typeHandler = null;
            this.noValue = true;
        }

        protected Criterion(String condition, Object value, String typeHandler) {
            super();
            this.condition = condition;
            this.value = value;
            this.typeHandler = typeHandler;
            if (value instanceof List<?>) {
                this.listValue = true;
            } else {
                this.singleValue = true;
            }
        }

        protected Criterion(String condition, Object value) {
            this(condition, value, null);
        }

        protected Criterion(String condition, Object value, Object secondValue, String typeHandler) {
            super();
            this.condition = condition;
            this.value = value;
            this.secondValue = secondValue;
            this.typeHandler = typeHandler;
            this.betweenValue = true;
        }

        protected Criterion(String condition, Object value, Object secondValue) {
            this(condition, value, secondValue, null);
        }
    }
}