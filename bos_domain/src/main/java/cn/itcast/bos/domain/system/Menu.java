package cn.itcast.bos.domain.system;

import javax.persistence.*;
import java.io.Serializable;

/**
 * @description:菜单
 */
@Entity
@Table(name = "T_MENU")
public class Menu implements Serializable {

	@Id
	@GeneratedValue
	@Column(name = "C_ID")
	private  Integer id;

	@Column(name = "C_NAME")
	private String name; // 菜单名称

	@Column(name = "C_PAGE")
	private String page; // 访问路径

	@Column(name = "C_PRIORITY")
	private Integer priority; // 优先级

	@Column(name = "C_DESCRIPTION")
	private String description; // 描述

	@Column(name = "C_PID")
	private Integer pid;

	//这里的get方法要手动的修改一下getpId为了EasyUI能够菜单分级
	@Transient
	public Integer getpId() {
		return pid;
	}

	public void setPid(Integer pid) {
		this.pid = pid;
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getPage() {
		return page;
	}

	public void setPage(String page) {
		this.page = page;
	}

	public Integer getPriority() {
		return priority;
	}

	public void setPriority(Integer priority) {
		this.priority = priority;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

}
