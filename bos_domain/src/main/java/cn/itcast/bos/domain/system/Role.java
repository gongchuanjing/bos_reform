package cn.itcast.bos.domain.system;

import javax.persistence.*;
import java.io.Serializable;

/**
 * @description:角色
 */
@Entity
@Table(name = "T_ROLE")
public class Role implements Serializable {
	@Id
	@GeneratedValue
	@Column(name = "C_ID")
	private Integer id;
	
	@Column(name = "C_NAME")
	private String name; // 角色名称
	
	@Column(name = "C_KEYWORD")
	private String keyword; // 角色关键字，用于权限控制
	
	@Column(name = "C_DESCRIPTION")
	private String description; // 描述




	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getKeyword() {
		return keyword;
	}

	public void setKeyword(String keyword) {
		this.keyword = keyword;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}


}
