package cn.itcast.bos.domain.system;

import javax.persistence.*;

/**
 * 角色菜单中间表
 */
@Entity
@Table(name="T_ROLE_MENU")
public class RoleMenu {

    @Id
    @GeneratedValue
    @Column(name = "C_ID")
    private Integer id;

    @Column(name="C_ROLE_ID")
    private Integer roleId;
    @Column(name="C_MENU_ID")
    private Integer menuId;


    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getMenuId() {
        return menuId;
    }

    public void setMenuId(Integer menuId) {
        this.menuId = menuId;
    }

    public Integer getRoleId() {

        return roleId;
    }

    public void setRoleId(Integer roleId) {
        this.roleId = roleId;
    }
}
