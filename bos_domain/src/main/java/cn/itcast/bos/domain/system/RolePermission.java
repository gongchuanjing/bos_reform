package cn.itcast.bos.domain.system;

import javax.persistence.*;

/**
 * 角色权限表
 */
@Entity
@Table(name="T_ROLE_PERMISSION")
public class RolePermission {

    @Id
    @GeneratedValue
    @Column(name = "C_ID")
    private Integer id;

    @Column(name="C_ROLE_ID")
    private Integer roleId;
    @Column(name="C_PERMISSION_ID")
    private Integer permissionId;


    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getRoleId() {
        return roleId;
    }

    public void setRoleId(Integer roleId) {
        this.roleId = roleId;
    }

    public Integer getPermissionId() {
        return permissionId;
    }

    public void setPermissionId(Integer permissionId) {
        this.permissionId = permissionId;
    }
}
