package cn.itcast.bos.domain.system;

import javax.persistence.*;

/**
 * 用户和角色中间表
 */
@Entity
@Table(name="T_USER_ROLE")
public class UserRole {

    @Id
    @GeneratedValue
    @Column(name = "C_ID")
    private Integer id;

    @Column(name="C_USER_ID")
    private Integer userId;

    @Column(name="C_ROLE_ID")
    private Integer roleId;


    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getUserId() {
        return userId;
    }

    public void setUserId(Integer userId) {
        this.userId = userId;
    }

    public Integer getRoleId() {
        return roleId;
    }

    public void setRoleId(Integer roleId) {
        this.roleId = roleId;
    }
}
