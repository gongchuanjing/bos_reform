package cn.itcast.bos.domain.take_delivery;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class OrderExample {
    protected String orderByClause;

    protected boolean distinct;

    protected List<Criteria> oredCriteria;

    public OrderExample() {
        oredCriteria = new ArrayList<Criteria>();
    }

    public void setOrderByClause(String orderByClause) {
        this.orderByClause = orderByClause;
    }

    public String getOrderByClause() {
        return orderByClause;
    }

    public void setDistinct(boolean distinct) {
        this.distinct = distinct;
    }

    public boolean isDistinct() {
        return distinct;
    }

    public List<Criteria> getOredCriteria() {
        return oredCriteria;
    }

    public void or(Criteria criteria) {
        oredCriteria.add(criteria);
    }

    public Criteria or() {
        Criteria criteria = createCriteriaInternal();
        oredCriteria.add(criteria);
        return criteria;
    }

    public Criteria createCriteria() {
        Criteria criteria = createCriteriaInternal();
        if (oredCriteria.size() == 0) {
            oredCriteria.add(criteria);
        }
        return criteria;
    }

    protected Criteria createCriteriaInternal() {
        Criteria criteria = new Criteria();
        return criteria;
    }

    public void clear() {
        oredCriteria.clear();
        orderByClause = null;
        distinct = false;
    }

    protected abstract static class GeneratedCriteria {
        protected List<Criterion> criteria;

        protected GeneratedCriteria() {
            super();
            criteria = new ArrayList<Criterion>();
        }

        public boolean isValid() {
            return criteria.size() > 0;
        }

        public List<Criterion> getAllCriteria() {
            return criteria;
        }

        public List<Criterion> getCriteria() {
            return criteria;
        }

        protected void addCriterion(String condition) {
            if (condition == null) {
                throw new RuntimeException("Value for condition cannot be null");
            }
            criteria.add(new Criterion(condition));
        }

        protected void addCriterion(String condition, Object value, String property) {
            if (value == null) {
                throw new RuntimeException("Value for " + property + " cannot be null");
            }
            criteria.add(new Criterion(condition, value));
        }

        protected void addCriterion(String condition, Object value1, Object value2, String property) {
            if (value1 == null || value2 == null) {
                throw new RuntimeException("Between values for " + property + " cannot be null");
            }
            criteria.add(new Criterion(condition, value1, value2));
        }

        public Criteria andIdIsNull() {
            addCriterion("C_ID is null");
            return (Criteria) this;
        }

        public Criteria andIdIsNotNull() {
            addCriterion("C_ID is not null");
            return (Criteria) this;
        }

        public Criteria andIdEqualTo(Integer value) {
            addCriterion("C_ID =", value, "id");
            return (Criteria) this;
        }

        public Criteria andIdNotEqualTo(Integer value) {
            addCriterion("C_ID <>", value, "id");
            return (Criteria) this;
        }

        public Criteria andIdGreaterThan(Integer value) {
            addCriterion("C_ID >", value, "id");
            return (Criteria) this;
        }

        public Criteria andIdGreaterThanOrEqualTo(Integer value) {
            addCriterion("C_ID >=", value, "id");
            return (Criteria) this;
        }

        public Criteria andIdLessThan(Integer value) {
            addCriterion("C_ID <", value, "id");
            return (Criteria) this;
        }

        public Criteria andIdLessThanOrEqualTo(Integer value) {
            addCriterion("C_ID <=", value, "id");
            return (Criteria) this;
        }

        public Criteria andIdIn(List<Integer> values) {
            addCriterion("C_ID in", values, "id");
            return (Criteria) this;
        }

        public Criteria andIdNotIn(List<Integer> values) {
            addCriterion("C_ID not in", values, "id");
            return (Criteria) this;
        }

        public Criteria andIdBetween(Integer value1, Integer value2) {
            addCriterion("C_ID between", value1, value2, "id");
            return (Criteria) this;
        }

        public Criteria andIdNotBetween(Integer value1, Integer value2) {
            addCriterion("C_ID not between", value1, value2, "id");
            return (Criteria) this;
        }

        public Criteria andCustomerIdIsNull() {
            addCriterion("C_CUSTOMER_ID is null");
            return (Criteria) this;
        }

        public Criteria andCustomerIdIsNotNull() {
            addCriterion("C_CUSTOMER_ID is not null");
            return (Criteria) this;
        }

        public Criteria andCustomerIdEqualTo(Integer value) {
            addCriterion("C_CUSTOMER_ID =", value, "customerId");
            return (Criteria) this;
        }

        public Criteria andCustomerIdNotEqualTo(Integer value) {
            addCriterion("C_CUSTOMER_ID <>", value, "customerId");
            return (Criteria) this;
        }

        public Criteria andCustomerIdGreaterThan(Integer value) {
            addCriterion("C_CUSTOMER_ID >", value, "customerId");
            return (Criteria) this;
        }

        public Criteria andCustomerIdGreaterThanOrEqualTo(Integer value) {
            addCriterion("C_CUSTOMER_ID >=", value, "customerId");
            return (Criteria) this;
        }

        public Criteria andCustomerIdLessThan(Integer value) {
            addCriterion("C_CUSTOMER_ID <", value, "customerId");
            return (Criteria) this;
        }

        public Criteria andCustomerIdLessThanOrEqualTo(Integer value) {
            addCriterion("C_CUSTOMER_ID <=", value, "customerId");
            return (Criteria) this;
        }

        public Criteria andCustomerIdIn(List<Integer> values) {
            addCriterion("C_CUSTOMER_ID in", values, "customerId");
            return (Criteria) this;
        }

        public Criteria andCustomerIdNotIn(List<Integer> values) {
            addCriterion("C_CUSTOMER_ID not in", values, "customerId");
            return (Criteria) this;
        }

        public Criteria andCustomerIdBetween(Integer value1, Integer value2) {
            addCriterion("C_CUSTOMER_ID between", value1, value2, "customerId");
            return (Criteria) this;
        }

        public Criteria andCustomerIdNotBetween(Integer value1, Integer value2) {
            addCriterion("C_CUSTOMER_ID not between", value1, value2, "customerId");
            return (Criteria) this;
        }

        public Criteria andGoodsTypeIsNull() {
            addCriterion("C_GOODS_TYPE is null");
            return (Criteria) this;
        }

        public Criteria andGoodsTypeIsNotNull() {
            addCriterion("C_GOODS_TYPE is not null");
            return (Criteria) this;
        }

        public Criteria andGoodsTypeEqualTo(String value) {
            addCriterion("C_GOODS_TYPE =", value, "goodsType");
            return (Criteria) this;
        }

        public Criteria andGoodsTypeNotEqualTo(String value) {
            addCriterion("C_GOODS_TYPE <>", value, "goodsType");
            return (Criteria) this;
        }

        public Criteria andGoodsTypeGreaterThan(String value) {
            addCriterion("C_GOODS_TYPE >", value, "goodsType");
            return (Criteria) this;
        }

        public Criteria andGoodsTypeGreaterThanOrEqualTo(String value) {
            addCriterion("C_GOODS_TYPE >=", value, "goodsType");
            return (Criteria) this;
        }

        public Criteria andGoodsTypeLessThan(String value) {
            addCriterion("C_GOODS_TYPE <", value, "goodsType");
            return (Criteria) this;
        }

        public Criteria andGoodsTypeLessThanOrEqualTo(String value) {
            addCriterion("C_GOODS_TYPE <=", value, "goodsType");
            return (Criteria) this;
        }

        public Criteria andGoodsTypeLike(String value) {
            addCriterion("C_GOODS_TYPE like", value, "goodsType");
            return (Criteria) this;
        }

        public Criteria andGoodsTypeNotLike(String value) {
            addCriterion("C_GOODS_TYPE not like", value, "goodsType");
            return (Criteria) this;
        }

        public Criteria andGoodsTypeIn(List<String> values) {
            addCriterion("C_GOODS_TYPE in", values, "goodsType");
            return (Criteria) this;
        }

        public Criteria andGoodsTypeNotIn(List<String> values) {
            addCriterion("C_GOODS_TYPE not in", values, "goodsType");
            return (Criteria) this;
        }

        public Criteria andGoodsTypeBetween(String value1, String value2) {
            addCriterion("C_GOODS_TYPE between", value1, value2, "goodsType");
            return (Criteria) this;
        }

        public Criteria andGoodsTypeNotBetween(String value1, String value2) {
            addCriterion("C_GOODS_TYPE not between", value1, value2, "goodsType");
            return (Criteria) this;
        }

        public Criteria andOrderNumIsNull() {
            addCriterion("C_ORDER_NUM is null");
            return (Criteria) this;
        }

        public Criteria andOrderNumIsNotNull() {
            addCriterion("C_ORDER_NUM is not null");
            return (Criteria) this;
        }

        public Criteria andOrderNumEqualTo(String value) {
            addCriterion("C_ORDER_NUM =", value, "orderNum");
            return (Criteria) this;
        }

        public Criteria andOrderNumNotEqualTo(String value) {
            addCriterion("C_ORDER_NUM <>", value, "orderNum");
            return (Criteria) this;
        }

        public Criteria andOrderNumGreaterThan(String value) {
            addCriterion("C_ORDER_NUM >", value, "orderNum");
            return (Criteria) this;
        }

        public Criteria andOrderNumGreaterThanOrEqualTo(String value) {
            addCriterion("C_ORDER_NUM >=", value, "orderNum");
            return (Criteria) this;
        }

        public Criteria andOrderNumLessThan(String value) {
            addCriterion("C_ORDER_NUM <", value, "orderNum");
            return (Criteria) this;
        }

        public Criteria andOrderNumLessThanOrEqualTo(String value) {
            addCriterion("C_ORDER_NUM <=", value, "orderNum");
            return (Criteria) this;
        }

        public Criteria andOrderNumLike(String value) {
            addCriterion("C_ORDER_NUM like", value, "orderNum");
            return (Criteria) this;
        }

        public Criteria andOrderNumNotLike(String value) {
            addCriterion("C_ORDER_NUM not like", value, "orderNum");
            return (Criteria) this;
        }

        public Criteria andOrderNumIn(List<String> values) {
            addCriterion("C_ORDER_NUM in", values, "orderNum");
            return (Criteria) this;
        }

        public Criteria andOrderNumNotIn(List<String> values) {
            addCriterion("C_ORDER_NUM not in", values, "orderNum");
            return (Criteria) this;
        }

        public Criteria andOrderNumBetween(String value1, String value2) {
            addCriterion("C_ORDER_NUM between", value1, value2, "orderNum");
            return (Criteria) this;
        }

        public Criteria andOrderNumNotBetween(String value1, String value2) {
            addCriterion("C_ORDER_NUM not between", value1, value2, "orderNum");
            return (Criteria) this;
        }

        public Criteria andOrderTimeIsNull() {
            addCriterion("C_ORDER_TIME is null");
            return (Criteria) this;
        }

        public Criteria andOrderTimeIsNotNull() {
            addCriterion("C_ORDER_TIME is not null");
            return (Criteria) this;
        }

        public Criteria andOrderTimeEqualTo(Date value) {
            addCriterion("C_ORDER_TIME =", value, "orderTime");
            return (Criteria) this;
        }

        public Criteria andOrderTimeNotEqualTo(Date value) {
            addCriterion("C_ORDER_TIME <>", value, "orderTime");
            return (Criteria) this;
        }

        public Criteria andOrderTimeGreaterThan(Date value) {
            addCriterion("C_ORDER_TIME >", value, "orderTime");
            return (Criteria) this;
        }

        public Criteria andOrderTimeGreaterThanOrEqualTo(Date value) {
            addCriterion("C_ORDER_TIME >=", value, "orderTime");
            return (Criteria) this;
        }

        public Criteria andOrderTimeLessThan(Date value) {
            addCriterion("C_ORDER_TIME <", value, "orderTime");
            return (Criteria) this;
        }

        public Criteria andOrderTimeLessThanOrEqualTo(Date value) {
            addCriterion("C_ORDER_TIME <=", value, "orderTime");
            return (Criteria) this;
        }

        public Criteria andOrderTimeIn(List<Date> values) {
            addCriterion("C_ORDER_TIME in", values, "orderTime");
            return (Criteria) this;
        }

        public Criteria andOrderTimeNotIn(List<Date> values) {
            addCriterion("C_ORDER_TIME not in", values, "orderTime");
            return (Criteria) this;
        }

        public Criteria andOrderTimeBetween(Date value1, Date value2) {
            addCriterion("C_ORDER_TIME between", value1, value2, "orderTime");
            return (Criteria) this;
        }

        public Criteria andOrderTimeNotBetween(Date value1, Date value2) {
            addCriterion("C_ORDER_TIME not between", value1, value2, "orderTime");
            return (Criteria) this;
        }

        public Criteria andOrderTypeIsNull() {
            addCriterion("C_ORDER_TYPE is null");
            return (Criteria) this;
        }

        public Criteria andOrderTypeIsNotNull() {
            addCriterion("C_ORDER_TYPE is not null");
            return (Criteria) this;
        }

        public Criteria andOrderTypeEqualTo(String value) {
            addCriterion("C_ORDER_TYPE =", value, "orderType");
            return (Criteria) this;
        }

        public Criteria andOrderTypeNotEqualTo(String value) {
            addCriterion("C_ORDER_TYPE <>", value, "orderType");
            return (Criteria) this;
        }

        public Criteria andOrderTypeGreaterThan(String value) {
            addCriterion("C_ORDER_TYPE >", value, "orderType");
            return (Criteria) this;
        }

        public Criteria andOrderTypeGreaterThanOrEqualTo(String value) {
            addCriterion("C_ORDER_TYPE >=", value, "orderType");
            return (Criteria) this;
        }

        public Criteria andOrderTypeLessThan(String value) {
            addCriterion("C_ORDER_TYPE <", value, "orderType");
            return (Criteria) this;
        }

        public Criteria andOrderTypeLessThanOrEqualTo(String value) {
            addCriterion("C_ORDER_TYPE <=", value, "orderType");
            return (Criteria) this;
        }

        public Criteria andOrderTypeLike(String value) {
            addCriterion("C_ORDER_TYPE like", value, "orderType");
            return (Criteria) this;
        }

        public Criteria andOrderTypeNotLike(String value) {
            addCriterion("C_ORDER_TYPE not like", value, "orderType");
            return (Criteria) this;
        }

        public Criteria andOrderTypeIn(List<String> values) {
            addCriterion("C_ORDER_TYPE in", values, "orderType");
            return (Criteria) this;
        }

        public Criteria andOrderTypeNotIn(List<String> values) {
            addCriterion("C_ORDER_TYPE not in", values, "orderType");
            return (Criteria) this;
        }

        public Criteria andOrderTypeBetween(String value1, String value2) {
            addCriterion("C_ORDER_TYPE between", value1, value2, "orderType");
            return (Criteria) this;
        }

        public Criteria andOrderTypeNotBetween(String value1, String value2) {
            addCriterion("C_ORDER_TYPE not between", value1, value2, "orderType");
            return (Criteria) this;
        }

        public Criteria andPayTypeNumIsNull() {
            addCriterion("C_PAY_TYPE_NUM is null");
            return (Criteria) this;
        }

        public Criteria andPayTypeNumIsNotNull() {
            addCriterion("C_PAY_TYPE_NUM is not null");
            return (Criteria) this;
        }

        public Criteria andPayTypeNumEqualTo(String value) {
            addCriterion("C_PAY_TYPE_NUM =", value, "payTypeNum");
            return (Criteria) this;
        }

        public Criteria andPayTypeNumNotEqualTo(String value) {
            addCriterion("C_PAY_TYPE_NUM <>", value, "payTypeNum");
            return (Criteria) this;
        }

        public Criteria andPayTypeNumGreaterThan(String value) {
            addCriterion("C_PAY_TYPE_NUM >", value, "payTypeNum");
            return (Criteria) this;
        }

        public Criteria andPayTypeNumGreaterThanOrEqualTo(String value) {
            addCriterion("C_PAY_TYPE_NUM >=", value, "payTypeNum");
            return (Criteria) this;
        }

        public Criteria andPayTypeNumLessThan(String value) {
            addCriterion("C_PAY_TYPE_NUM <", value, "payTypeNum");
            return (Criteria) this;
        }

        public Criteria andPayTypeNumLessThanOrEqualTo(String value) {
            addCriterion("C_PAY_TYPE_NUM <=", value, "payTypeNum");
            return (Criteria) this;
        }

        public Criteria andPayTypeNumLike(String value) {
            addCriterion("C_PAY_TYPE_NUM like", value, "payTypeNum");
            return (Criteria) this;
        }

        public Criteria andPayTypeNumNotLike(String value) {
            addCriterion("C_PAY_TYPE_NUM not like", value, "payTypeNum");
            return (Criteria) this;
        }

        public Criteria andPayTypeNumIn(List<String> values) {
            addCriterion("C_PAY_TYPE_NUM in", values, "payTypeNum");
            return (Criteria) this;
        }

        public Criteria andPayTypeNumNotIn(List<String> values) {
            addCriterion("C_PAY_TYPE_NUM not in", values, "payTypeNum");
            return (Criteria) this;
        }

        public Criteria andPayTypeNumBetween(String value1, String value2) {
            addCriterion("C_PAY_TYPE_NUM between", value1, value2, "payTypeNum");
            return (Criteria) this;
        }

        public Criteria andPayTypeNumNotBetween(String value1, String value2) {
            addCriterion("C_PAY_TYPE_NUM not between", value1, value2, "payTypeNum");
            return (Criteria) this;
        }

        public Criteria andRecAddressIsNull() {
            addCriterion("C_REC_ADDRESS is null");
            return (Criteria) this;
        }

        public Criteria andRecAddressIsNotNull() {
            addCriterion("C_REC_ADDRESS is not null");
            return (Criteria) this;
        }

        public Criteria andRecAddressEqualTo(String value) {
            addCriterion("C_REC_ADDRESS =", value, "recAddress");
            return (Criteria) this;
        }

        public Criteria andRecAddressNotEqualTo(String value) {
            addCriterion("C_REC_ADDRESS <>", value, "recAddress");
            return (Criteria) this;
        }

        public Criteria andRecAddressGreaterThan(String value) {
            addCriterion("C_REC_ADDRESS >", value, "recAddress");
            return (Criteria) this;
        }

        public Criteria andRecAddressGreaterThanOrEqualTo(String value) {
            addCriterion("C_REC_ADDRESS >=", value, "recAddress");
            return (Criteria) this;
        }

        public Criteria andRecAddressLessThan(String value) {
            addCriterion("C_REC_ADDRESS <", value, "recAddress");
            return (Criteria) this;
        }

        public Criteria andRecAddressLessThanOrEqualTo(String value) {
            addCriterion("C_REC_ADDRESS <=", value, "recAddress");
            return (Criteria) this;
        }

        public Criteria andRecAddressLike(String value) {
            addCriterion("C_REC_ADDRESS like", value, "recAddress");
            return (Criteria) this;
        }

        public Criteria andRecAddressNotLike(String value) {
            addCriterion("C_REC_ADDRESS not like", value, "recAddress");
            return (Criteria) this;
        }

        public Criteria andRecAddressIn(List<String> values) {
            addCriterion("C_REC_ADDRESS in", values, "recAddress");
            return (Criteria) this;
        }

        public Criteria andRecAddressNotIn(List<String> values) {
            addCriterion("C_REC_ADDRESS not in", values, "recAddress");
            return (Criteria) this;
        }

        public Criteria andRecAddressBetween(String value1, String value2) {
            addCriterion("C_REC_ADDRESS between", value1, value2, "recAddress");
            return (Criteria) this;
        }

        public Criteria andRecAddressNotBetween(String value1, String value2) {
            addCriterion("C_REC_ADDRESS not between", value1, value2, "recAddress");
            return (Criteria) this;
        }

        public Criteria andRecCompanyIsNull() {
            addCriterion("C_REC_COMPANY is null");
            return (Criteria) this;
        }

        public Criteria andRecCompanyIsNotNull() {
            addCriterion("C_REC_COMPANY is not null");
            return (Criteria) this;
        }

        public Criteria andRecCompanyEqualTo(String value) {
            addCriterion("C_REC_COMPANY =", value, "recCompany");
            return (Criteria) this;
        }

        public Criteria andRecCompanyNotEqualTo(String value) {
            addCriterion("C_REC_COMPANY <>", value, "recCompany");
            return (Criteria) this;
        }

        public Criteria andRecCompanyGreaterThan(String value) {
            addCriterion("C_REC_COMPANY >", value, "recCompany");
            return (Criteria) this;
        }

        public Criteria andRecCompanyGreaterThanOrEqualTo(String value) {
            addCriterion("C_REC_COMPANY >=", value, "recCompany");
            return (Criteria) this;
        }

        public Criteria andRecCompanyLessThan(String value) {
            addCriterion("C_REC_COMPANY <", value, "recCompany");
            return (Criteria) this;
        }

        public Criteria andRecCompanyLessThanOrEqualTo(String value) {
            addCriterion("C_REC_COMPANY <=", value, "recCompany");
            return (Criteria) this;
        }

        public Criteria andRecCompanyLike(String value) {
            addCriterion("C_REC_COMPANY like", value, "recCompany");
            return (Criteria) this;
        }

        public Criteria andRecCompanyNotLike(String value) {
            addCriterion("C_REC_COMPANY not like", value, "recCompany");
            return (Criteria) this;
        }

        public Criteria andRecCompanyIn(List<String> values) {
            addCriterion("C_REC_COMPANY in", values, "recCompany");
            return (Criteria) this;
        }

        public Criteria andRecCompanyNotIn(List<String> values) {
            addCriterion("C_REC_COMPANY not in", values, "recCompany");
            return (Criteria) this;
        }

        public Criteria andRecCompanyBetween(String value1, String value2) {
            addCriterion("C_REC_COMPANY between", value1, value2, "recCompany");
            return (Criteria) this;
        }

        public Criteria andRecCompanyNotBetween(String value1, String value2) {
            addCriterion("C_REC_COMPANY not between", value1, value2, "recCompany");
            return (Criteria) this;
        }

        public Criteria andRecMobileIsNull() {
            addCriterion("C_REC_MOBILE is null");
            return (Criteria) this;
        }

        public Criteria andRecMobileIsNotNull() {
            addCriterion("C_REC_MOBILE is not null");
            return (Criteria) this;
        }

        public Criteria andRecMobileEqualTo(String value) {
            addCriterion("C_REC_MOBILE =", value, "recMobile");
            return (Criteria) this;
        }

        public Criteria andRecMobileNotEqualTo(String value) {
            addCriterion("C_REC_MOBILE <>", value, "recMobile");
            return (Criteria) this;
        }

        public Criteria andRecMobileGreaterThan(String value) {
            addCriterion("C_REC_MOBILE >", value, "recMobile");
            return (Criteria) this;
        }

        public Criteria andRecMobileGreaterThanOrEqualTo(String value) {
            addCriterion("C_REC_MOBILE >=", value, "recMobile");
            return (Criteria) this;
        }

        public Criteria andRecMobileLessThan(String value) {
            addCriterion("C_REC_MOBILE <", value, "recMobile");
            return (Criteria) this;
        }

        public Criteria andRecMobileLessThanOrEqualTo(String value) {
            addCriterion("C_REC_MOBILE <=", value, "recMobile");
            return (Criteria) this;
        }

        public Criteria andRecMobileLike(String value) {
            addCriterion("C_REC_MOBILE like", value, "recMobile");
            return (Criteria) this;
        }

        public Criteria andRecMobileNotLike(String value) {
            addCriterion("C_REC_MOBILE not like", value, "recMobile");
            return (Criteria) this;
        }

        public Criteria andRecMobileIn(List<String> values) {
            addCriterion("C_REC_MOBILE in", values, "recMobile");
            return (Criteria) this;
        }

        public Criteria andRecMobileNotIn(List<String> values) {
            addCriterion("C_REC_MOBILE not in", values, "recMobile");
            return (Criteria) this;
        }

        public Criteria andRecMobileBetween(String value1, String value2) {
            addCriterion("C_REC_MOBILE between", value1, value2, "recMobile");
            return (Criteria) this;
        }

        public Criteria andRecMobileNotBetween(String value1, String value2) {
            addCriterion("C_REC_MOBILE not between", value1, value2, "recMobile");
            return (Criteria) this;
        }

        public Criteria andRecNameIsNull() {
            addCriterion("C_REC_NAME is null");
            return (Criteria) this;
        }

        public Criteria andRecNameIsNotNull() {
            addCriterion("C_REC_NAME is not null");
            return (Criteria) this;
        }

        public Criteria andRecNameEqualTo(String value) {
            addCriterion("C_REC_NAME =", value, "recName");
            return (Criteria) this;
        }

        public Criteria andRecNameNotEqualTo(String value) {
            addCriterion("C_REC_NAME <>", value, "recName");
            return (Criteria) this;
        }

        public Criteria andRecNameGreaterThan(String value) {
            addCriterion("C_REC_NAME >", value, "recName");
            return (Criteria) this;
        }

        public Criteria andRecNameGreaterThanOrEqualTo(String value) {
            addCriterion("C_REC_NAME >=", value, "recName");
            return (Criteria) this;
        }

        public Criteria andRecNameLessThan(String value) {
            addCriterion("C_REC_NAME <", value, "recName");
            return (Criteria) this;
        }

        public Criteria andRecNameLessThanOrEqualTo(String value) {
            addCriterion("C_REC_NAME <=", value, "recName");
            return (Criteria) this;
        }

        public Criteria andRecNameLike(String value) {
            addCriterion("C_REC_NAME like", value, "recName");
            return (Criteria) this;
        }

        public Criteria andRecNameNotLike(String value) {
            addCriterion("C_REC_NAME not like", value, "recName");
            return (Criteria) this;
        }

        public Criteria andRecNameIn(List<String> values) {
            addCriterion("C_REC_NAME in", values, "recName");
            return (Criteria) this;
        }

        public Criteria andRecNameNotIn(List<String> values) {
            addCriterion("C_REC_NAME not in", values, "recName");
            return (Criteria) this;
        }

        public Criteria andRecNameBetween(String value1, String value2) {
            addCriterion("C_REC_NAME between", value1, value2, "recName");
            return (Criteria) this;
        }

        public Criteria andRecNameNotBetween(String value1, String value2) {
            addCriterion("C_REC_NAME not between", value1, value2, "recName");
            return (Criteria) this;
        }

        public Criteria andRemarkIsNull() {
            addCriterion("C_REMARK is null");
            return (Criteria) this;
        }

        public Criteria andRemarkIsNotNull() {
            addCriterion("C_REMARK is not null");
            return (Criteria) this;
        }

        public Criteria andRemarkEqualTo(String value) {
            addCriterion("C_REMARK =", value, "remark");
            return (Criteria) this;
        }

        public Criteria andRemarkNotEqualTo(String value) {
            addCriterion("C_REMARK <>", value, "remark");
            return (Criteria) this;
        }

        public Criteria andRemarkGreaterThan(String value) {
            addCriterion("C_REMARK >", value, "remark");
            return (Criteria) this;
        }

        public Criteria andRemarkGreaterThanOrEqualTo(String value) {
            addCriterion("C_REMARK >=", value, "remark");
            return (Criteria) this;
        }

        public Criteria andRemarkLessThan(String value) {
            addCriterion("C_REMARK <", value, "remark");
            return (Criteria) this;
        }

        public Criteria andRemarkLessThanOrEqualTo(String value) {
            addCriterion("C_REMARK <=", value, "remark");
            return (Criteria) this;
        }

        public Criteria andRemarkLike(String value) {
            addCriterion("C_REMARK like", value, "remark");
            return (Criteria) this;
        }

        public Criteria andRemarkNotLike(String value) {
            addCriterion("C_REMARK not like", value, "remark");
            return (Criteria) this;
        }

        public Criteria andRemarkIn(List<String> values) {
            addCriterion("C_REMARK in", values, "remark");
            return (Criteria) this;
        }

        public Criteria andRemarkNotIn(List<String> values) {
            addCriterion("C_REMARK not in", values, "remark");
            return (Criteria) this;
        }

        public Criteria andRemarkBetween(String value1, String value2) {
            addCriterion("C_REMARK between", value1, value2, "remark");
            return (Criteria) this;
        }

        public Criteria andRemarkNotBetween(String value1, String value2) {
            addCriterion("C_REMARK not between", value1, value2, "remark");
            return (Criteria) this;
        }

        public Criteria andSendAddressIsNull() {
            addCriterion("C_SEND_ADDRESS is null");
            return (Criteria) this;
        }

        public Criteria andSendAddressIsNotNull() {
            addCriterion("C_SEND_ADDRESS is not null");
            return (Criteria) this;
        }

        public Criteria andSendAddressEqualTo(String value) {
            addCriterion("C_SEND_ADDRESS =", value, "sendAddress");
            return (Criteria) this;
        }

        public Criteria andSendAddressNotEqualTo(String value) {
            addCriterion("C_SEND_ADDRESS <>", value, "sendAddress");
            return (Criteria) this;
        }

        public Criteria andSendAddressGreaterThan(String value) {
            addCriterion("C_SEND_ADDRESS >", value, "sendAddress");
            return (Criteria) this;
        }

        public Criteria andSendAddressGreaterThanOrEqualTo(String value) {
            addCriterion("C_SEND_ADDRESS >=", value, "sendAddress");
            return (Criteria) this;
        }

        public Criteria andSendAddressLessThan(String value) {
            addCriterion("C_SEND_ADDRESS <", value, "sendAddress");
            return (Criteria) this;
        }

        public Criteria andSendAddressLessThanOrEqualTo(String value) {
            addCriterion("C_SEND_ADDRESS <=", value, "sendAddress");
            return (Criteria) this;
        }

        public Criteria andSendAddressLike(String value) {
            addCriterion("C_SEND_ADDRESS like", value, "sendAddress");
            return (Criteria) this;
        }

        public Criteria andSendAddressNotLike(String value) {
            addCriterion("C_SEND_ADDRESS not like", value, "sendAddress");
            return (Criteria) this;
        }

        public Criteria andSendAddressIn(List<String> values) {
            addCriterion("C_SEND_ADDRESS in", values, "sendAddress");
            return (Criteria) this;
        }

        public Criteria andSendAddressNotIn(List<String> values) {
            addCriterion("C_SEND_ADDRESS not in", values, "sendAddress");
            return (Criteria) this;
        }

        public Criteria andSendAddressBetween(String value1, String value2) {
            addCriterion("C_SEND_ADDRESS between", value1, value2, "sendAddress");
            return (Criteria) this;
        }

        public Criteria andSendAddressNotBetween(String value1, String value2) {
            addCriterion("C_SEND_ADDRESS not between", value1, value2, "sendAddress");
            return (Criteria) this;
        }

        public Criteria andSendCompanyIsNull() {
            addCriterion("C_SEND_COMPANY is null");
            return (Criteria) this;
        }

        public Criteria andSendCompanyIsNotNull() {
            addCriterion("C_SEND_COMPANY is not null");
            return (Criteria) this;
        }

        public Criteria andSendCompanyEqualTo(String value) {
            addCriterion("C_SEND_COMPANY =", value, "sendCompany");
            return (Criteria) this;
        }

        public Criteria andSendCompanyNotEqualTo(String value) {
            addCriterion("C_SEND_COMPANY <>", value, "sendCompany");
            return (Criteria) this;
        }

        public Criteria andSendCompanyGreaterThan(String value) {
            addCriterion("C_SEND_COMPANY >", value, "sendCompany");
            return (Criteria) this;
        }

        public Criteria andSendCompanyGreaterThanOrEqualTo(String value) {
            addCriterion("C_SEND_COMPANY >=", value, "sendCompany");
            return (Criteria) this;
        }

        public Criteria andSendCompanyLessThan(String value) {
            addCriterion("C_SEND_COMPANY <", value, "sendCompany");
            return (Criteria) this;
        }

        public Criteria andSendCompanyLessThanOrEqualTo(String value) {
            addCriterion("C_SEND_COMPANY <=", value, "sendCompany");
            return (Criteria) this;
        }

        public Criteria andSendCompanyLike(String value) {
            addCriterion("C_SEND_COMPANY like", value, "sendCompany");
            return (Criteria) this;
        }

        public Criteria andSendCompanyNotLike(String value) {
            addCriterion("C_SEND_COMPANY not like", value, "sendCompany");
            return (Criteria) this;
        }

        public Criteria andSendCompanyIn(List<String> values) {
            addCriterion("C_SEND_COMPANY in", values, "sendCompany");
            return (Criteria) this;
        }

        public Criteria andSendCompanyNotIn(List<String> values) {
            addCriterion("C_SEND_COMPANY not in", values, "sendCompany");
            return (Criteria) this;
        }

        public Criteria andSendCompanyBetween(String value1, String value2) {
            addCriterion("C_SEND_COMPANY between", value1, value2, "sendCompany");
            return (Criteria) this;
        }

        public Criteria andSendCompanyNotBetween(String value1, String value2) {
            addCriterion("C_SEND_COMPANY not between", value1, value2, "sendCompany");
            return (Criteria) this;
        }

        public Criteria andSendMobileIsNull() {
            addCriterion("C_SEND_MOBILE is null");
            return (Criteria) this;
        }

        public Criteria andSendMobileIsNotNull() {
            addCriterion("C_SEND_MOBILE is not null");
            return (Criteria) this;
        }

        public Criteria andSendMobileEqualTo(String value) {
            addCriterion("C_SEND_MOBILE =", value, "sendMobile");
            return (Criteria) this;
        }

        public Criteria andSendMobileNotEqualTo(String value) {
            addCriterion("C_SEND_MOBILE <>", value, "sendMobile");
            return (Criteria) this;
        }

        public Criteria andSendMobileGreaterThan(String value) {
            addCriterion("C_SEND_MOBILE >", value, "sendMobile");
            return (Criteria) this;
        }

        public Criteria andSendMobileGreaterThanOrEqualTo(String value) {
            addCriterion("C_SEND_MOBILE >=", value, "sendMobile");
            return (Criteria) this;
        }

        public Criteria andSendMobileLessThan(String value) {
            addCriterion("C_SEND_MOBILE <", value, "sendMobile");
            return (Criteria) this;
        }

        public Criteria andSendMobileLessThanOrEqualTo(String value) {
            addCriterion("C_SEND_MOBILE <=", value, "sendMobile");
            return (Criteria) this;
        }

        public Criteria andSendMobileLike(String value) {
            addCriterion("C_SEND_MOBILE like", value, "sendMobile");
            return (Criteria) this;
        }

        public Criteria andSendMobileNotLike(String value) {
            addCriterion("C_SEND_MOBILE not like", value, "sendMobile");
            return (Criteria) this;
        }

        public Criteria andSendMobileIn(List<String> values) {
            addCriterion("C_SEND_MOBILE in", values, "sendMobile");
            return (Criteria) this;
        }

        public Criteria andSendMobileNotIn(List<String> values) {
            addCriterion("C_SEND_MOBILE not in", values, "sendMobile");
            return (Criteria) this;
        }

        public Criteria andSendMobileBetween(String value1, String value2) {
            addCriterion("C_SEND_MOBILE between", value1, value2, "sendMobile");
            return (Criteria) this;
        }

        public Criteria andSendMobileNotBetween(String value1, String value2) {
            addCriterion("C_SEND_MOBILE not between", value1, value2, "sendMobile");
            return (Criteria) this;
        }

        public Criteria andSendMobileMsgIsNull() {
            addCriterion("C_SEND_MOBILE_MSG is null");
            return (Criteria) this;
        }

        public Criteria andSendMobileMsgIsNotNull() {
            addCriterion("C_SEND_MOBILE_MSG is not null");
            return (Criteria) this;
        }

        public Criteria andSendMobileMsgEqualTo(String value) {
            addCriterion("C_SEND_MOBILE_MSG =", value, "sendMobileMsg");
            return (Criteria) this;
        }

        public Criteria andSendMobileMsgNotEqualTo(String value) {
            addCriterion("C_SEND_MOBILE_MSG <>", value, "sendMobileMsg");
            return (Criteria) this;
        }

        public Criteria andSendMobileMsgGreaterThan(String value) {
            addCriterion("C_SEND_MOBILE_MSG >", value, "sendMobileMsg");
            return (Criteria) this;
        }

        public Criteria andSendMobileMsgGreaterThanOrEqualTo(String value) {
            addCriterion("C_SEND_MOBILE_MSG >=", value, "sendMobileMsg");
            return (Criteria) this;
        }

        public Criteria andSendMobileMsgLessThan(String value) {
            addCriterion("C_SEND_MOBILE_MSG <", value, "sendMobileMsg");
            return (Criteria) this;
        }

        public Criteria andSendMobileMsgLessThanOrEqualTo(String value) {
            addCriterion("C_SEND_MOBILE_MSG <=", value, "sendMobileMsg");
            return (Criteria) this;
        }

        public Criteria andSendMobileMsgLike(String value) {
            addCriterion("C_SEND_MOBILE_MSG like", value, "sendMobileMsg");
            return (Criteria) this;
        }

        public Criteria andSendMobileMsgNotLike(String value) {
            addCriterion("C_SEND_MOBILE_MSG not like", value, "sendMobileMsg");
            return (Criteria) this;
        }

        public Criteria andSendMobileMsgIn(List<String> values) {
            addCriterion("C_SEND_MOBILE_MSG in", values, "sendMobileMsg");
            return (Criteria) this;
        }

        public Criteria andSendMobileMsgNotIn(List<String> values) {
            addCriterion("C_SEND_MOBILE_MSG not in", values, "sendMobileMsg");
            return (Criteria) this;
        }

        public Criteria andSendMobileMsgBetween(String value1, String value2) {
            addCriterion("C_SEND_MOBILE_MSG between", value1, value2, "sendMobileMsg");
            return (Criteria) this;
        }

        public Criteria andSendMobileMsgNotBetween(String value1, String value2) {
            addCriterion("C_SEND_MOBILE_MSG not between", value1, value2, "sendMobileMsg");
            return (Criteria) this;
        }

        public Criteria andSendNameIsNull() {
            addCriterion("C_SEND_NAME is null");
            return (Criteria) this;
        }

        public Criteria andSendNameIsNotNull() {
            addCriterion("C_SEND_NAME is not null");
            return (Criteria) this;
        }

        public Criteria andSendNameEqualTo(String value) {
            addCriterion("C_SEND_NAME =", value, "sendName");
            return (Criteria) this;
        }

        public Criteria andSendNameNotEqualTo(String value) {
            addCriterion("C_SEND_NAME <>", value, "sendName");
            return (Criteria) this;
        }

        public Criteria andSendNameGreaterThan(String value) {
            addCriterion("C_SEND_NAME >", value, "sendName");
            return (Criteria) this;
        }

        public Criteria andSendNameGreaterThanOrEqualTo(String value) {
            addCriterion("C_SEND_NAME >=", value, "sendName");
            return (Criteria) this;
        }

        public Criteria andSendNameLessThan(String value) {
            addCriterion("C_SEND_NAME <", value, "sendName");
            return (Criteria) this;
        }

        public Criteria andSendNameLessThanOrEqualTo(String value) {
            addCriterion("C_SEND_NAME <=", value, "sendName");
            return (Criteria) this;
        }

        public Criteria andSendNameLike(String value) {
            addCriterion("C_SEND_NAME like", value, "sendName");
            return (Criteria) this;
        }

        public Criteria andSendNameNotLike(String value) {
            addCriterion("C_SEND_NAME not like", value, "sendName");
            return (Criteria) this;
        }

        public Criteria andSendNameIn(List<String> values) {
            addCriterion("C_SEND_NAME in", values, "sendName");
            return (Criteria) this;
        }

        public Criteria andSendNameNotIn(List<String> values) {
            addCriterion("C_SEND_NAME not in", values, "sendName");
            return (Criteria) this;
        }

        public Criteria andSendNameBetween(String value1, String value2) {
            addCriterion("C_SEND_NAME between", value1, value2, "sendName");
            return (Criteria) this;
        }

        public Criteria andSendNameNotBetween(String value1, String value2) {
            addCriterion("C_SEND_NAME not between", value1, value2, "sendName");
            return (Criteria) this;
        }

        public Criteria andSendProNumIsNull() {
            addCriterion("C_SEND_PRO_NUM is null");
            return (Criteria) this;
        }

        public Criteria andSendProNumIsNotNull() {
            addCriterion("C_SEND_PRO_NUM is not null");
            return (Criteria) this;
        }

        public Criteria andSendProNumEqualTo(String value) {
            addCriterion("C_SEND_PRO_NUM =", value, "sendProNum");
            return (Criteria) this;
        }

        public Criteria andSendProNumNotEqualTo(String value) {
            addCriterion("C_SEND_PRO_NUM <>", value, "sendProNum");
            return (Criteria) this;
        }

        public Criteria andSendProNumGreaterThan(String value) {
            addCriterion("C_SEND_PRO_NUM >", value, "sendProNum");
            return (Criteria) this;
        }

        public Criteria andSendProNumGreaterThanOrEqualTo(String value) {
            addCriterion("C_SEND_PRO_NUM >=", value, "sendProNum");
            return (Criteria) this;
        }

        public Criteria andSendProNumLessThan(String value) {
            addCriterion("C_SEND_PRO_NUM <", value, "sendProNum");
            return (Criteria) this;
        }

        public Criteria andSendProNumLessThanOrEqualTo(String value) {
            addCriterion("C_SEND_PRO_NUM <=", value, "sendProNum");
            return (Criteria) this;
        }

        public Criteria andSendProNumLike(String value) {
            addCriterion("C_SEND_PRO_NUM like", value, "sendProNum");
            return (Criteria) this;
        }

        public Criteria andSendProNumNotLike(String value) {
            addCriterion("C_SEND_PRO_NUM not like", value, "sendProNum");
            return (Criteria) this;
        }

        public Criteria andSendProNumIn(List<String> values) {
            addCriterion("C_SEND_PRO_NUM in", values, "sendProNum");
            return (Criteria) this;
        }

        public Criteria andSendProNumNotIn(List<String> values) {
            addCriterion("C_SEND_PRO_NUM not in", values, "sendProNum");
            return (Criteria) this;
        }

        public Criteria andSendProNumBetween(String value1, String value2) {
            addCriterion("C_SEND_PRO_NUM between", value1, value2, "sendProNum");
            return (Criteria) this;
        }

        public Criteria andSendProNumNotBetween(String value1, String value2) {
            addCriterion("C_SEND_PRO_NUM not between", value1, value2, "sendProNum");
            return (Criteria) this;
        }

        public Criteria andStatusIsNull() {
            addCriterion("C_STATUS is null");
            return (Criteria) this;
        }

        public Criteria andStatusIsNotNull() {
            addCriterion("C_STATUS is not null");
            return (Criteria) this;
        }

        public Criteria andStatusEqualTo(String value) {
            addCriterion("C_STATUS =", value, "status");
            return (Criteria) this;
        }

        public Criteria andStatusNotEqualTo(String value) {
            addCriterion("C_STATUS <>", value, "status");
            return (Criteria) this;
        }

        public Criteria andStatusGreaterThan(String value) {
            addCriterion("C_STATUS >", value, "status");
            return (Criteria) this;
        }

        public Criteria andStatusGreaterThanOrEqualTo(String value) {
            addCriterion("C_STATUS >=", value, "status");
            return (Criteria) this;
        }

        public Criteria andStatusLessThan(String value) {
            addCriterion("C_STATUS <", value, "status");
            return (Criteria) this;
        }

        public Criteria andStatusLessThanOrEqualTo(String value) {
            addCriterion("C_STATUS <=", value, "status");
            return (Criteria) this;
        }

        public Criteria andStatusLike(String value) {
            addCriterion("C_STATUS like", value, "status");
            return (Criteria) this;
        }

        public Criteria andStatusNotLike(String value) {
            addCriterion("C_STATUS not like", value, "status");
            return (Criteria) this;
        }

        public Criteria andStatusIn(List<String> values) {
            addCriterion("C_STATUS in", values, "status");
            return (Criteria) this;
        }

        public Criteria andStatusNotIn(List<String> values) {
            addCriterion("C_STATUS not in", values, "status");
            return (Criteria) this;
        }

        public Criteria andStatusBetween(String value1, String value2) {
            addCriterion("C_STATUS between", value1, value2, "status");
            return (Criteria) this;
        }

        public Criteria andStatusNotBetween(String value1, String value2) {
            addCriterion("C_STATUS not between", value1, value2, "status");
            return (Criteria) this;
        }

        public Criteria andTelephoneIsNull() {
            addCriterion("C_TELEPHONE is null");
            return (Criteria) this;
        }

        public Criteria andTelephoneIsNotNull() {
            addCriterion("C_TELEPHONE is not null");
            return (Criteria) this;
        }

        public Criteria andTelephoneEqualTo(String value) {
            addCriterion("C_TELEPHONE =", value, "telephone");
            return (Criteria) this;
        }

        public Criteria andTelephoneNotEqualTo(String value) {
            addCriterion("C_TELEPHONE <>", value, "telephone");
            return (Criteria) this;
        }

        public Criteria andTelephoneGreaterThan(String value) {
            addCriterion("C_TELEPHONE >", value, "telephone");
            return (Criteria) this;
        }

        public Criteria andTelephoneGreaterThanOrEqualTo(String value) {
            addCriterion("C_TELEPHONE >=", value, "telephone");
            return (Criteria) this;
        }

        public Criteria andTelephoneLessThan(String value) {
            addCriterion("C_TELEPHONE <", value, "telephone");
            return (Criteria) this;
        }

        public Criteria andTelephoneLessThanOrEqualTo(String value) {
            addCriterion("C_TELEPHONE <=", value, "telephone");
            return (Criteria) this;
        }

        public Criteria andTelephoneLike(String value) {
            addCriterion("C_TELEPHONE like", value, "telephone");
            return (Criteria) this;
        }

        public Criteria andTelephoneNotLike(String value) {
            addCriterion("C_TELEPHONE not like", value, "telephone");
            return (Criteria) this;
        }

        public Criteria andTelephoneIn(List<String> values) {
            addCriterion("C_TELEPHONE in", values, "telephone");
            return (Criteria) this;
        }

        public Criteria andTelephoneNotIn(List<String> values) {
            addCriterion("C_TELEPHONE not in", values, "telephone");
            return (Criteria) this;
        }

        public Criteria andTelephoneBetween(String value1, String value2) {
            addCriterion("C_TELEPHONE between", value1, value2, "telephone");
            return (Criteria) this;
        }

        public Criteria andTelephoneNotBetween(String value1, String value2) {
            addCriterion("C_TELEPHONE not between", value1, value2, "telephone");
            return (Criteria) this;
        }

        public Criteria andWeightIsNull() {
            addCriterion("C_WEIGHT is null");
            return (Criteria) this;
        }

        public Criteria andWeightIsNotNull() {
            addCriterion("C_WEIGHT is not null");
            return (Criteria) this;
        }

        public Criteria andWeightEqualTo(Double value) {
            addCriterion("C_WEIGHT =", value, "weight");
            return (Criteria) this;
        }

        public Criteria andWeightNotEqualTo(Double value) {
            addCriterion("C_WEIGHT <>", value, "weight");
            return (Criteria) this;
        }

        public Criteria andWeightGreaterThan(Double value) {
            addCriterion("C_WEIGHT >", value, "weight");
            return (Criteria) this;
        }

        public Criteria andWeightGreaterThanOrEqualTo(Double value) {
            addCriterion("C_WEIGHT >=", value, "weight");
            return (Criteria) this;
        }

        public Criteria andWeightLessThan(Double value) {
            addCriterion("C_WEIGHT <", value, "weight");
            return (Criteria) this;
        }

        public Criteria andWeightLessThanOrEqualTo(Double value) {
            addCriterion("C_WEIGHT <=", value, "weight");
            return (Criteria) this;
        }

        public Criteria andWeightIn(List<Double> values) {
            addCriterion("C_WEIGHT in", values, "weight");
            return (Criteria) this;
        }

        public Criteria andWeightNotIn(List<Double> values) {
            addCriterion("C_WEIGHT not in", values, "weight");
            return (Criteria) this;
        }

        public Criteria andWeightBetween(Double value1, Double value2) {
            addCriterion("C_WEIGHT between", value1, value2, "weight");
            return (Criteria) this;
        }

        public Criteria andWeightNotBetween(Double value1, Double value2) {
            addCriterion("C_WEIGHT not between", value1, value2, "weight");
            return (Criteria) this;
        }

        public Criteria andCourierIdIsNull() {
            addCriterion("C_COURIER_ID is null");
            return (Criteria) this;
        }

        public Criteria andCourierIdIsNotNull() {
            addCriterion("C_COURIER_ID is not null");
            return (Criteria) this;
        }

        public Criteria andCourierIdEqualTo(Integer value) {
            addCriterion("C_COURIER_ID =", value, "courierId");
            return (Criteria) this;
        }

        public Criteria andCourierIdNotEqualTo(Integer value) {
            addCriterion("C_COURIER_ID <>", value, "courierId");
            return (Criteria) this;
        }

        public Criteria andCourierIdGreaterThan(Integer value) {
            addCriterion("C_COURIER_ID >", value, "courierId");
            return (Criteria) this;
        }

        public Criteria andCourierIdGreaterThanOrEqualTo(Integer value) {
            addCriterion("C_COURIER_ID >=", value, "courierId");
            return (Criteria) this;
        }

        public Criteria andCourierIdLessThan(Integer value) {
            addCriterion("C_COURIER_ID <", value, "courierId");
            return (Criteria) this;
        }

        public Criteria andCourierIdLessThanOrEqualTo(Integer value) {
            addCriterion("C_COURIER_ID <=", value, "courierId");
            return (Criteria) this;
        }

        public Criteria andCourierIdIn(List<Integer> values) {
            addCriterion("C_COURIER_ID in", values, "courierId");
            return (Criteria) this;
        }

        public Criteria andCourierIdNotIn(List<Integer> values) {
            addCriterion("C_COURIER_ID not in", values, "courierId");
            return (Criteria) this;
        }

        public Criteria andCourierIdBetween(Integer value1, Integer value2) {
            addCriterion("C_COURIER_ID between", value1, value2, "courierId");
            return (Criteria) this;
        }

        public Criteria andCourierIdNotBetween(Integer value1, Integer value2) {
            addCriterion("C_COURIER_ID not between", value1, value2, "courierId");
            return (Criteria) this;
        }

        public Criteria andRecAreaIdIsNull() {
            addCriterion("C_REC_AREA_ID is null");
            return (Criteria) this;
        }

        public Criteria andRecAreaIdIsNotNull() {
            addCriterion("C_REC_AREA_ID is not null");
            return (Criteria) this;
        }

        public Criteria andRecAreaIdEqualTo(String value) {
            addCriterion("C_REC_AREA_ID =", value, "recAreaId");
            return (Criteria) this;
        }

        public Criteria andRecAreaIdNotEqualTo(String value) {
            addCriterion("C_REC_AREA_ID <>", value, "recAreaId");
            return (Criteria) this;
        }

        public Criteria andRecAreaIdGreaterThan(String value) {
            addCriterion("C_REC_AREA_ID >", value, "recAreaId");
            return (Criteria) this;
        }

        public Criteria andRecAreaIdGreaterThanOrEqualTo(String value) {
            addCriterion("C_REC_AREA_ID >=", value, "recAreaId");
            return (Criteria) this;
        }

        public Criteria andRecAreaIdLessThan(String value) {
            addCriterion("C_REC_AREA_ID <", value, "recAreaId");
            return (Criteria) this;
        }

        public Criteria andRecAreaIdLessThanOrEqualTo(String value) {
            addCriterion("C_REC_AREA_ID <=", value, "recAreaId");
            return (Criteria) this;
        }

        public Criteria andRecAreaIdLike(String value) {
            addCriterion("C_REC_AREA_ID like", value, "recAreaId");
            return (Criteria) this;
        }

        public Criteria andRecAreaIdNotLike(String value) {
            addCriterion("C_REC_AREA_ID not like", value, "recAreaId");
            return (Criteria) this;
        }

        public Criteria andRecAreaIdIn(List<String> values) {
            addCriterion("C_REC_AREA_ID in", values, "recAreaId");
            return (Criteria) this;
        }

        public Criteria andRecAreaIdNotIn(List<String> values) {
            addCriterion("C_REC_AREA_ID not in", values, "recAreaId");
            return (Criteria) this;
        }

        public Criteria andRecAreaIdBetween(String value1, String value2) {
            addCriterion("C_REC_AREA_ID between", value1, value2, "recAreaId");
            return (Criteria) this;
        }

        public Criteria andRecAreaIdNotBetween(String value1, String value2) {
            addCriterion("C_REC_AREA_ID not between", value1, value2, "recAreaId");
            return (Criteria) this;
        }

        public Criteria andSendAreaIdIsNull() {
            addCriterion("C_SEND_AREA_ID is null");
            return (Criteria) this;
        }

        public Criteria andSendAreaIdIsNotNull() {
            addCriterion("C_SEND_AREA_ID is not null");
            return (Criteria) this;
        }

        public Criteria andSendAreaIdEqualTo(String value) {
            addCriterion("C_SEND_AREA_ID =", value, "sendAreaId");
            return (Criteria) this;
        }

        public Criteria andSendAreaIdNotEqualTo(String value) {
            addCriterion("C_SEND_AREA_ID <>", value, "sendAreaId");
            return (Criteria) this;
        }

        public Criteria andSendAreaIdGreaterThan(String value) {
            addCriterion("C_SEND_AREA_ID >", value, "sendAreaId");
            return (Criteria) this;
        }

        public Criteria andSendAreaIdGreaterThanOrEqualTo(String value) {
            addCriterion("C_SEND_AREA_ID >=", value, "sendAreaId");
            return (Criteria) this;
        }

        public Criteria andSendAreaIdLessThan(String value) {
            addCriterion("C_SEND_AREA_ID <", value, "sendAreaId");
            return (Criteria) this;
        }

        public Criteria andSendAreaIdLessThanOrEqualTo(String value) {
            addCriterion("C_SEND_AREA_ID <=", value, "sendAreaId");
            return (Criteria) this;
        }

        public Criteria andSendAreaIdLike(String value) {
            addCriterion("C_SEND_AREA_ID like", value, "sendAreaId");
            return (Criteria) this;
        }

        public Criteria andSendAreaIdNotLike(String value) {
            addCriterion("C_SEND_AREA_ID not like", value, "sendAreaId");
            return (Criteria) this;
        }

        public Criteria andSendAreaIdIn(List<String> values) {
            addCriterion("C_SEND_AREA_ID in", values, "sendAreaId");
            return (Criteria) this;
        }

        public Criteria andSendAreaIdNotIn(List<String> values) {
            addCriterion("C_SEND_AREA_ID not in", values, "sendAreaId");
            return (Criteria) this;
        }

        public Criteria andSendAreaIdBetween(String value1, String value2) {
            addCriterion("C_SEND_AREA_ID between", value1, value2, "sendAreaId");
            return (Criteria) this;
        }

        public Criteria andSendAreaIdNotBetween(String value1, String value2) {
            addCriterion("C_SEND_AREA_ID not between", value1, value2, "sendAreaId");
            return (Criteria) this;
        }
    }

    public static class Criteria extends GeneratedCriteria {

        protected Criteria() {
            super();
        }
    }

    public static class Criterion {
        private String condition;

        private Object value;

        private Object secondValue;

        private boolean noValue;

        private boolean singleValue;

        private boolean betweenValue;

        private boolean listValue;

        private String typeHandler;

        public String getCondition() {
            return condition;
        }

        public Object getValue() {
            return value;
        }

        public Object getSecondValue() {
            return secondValue;
        }

        public boolean isNoValue() {
            return noValue;
        }

        public boolean isSingleValue() {
            return singleValue;
        }

        public boolean isBetweenValue() {
            return betweenValue;
        }

        public boolean isListValue() {
            return listValue;
        }

        public String getTypeHandler() {
            return typeHandler;
        }

        protected Criterion(String condition) {
            super();
            this.condition = condition;
            this.typeHandler = null;
            this.noValue = true;
        }

        protected Criterion(String condition, Object value, String typeHandler) {
            super();
            this.condition = condition;
            this.value = value;
            this.typeHandler = typeHandler;
            if (value instanceof List<?>) {
                this.listValue = true;
            } else {
                this.singleValue = true;
            }
        }

        protected Criterion(String condition, Object value) {
            this(condition, value, null);
        }

        protected Criterion(String condition, Object value, Object secondValue, String typeHandler) {
            super();
            this.condition = condition;
            this.value = value;
            this.secondValue = secondValue;
            this.typeHandler = typeHandler;
            this.betweenValue = true;
        }

        protected Criterion(String condition, Object value, Object secondValue) {
            this(condition, value, secondValue, null);
        }
    }
}