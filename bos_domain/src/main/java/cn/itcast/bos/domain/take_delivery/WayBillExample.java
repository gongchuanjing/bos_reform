package cn.itcast.bos.domain.take_delivery;

import java.util.ArrayList;
import java.util.List;

public class WayBillExample {
    protected String orderByClause;

    protected boolean distinct;

    protected List<Criteria> oredCriteria;

    public WayBillExample() {
        oredCriteria = new ArrayList<Criteria>();
    }

    public void setOrderByClause(String orderByClause) {
        this.orderByClause = orderByClause;
    }

    public String getOrderByClause() {
        return orderByClause;
    }

    public void setDistinct(boolean distinct) {
        this.distinct = distinct;
    }

    public boolean isDistinct() {
        return distinct;
    }

    public List<Criteria> getOredCriteria() {
        return oredCriteria;
    }

    public void or(Criteria criteria) {
        oredCriteria.add(criteria);
    }

    public Criteria or() {
        Criteria criteria = createCriteriaInternal();
        oredCriteria.add(criteria);
        return criteria;
    }

    public Criteria createCriteria() {
        Criteria criteria = createCriteriaInternal();
        if (oredCriteria.size() == 0) {
            oredCriteria.add(criteria);
        }
        return criteria;
    }

    protected Criteria createCriteriaInternal() {
        Criteria criteria = new Criteria();
        return criteria;
    }

    public void clear() {
        oredCriteria.clear();
        orderByClause = null;
        distinct = false;
    }

    protected abstract static class GeneratedCriteria {
        protected List<Criterion> criteria;

        protected GeneratedCriteria() {
            super();
            criteria = new ArrayList<Criterion>();
        }

        public boolean isValid() {
            return criteria.size() > 0;
        }

        public List<Criterion> getAllCriteria() {
            return criteria;
        }

        public List<Criterion> getCriteria() {
            return criteria;
        }

        protected void addCriterion(String condition) {
            if (condition == null) {
                throw new RuntimeException("Value for condition cannot be null");
            }
            criteria.add(new Criterion(condition));
        }

        protected void addCriterion(String condition, Object value, String property) {
            if (value == null) {
                throw new RuntimeException("Value for " + property + " cannot be null");
            }
            criteria.add(new Criterion(condition, value));
        }

        protected void addCriterion(String condition, Object value1, Object value2, String property) {
            if (value1 == null || value2 == null) {
                throw new RuntimeException("Between values for " + property + " cannot be null");
            }
            criteria.add(new Criterion(condition, value1, value2));
        }

        public Criteria andIdIsNull() {
            addCriterion("C_ID is null");
            return (Criteria) this;
        }

        public Criteria andIdIsNotNull() {
            addCriterion("C_ID is not null");
            return (Criteria) this;
        }

        public Criteria andIdEqualTo(Integer value) {
            addCriterion("C_ID =", value, "id");
            return (Criteria) this;
        }

        public Criteria andIdNotEqualTo(Integer value) {
            addCriterion("C_ID <>", value, "id");
            return (Criteria) this;
        }

        public Criteria andIdGreaterThan(Integer value) {
            addCriterion("C_ID >", value, "id");
            return (Criteria) this;
        }

        public Criteria andIdGreaterThanOrEqualTo(Integer value) {
            addCriterion("C_ID >=", value, "id");
            return (Criteria) this;
        }

        public Criteria andIdLessThan(Integer value) {
            addCriterion("C_ID <", value, "id");
            return (Criteria) this;
        }

        public Criteria andIdLessThanOrEqualTo(Integer value) {
            addCriterion("C_ID <=", value, "id");
            return (Criteria) this;
        }

        public Criteria andIdIn(List<Integer> values) {
            addCriterion("C_ID in", values, "id");
            return (Criteria) this;
        }

        public Criteria andIdNotIn(List<Integer> values) {
            addCriterion("C_ID not in", values, "id");
            return (Criteria) this;
        }

        public Criteria andIdBetween(Integer value1, Integer value2) {
            addCriterion("C_ID between", value1, value2, "id");
            return (Criteria) this;
        }

        public Criteria andIdNotBetween(Integer value1, Integer value2) {
            addCriterion("C_ID not between", value1, value2, "id");
            return (Criteria) this;
        }

        public Criteria andActlweitIsNull() {
            addCriterion("C_ACTLWEIT is null");
            return (Criteria) this;
        }

        public Criteria andActlweitIsNotNull() {
            addCriterion("C_ACTLWEIT is not null");
            return (Criteria) this;
        }

        public Criteria andActlweitEqualTo(Double value) {
            addCriterion("C_ACTLWEIT =", value, "actlweit");
            return (Criteria) this;
        }

        public Criteria andActlweitNotEqualTo(Double value) {
            addCriterion("C_ACTLWEIT <>", value, "actlweit");
            return (Criteria) this;
        }

        public Criteria andActlweitGreaterThan(Double value) {
            addCriterion("C_ACTLWEIT >", value, "actlweit");
            return (Criteria) this;
        }

        public Criteria andActlweitGreaterThanOrEqualTo(Double value) {
            addCriterion("C_ACTLWEIT >=", value, "actlweit");
            return (Criteria) this;
        }

        public Criteria andActlweitLessThan(Double value) {
            addCriterion("C_ACTLWEIT <", value, "actlweit");
            return (Criteria) this;
        }

        public Criteria andActlweitLessThanOrEqualTo(Double value) {
            addCriterion("C_ACTLWEIT <=", value, "actlweit");
            return (Criteria) this;
        }

        public Criteria andActlweitIn(List<Double> values) {
            addCriterion("C_ACTLWEIT in", values, "actlweit");
            return (Criteria) this;
        }

        public Criteria andActlweitNotIn(List<Double> values) {
            addCriterion("C_ACTLWEIT not in", values, "actlweit");
            return (Criteria) this;
        }

        public Criteria andActlweitBetween(Double value1, Double value2) {
            addCriterion("C_ACTLWEIT between", value1, value2, "actlweit");
            return (Criteria) this;
        }

        public Criteria andActlweitNotBetween(Double value1, Double value2) {
            addCriterion("C_ACTLWEIT not between", value1, value2, "actlweit");
            return (Criteria) this;
        }

        public Criteria andArriveCityIsNull() {
            addCriterion("C_ARRIVE_CITY is null");
            return (Criteria) this;
        }

        public Criteria andArriveCityIsNotNull() {
            addCriterion("C_ARRIVE_CITY is not null");
            return (Criteria) this;
        }

        public Criteria andArriveCityEqualTo(String value) {
            addCriterion("C_ARRIVE_CITY =", value, "arriveCity");
            return (Criteria) this;
        }

        public Criteria andArriveCityNotEqualTo(String value) {
            addCriterion("C_ARRIVE_CITY <>", value, "arriveCity");
            return (Criteria) this;
        }

        public Criteria andArriveCityGreaterThan(String value) {
            addCriterion("C_ARRIVE_CITY >", value, "arriveCity");
            return (Criteria) this;
        }

        public Criteria andArriveCityGreaterThanOrEqualTo(String value) {
            addCriterion("C_ARRIVE_CITY >=", value, "arriveCity");
            return (Criteria) this;
        }

        public Criteria andArriveCityLessThan(String value) {
            addCriterion("C_ARRIVE_CITY <", value, "arriveCity");
            return (Criteria) this;
        }

        public Criteria andArriveCityLessThanOrEqualTo(String value) {
            addCriterion("C_ARRIVE_CITY <=", value, "arriveCity");
            return (Criteria) this;
        }

        public Criteria andArriveCityLike(String value) {
            addCriterion("C_ARRIVE_CITY like", value, "arriveCity");
            return (Criteria) this;
        }

        public Criteria andArriveCityNotLike(String value) {
            addCriterion("C_ARRIVE_CITY not like", value, "arriveCity");
            return (Criteria) this;
        }

        public Criteria andArriveCityIn(List<String> values) {
            addCriterion("C_ARRIVE_CITY in", values, "arriveCity");
            return (Criteria) this;
        }

        public Criteria andArriveCityNotIn(List<String> values) {
            addCriterion("C_ARRIVE_CITY not in", values, "arriveCity");
            return (Criteria) this;
        }

        public Criteria andArriveCityBetween(String value1, String value2) {
            addCriterion("C_ARRIVE_CITY between", value1, value2, "arriveCity");
            return (Criteria) this;
        }

        public Criteria andArriveCityNotBetween(String value1, String value2) {
            addCriterion("C_ARRIVE_CITY not between", value1, value2, "arriveCity");
            return (Criteria) this;
        }

        public Criteria andDeltagIsNull() {
            addCriterion("C_DELTAG is null");
            return (Criteria) this;
        }

        public Criteria andDeltagIsNotNull() {
            addCriterion("C_DELTAG is not null");
            return (Criteria) this;
        }

        public Criteria andDeltagEqualTo(String value) {
            addCriterion("C_DELTAG =", value, "deltag");
            return (Criteria) this;
        }

        public Criteria andDeltagNotEqualTo(String value) {
            addCriterion("C_DELTAG <>", value, "deltag");
            return (Criteria) this;
        }

        public Criteria andDeltagGreaterThan(String value) {
            addCriterion("C_DELTAG >", value, "deltag");
            return (Criteria) this;
        }

        public Criteria andDeltagGreaterThanOrEqualTo(String value) {
            addCriterion("C_DELTAG >=", value, "deltag");
            return (Criteria) this;
        }

        public Criteria andDeltagLessThan(String value) {
            addCriterion("C_DELTAG <", value, "deltag");
            return (Criteria) this;
        }

        public Criteria andDeltagLessThanOrEqualTo(String value) {
            addCriterion("C_DELTAG <=", value, "deltag");
            return (Criteria) this;
        }

        public Criteria andDeltagLike(String value) {
            addCriterion("C_DELTAG like", value, "deltag");
            return (Criteria) this;
        }

        public Criteria andDeltagNotLike(String value) {
            addCriterion("C_DELTAG not like", value, "deltag");
            return (Criteria) this;
        }

        public Criteria andDeltagIn(List<String> values) {
            addCriterion("C_DELTAG in", values, "deltag");
            return (Criteria) this;
        }

        public Criteria andDeltagNotIn(List<String> values) {
            addCriterion("C_DELTAG not in", values, "deltag");
            return (Criteria) this;
        }

        public Criteria andDeltagBetween(String value1, String value2) {
            addCriterion("C_DELTAG between", value1, value2, "deltag");
            return (Criteria) this;
        }

        public Criteria andDeltagNotBetween(String value1, String value2) {
            addCriterion("C_DELTAG not between", value1, value2, "deltag");
            return (Criteria) this;
        }

        public Criteria andFeeitemnumIsNull() {
            addCriterion("C_FEEITEMNUM is null");
            return (Criteria) this;
        }

        public Criteria andFeeitemnumIsNotNull() {
            addCriterion("C_FEEITEMNUM is not null");
            return (Criteria) this;
        }

        public Criteria andFeeitemnumEqualTo(Integer value) {
            addCriterion("C_FEEITEMNUM =", value, "feeitemnum");
            return (Criteria) this;
        }

        public Criteria andFeeitemnumNotEqualTo(Integer value) {
            addCriterion("C_FEEITEMNUM <>", value, "feeitemnum");
            return (Criteria) this;
        }

        public Criteria andFeeitemnumGreaterThan(Integer value) {
            addCriterion("C_FEEITEMNUM >", value, "feeitemnum");
            return (Criteria) this;
        }

        public Criteria andFeeitemnumGreaterThanOrEqualTo(Integer value) {
            addCriterion("C_FEEITEMNUM >=", value, "feeitemnum");
            return (Criteria) this;
        }

        public Criteria andFeeitemnumLessThan(Integer value) {
            addCriterion("C_FEEITEMNUM <", value, "feeitemnum");
            return (Criteria) this;
        }

        public Criteria andFeeitemnumLessThanOrEqualTo(Integer value) {
            addCriterion("C_FEEITEMNUM <=", value, "feeitemnum");
            return (Criteria) this;
        }

        public Criteria andFeeitemnumIn(List<Integer> values) {
            addCriterion("C_FEEITEMNUM in", values, "feeitemnum");
            return (Criteria) this;
        }

        public Criteria andFeeitemnumNotIn(List<Integer> values) {
            addCriterion("C_FEEITEMNUM not in", values, "feeitemnum");
            return (Criteria) this;
        }

        public Criteria andFeeitemnumBetween(Integer value1, Integer value2) {
            addCriterion("C_FEEITEMNUM between", value1, value2, "feeitemnum");
            return (Criteria) this;
        }

        public Criteria andFeeitemnumNotBetween(Integer value1, Integer value2) {
            addCriterion("C_FEEITEMNUM not between", value1, value2, "feeitemnum");
            return (Criteria) this;
        }

        public Criteria andFloadreqrIsNull() {
            addCriterion("C_FLOADREQR is null");
            return (Criteria) this;
        }

        public Criteria andFloadreqrIsNotNull() {
            addCriterion("C_FLOADREQR is not null");
            return (Criteria) this;
        }

        public Criteria andFloadreqrEqualTo(String value) {
            addCriterion("C_FLOADREQR =", value, "floadreqr");
            return (Criteria) this;
        }

        public Criteria andFloadreqrNotEqualTo(String value) {
            addCriterion("C_FLOADREQR <>", value, "floadreqr");
            return (Criteria) this;
        }

        public Criteria andFloadreqrGreaterThan(String value) {
            addCriterion("C_FLOADREQR >", value, "floadreqr");
            return (Criteria) this;
        }

        public Criteria andFloadreqrGreaterThanOrEqualTo(String value) {
            addCriterion("C_FLOADREQR >=", value, "floadreqr");
            return (Criteria) this;
        }

        public Criteria andFloadreqrLessThan(String value) {
            addCriterion("C_FLOADREQR <", value, "floadreqr");
            return (Criteria) this;
        }

        public Criteria andFloadreqrLessThanOrEqualTo(String value) {
            addCriterion("C_FLOADREQR <=", value, "floadreqr");
            return (Criteria) this;
        }

        public Criteria andFloadreqrLike(String value) {
            addCriterion("C_FLOADREQR like", value, "floadreqr");
            return (Criteria) this;
        }

        public Criteria andFloadreqrNotLike(String value) {
            addCriterion("C_FLOADREQR not like", value, "floadreqr");
            return (Criteria) this;
        }

        public Criteria andFloadreqrIn(List<String> values) {
            addCriterion("C_FLOADREQR in", values, "floadreqr");
            return (Criteria) this;
        }

        public Criteria andFloadreqrNotIn(List<String> values) {
            addCriterion("C_FLOADREQR not in", values, "floadreqr");
            return (Criteria) this;
        }

        public Criteria andFloadreqrBetween(String value1, String value2) {
            addCriterion("C_FLOADREQR between", value1, value2, "floadreqr");
            return (Criteria) this;
        }

        public Criteria andFloadreqrNotBetween(String value1, String value2) {
            addCriterion("C_FLOADREQR not between", value1, value2, "floadreqr");
            return (Criteria) this;
        }

        public Criteria andGoodsTypeIsNull() {
            addCriterion("C_GOODS_TYPE is null");
            return (Criteria) this;
        }

        public Criteria andGoodsTypeIsNotNull() {
            addCriterion("C_GOODS_TYPE is not null");
            return (Criteria) this;
        }

        public Criteria andGoodsTypeEqualTo(String value) {
            addCriterion("C_GOODS_TYPE =", value, "goodsType");
            return (Criteria) this;
        }

        public Criteria andGoodsTypeNotEqualTo(String value) {
            addCriterion("C_GOODS_TYPE <>", value, "goodsType");
            return (Criteria) this;
        }

        public Criteria andGoodsTypeGreaterThan(String value) {
            addCriterion("C_GOODS_TYPE >", value, "goodsType");
            return (Criteria) this;
        }

        public Criteria andGoodsTypeGreaterThanOrEqualTo(String value) {
            addCriterion("C_GOODS_TYPE >=", value, "goodsType");
            return (Criteria) this;
        }

        public Criteria andGoodsTypeLessThan(String value) {
            addCriterion("C_GOODS_TYPE <", value, "goodsType");
            return (Criteria) this;
        }

        public Criteria andGoodsTypeLessThanOrEqualTo(String value) {
            addCriterion("C_GOODS_TYPE <=", value, "goodsType");
            return (Criteria) this;
        }

        public Criteria andGoodsTypeLike(String value) {
            addCriterion("C_GOODS_TYPE like", value, "goodsType");
            return (Criteria) this;
        }

        public Criteria andGoodsTypeNotLike(String value) {
            addCriterion("C_GOODS_TYPE not like", value, "goodsType");
            return (Criteria) this;
        }

        public Criteria andGoodsTypeIn(List<String> values) {
            addCriterion("C_GOODS_TYPE in", values, "goodsType");
            return (Criteria) this;
        }

        public Criteria andGoodsTypeNotIn(List<String> values) {
            addCriterion("C_GOODS_TYPE not in", values, "goodsType");
            return (Criteria) this;
        }

        public Criteria andGoodsTypeBetween(String value1, String value2) {
            addCriterion("C_GOODS_TYPE between", value1, value2, "goodsType");
            return (Criteria) this;
        }

        public Criteria andGoodsTypeNotBetween(String value1, String value2) {
            addCriterion("C_GOODS_TYPE not between", value1, value2, "goodsType");
            return (Criteria) this;
        }

        public Criteria andNumIsNull() {
            addCriterion("C_NUM is null");
            return (Criteria) this;
        }

        public Criteria andNumIsNotNull() {
            addCriterion("C_NUM is not null");
            return (Criteria) this;
        }

        public Criteria andNumEqualTo(Integer value) {
            addCriterion("C_NUM =", value, "num");
            return (Criteria) this;
        }

        public Criteria andNumNotEqualTo(Integer value) {
            addCriterion("C_NUM <>", value, "num");
            return (Criteria) this;
        }

        public Criteria andNumGreaterThan(Integer value) {
            addCriterion("C_NUM >", value, "num");
            return (Criteria) this;
        }

        public Criteria andNumGreaterThanOrEqualTo(Integer value) {
            addCriterion("C_NUM >=", value, "num");
            return (Criteria) this;
        }

        public Criteria andNumLessThan(Integer value) {
            addCriterion("C_NUM <", value, "num");
            return (Criteria) this;
        }

        public Criteria andNumLessThanOrEqualTo(Integer value) {
            addCriterion("C_NUM <=", value, "num");
            return (Criteria) this;
        }

        public Criteria andNumIn(List<Integer> values) {
            addCriterion("C_NUM in", values, "num");
            return (Criteria) this;
        }

        public Criteria andNumNotIn(List<Integer> values) {
            addCriterion("C_NUM not in", values, "num");
            return (Criteria) this;
        }

        public Criteria andNumBetween(Integer value1, Integer value2) {
            addCriterion("C_NUM between", value1, value2, "num");
            return (Criteria) this;
        }

        public Criteria andNumNotBetween(Integer value1, Integer value2) {
            addCriterion("C_NUM not between", value1, value2, "num");
            return (Criteria) this;
        }

        public Criteria andPayTypeNumIsNull() {
            addCriterion("C_PAY_TYPE_NUM is null");
            return (Criteria) this;
        }

        public Criteria andPayTypeNumIsNotNull() {
            addCriterion("C_PAY_TYPE_NUM is not null");
            return (Criteria) this;
        }

        public Criteria andPayTypeNumEqualTo(String value) {
            addCriterion("C_PAY_TYPE_NUM =", value, "payTypeNum");
            return (Criteria) this;
        }

        public Criteria andPayTypeNumNotEqualTo(String value) {
            addCriterion("C_PAY_TYPE_NUM <>", value, "payTypeNum");
            return (Criteria) this;
        }

        public Criteria andPayTypeNumGreaterThan(String value) {
            addCriterion("C_PAY_TYPE_NUM >", value, "payTypeNum");
            return (Criteria) this;
        }

        public Criteria andPayTypeNumGreaterThanOrEqualTo(String value) {
            addCriterion("C_PAY_TYPE_NUM >=", value, "payTypeNum");
            return (Criteria) this;
        }

        public Criteria andPayTypeNumLessThan(String value) {
            addCriterion("C_PAY_TYPE_NUM <", value, "payTypeNum");
            return (Criteria) this;
        }

        public Criteria andPayTypeNumLessThanOrEqualTo(String value) {
            addCriterion("C_PAY_TYPE_NUM <=", value, "payTypeNum");
            return (Criteria) this;
        }

        public Criteria andPayTypeNumLike(String value) {
            addCriterion("C_PAY_TYPE_NUM like", value, "payTypeNum");
            return (Criteria) this;
        }

        public Criteria andPayTypeNumNotLike(String value) {
            addCriterion("C_PAY_TYPE_NUM not like", value, "payTypeNum");
            return (Criteria) this;
        }

        public Criteria andPayTypeNumIn(List<String> values) {
            addCriterion("C_PAY_TYPE_NUM in", values, "payTypeNum");
            return (Criteria) this;
        }

        public Criteria andPayTypeNumNotIn(List<String> values) {
            addCriterion("C_PAY_TYPE_NUM not in", values, "payTypeNum");
            return (Criteria) this;
        }

        public Criteria andPayTypeNumBetween(String value1, String value2) {
            addCriterion("C_PAY_TYPE_NUM between", value1, value2, "payTypeNum");
            return (Criteria) this;
        }

        public Criteria andPayTypeNumNotBetween(String value1, String value2) {
            addCriterion("C_PAY_TYPE_NUM not between", value1, value2, "payTypeNum");
            return (Criteria) this;
        }

        public Criteria andRecAddressIsNull() {
            addCriterion("C_REC_ADDRESS is null");
            return (Criteria) this;
        }

        public Criteria andRecAddressIsNotNull() {
            addCriterion("C_REC_ADDRESS is not null");
            return (Criteria) this;
        }

        public Criteria andRecAddressEqualTo(String value) {
            addCriterion("C_REC_ADDRESS =", value, "recAddress");
            return (Criteria) this;
        }

        public Criteria andRecAddressNotEqualTo(String value) {
            addCriterion("C_REC_ADDRESS <>", value, "recAddress");
            return (Criteria) this;
        }

        public Criteria andRecAddressGreaterThan(String value) {
            addCriterion("C_REC_ADDRESS >", value, "recAddress");
            return (Criteria) this;
        }

        public Criteria andRecAddressGreaterThanOrEqualTo(String value) {
            addCriterion("C_REC_ADDRESS >=", value, "recAddress");
            return (Criteria) this;
        }

        public Criteria andRecAddressLessThan(String value) {
            addCriterion("C_REC_ADDRESS <", value, "recAddress");
            return (Criteria) this;
        }

        public Criteria andRecAddressLessThanOrEqualTo(String value) {
            addCriterion("C_REC_ADDRESS <=", value, "recAddress");
            return (Criteria) this;
        }

        public Criteria andRecAddressLike(String value) {
            addCriterion("C_REC_ADDRESS like", value, "recAddress");
            return (Criteria) this;
        }

        public Criteria andRecAddressNotLike(String value) {
            addCriterion("C_REC_ADDRESS not like", value, "recAddress");
            return (Criteria) this;
        }

        public Criteria andRecAddressIn(List<String> values) {
            addCriterion("C_REC_ADDRESS in", values, "recAddress");
            return (Criteria) this;
        }

        public Criteria andRecAddressNotIn(List<String> values) {
            addCriterion("C_REC_ADDRESS not in", values, "recAddress");
            return (Criteria) this;
        }

        public Criteria andRecAddressBetween(String value1, String value2) {
            addCriterion("C_REC_ADDRESS between", value1, value2, "recAddress");
            return (Criteria) this;
        }

        public Criteria andRecAddressNotBetween(String value1, String value2) {
            addCriterion("C_REC_ADDRESS not between", value1, value2, "recAddress");
            return (Criteria) this;
        }

        public Criteria andRecCompanyIsNull() {
            addCriterion("C_REC_COMPANY is null");
            return (Criteria) this;
        }

        public Criteria andRecCompanyIsNotNull() {
            addCriterion("C_REC_COMPANY is not null");
            return (Criteria) this;
        }

        public Criteria andRecCompanyEqualTo(String value) {
            addCriterion("C_REC_COMPANY =", value, "recCompany");
            return (Criteria) this;
        }

        public Criteria andRecCompanyNotEqualTo(String value) {
            addCriterion("C_REC_COMPANY <>", value, "recCompany");
            return (Criteria) this;
        }

        public Criteria andRecCompanyGreaterThan(String value) {
            addCriterion("C_REC_COMPANY >", value, "recCompany");
            return (Criteria) this;
        }

        public Criteria andRecCompanyGreaterThanOrEqualTo(String value) {
            addCriterion("C_REC_COMPANY >=", value, "recCompany");
            return (Criteria) this;
        }

        public Criteria andRecCompanyLessThan(String value) {
            addCriterion("C_REC_COMPANY <", value, "recCompany");
            return (Criteria) this;
        }

        public Criteria andRecCompanyLessThanOrEqualTo(String value) {
            addCriterion("C_REC_COMPANY <=", value, "recCompany");
            return (Criteria) this;
        }

        public Criteria andRecCompanyLike(String value) {
            addCriterion("C_REC_COMPANY like", value, "recCompany");
            return (Criteria) this;
        }

        public Criteria andRecCompanyNotLike(String value) {
            addCriterion("C_REC_COMPANY not like", value, "recCompany");
            return (Criteria) this;
        }

        public Criteria andRecCompanyIn(List<String> values) {
            addCriterion("C_REC_COMPANY in", values, "recCompany");
            return (Criteria) this;
        }

        public Criteria andRecCompanyNotIn(List<String> values) {
            addCriterion("C_REC_COMPANY not in", values, "recCompany");
            return (Criteria) this;
        }

        public Criteria andRecCompanyBetween(String value1, String value2) {
            addCriterion("C_REC_COMPANY between", value1, value2, "recCompany");
            return (Criteria) this;
        }

        public Criteria andRecCompanyNotBetween(String value1, String value2) {
            addCriterion("C_REC_COMPANY not between", value1, value2, "recCompany");
            return (Criteria) this;
        }

        public Criteria andRecMobileIsNull() {
            addCriterion("C_REC_MOBILE is null");
            return (Criteria) this;
        }

        public Criteria andRecMobileIsNotNull() {
            addCriterion("C_REC_MOBILE is not null");
            return (Criteria) this;
        }

        public Criteria andRecMobileEqualTo(String value) {
            addCriterion("C_REC_MOBILE =", value, "recMobile");
            return (Criteria) this;
        }

        public Criteria andRecMobileNotEqualTo(String value) {
            addCriterion("C_REC_MOBILE <>", value, "recMobile");
            return (Criteria) this;
        }

        public Criteria andRecMobileGreaterThan(String value) {
            addCriterion("C_REC_MOBILE >", value, "recMobile");
            return (Criteria) this;
        }

        public Criteria andRecMobileGreaterThanOrEqualTo(String value) {
            addCriterion("C_REC_MOBILE >=", value, "recMobile");
            return (Criteria) this;
        }

        public Criteria andRecMobileLessThan(String value) {
            addCriterion("C_REC_MOBILE <", value, "recMobile");
            return (Criteria) this;
        }

        public Criteria andRecMobileLessThanOrEqualTo(String value) {
            addCriterion("C_REC_MOBILE <=", value, "recMobile");
            return (Criteria) this;
        }

        public Criteria andRecMobileLike(String value) {
            addCriterion("C_REC_MOBILE like", value, "recMobile");
            return (Criteria) this;
        }

        public Criteria andRecMobileNotLike(String value) {
            addCriterion("C_REC_MOBILE not like", value, "recMobile");
            return (Criteria) this;
        }

        public Criteria andRecMobileIn(List<String> values) {
            addCriterion("C_REC_MOBILE in", values, "recMobile");
            return (Criteria) this;
        }

        public Criteria andRecMobileNotIn(List<String> values) {
            addCriterion("C_REC_MOBILE not in", values, "recMobile");
            return (Criteria) this;
        }

        public Criteria andRecMobileBetween(String value1, String value2) {
            addCriterion("C_REC_MOBILE between", value1, value2, "recMobile");
            return (Criteria) this;
        }

        public Criteria andRecMobileNotBetween(String value1, String value2) {
            addCriterion("C_REC_MOBILE not between", value1, value2, "recMobile");
            return (Criteria) this;
        }

        public Criteria andRecNameIsNull() {
            addCriterion("C_REC_NAME is null");
            return (Criteria) this;
        }

        public Criteria andRecNameIsNotNull() {
            addCriterion("C_REC_NAME is not null");
            return (Criteria) this;
        }

        public Criteria andRecNameEqualTo(String value) {
            addCriterion("C_REC_NAME =", value, "recName");
            return (Criteria) this;
        }

        public Criteria andRecNameNotEqualTo(String value) {
            addCriterion("C_REC_NAME <>", value, "recName");
            return (Criteria) this;
        }

        public Criteria andRecNameGreaterThan(String value) {
            addCriterion("C_REC_NAME >", value, "recName");
            return (Criteria) this;
        }

        public Criteria andRecNameGreaterThanOrEqualTo(String value) {
            addCriterion("C_REC_NAME >=", value, "recName");
            return (Criteria) this;
        }

        public Criteria andRecNameLessThan(String value) {
            addCriterion("C_REC_NAME <", value, "recName");
            return (Criteria) this;
        }

        public Criteria andRecNameLessThanOrEqualTo(String value) {
            addCriterion("C_REC_NAME <=", value, "recName");
            return (Criteria) this;
        }

        public Criteria andRecNameLike(String value) {
            addCriterion("C_REC_NAME like", value, "recName");
            return (Criteria) this;
        }

        public Criteria andRecNameNotLike(String value) {
            addCriterion("C_REC_NAME not like", value, "recName");
            return (Criteria) this;
        }

        public Criteria andRecNameIn(List<String> values) {
            addCriterion("C_REC_NAME in", values, "recName");
            return (Criteria) this;
        }

        public Criteria andRecNameNotIn(List<String> values) {
            addCriterion("C_REC_NAME not in", values, "recName");
            return (Criteria) this;
        }

        public Criteria andRecNameBetween(String value1, String value2) {
            addCriterion("C_REC_NAME between", value1, value2, "recName");
            return (Criteria) this;
        }

        public Criteria andRecNameNotBetween(String value1, String value2) {
            addCriterion("C_REC_NAME not between", value1, value2, "recName");
            return (Criteria) this;
        }

        public Criteria andRemarkIsNull() {
            addCriterion("C_REMARK is null");
            return (Criteria) this;
        }

        public Criteria andRemarkIsNotNull() {
            addCriterion("C_REMARK is not null");
            return (Criteria) this;
        }

        public Criteria andRemarkEqualTo(String value) {
            addCriterion("C_REMARK =", value, "remark");
            return (Criteria) this;
        }

        public Criteria andRemarkNotEqualTo(String value) {
            addCriterion("C_REMARK <>", value, "remark");
            return (Criteria) this;
        }

        public Criteria andRemarkGreaterThan(String value) {
            addCriterion("C_REMARK >", value, "remark");
            return (Criteria) this;
        }

        public Criteria andRemarkGreaterThanOrEqualTo(String value) {
            addCriterion("C_REMARK >=", value, "remark");
            return (Criteria) this;
        }

        public Criteria andRemarkLessThan(String value) {
            addCriterion("C_REMARK <", value, "remark");
            return (Criteria) this;
        }

        public Criteria andRemarkLessThanOrEqualTo(String value) {
            addCriterion("C_REMARK <=", value, "remark");
            return (Criteria) this;
        }

        public Criteria andRemarkLike(String value) {
            addCriterion("C_REMARK like", value, "remark");
            return (Criteria) this;
        }

        public Criteria andRemarkNotLike(String value) {
            addCriterion("C_REMARK not like", value, "remark");
            return (Criteria) this;
        }

        public Criteria andRemarkIn(List<String> values) {
            addCriterion("C_REMARK in", values, "remark");
            return (Criteria) this;
        }

        public Criteria andRemarkNotIn(List<String> values) {
            addCriterion("C_REMARK not in", values, "remark");
            return (Criteria) this;
        }

        public Criteria andRemarkBetween(String value1, String value2) {
            addCriterion("C_REMARK between", value1, value2, "remark");
            return (Criteria) this;
        }

        public Criteria andRemarkNotBetween(String value1, String value2) {
            addCriterion("C_REMARK not between", value1, value2, "remark");
            return (Criteria) this;
        }

        public Criteria andSendAddressIsNull() {
            addCriterion("C_SEND_ADDRESS is null");
            return (Criteria) this;
        }

        public Criteria andSendAddressIsNotNull() {
            addCriterion("C_SEND_ADDRESS is not null");
            return (Criteria) this;
        }

        public Criteria andSendAddressEqualTo(String value) {
            addCriterion("C_SEND_ADDRESS =", value, "sendAddress");
            return (Criteria) this;
        }

        public Criteria andSendAddressNotEqualTo(String value) {
            addCriterion("C_SEND_ADDRESS <>", value, "sendAddress");
            return (Criteria) this;
        }

        public Criteria andSendAddressGreaterThan(String value) {
            addCriterion("C_SEND_ADDRESS >", value, "sendAddress");
            return (Criteria) this;
        }

        public Criteria andSendAddressGreaterThanOrEqualTo(String value) {
            addCriterion("C_SEND_ADDRESS >=", value, "sendAddress");
            return (Criteria) this;
        }

        public Criteria andSendAddressLessThan(String value) {
            addCriterion("C_SEND_ADDRESS <", value, "sendAddress");
            return (Criteria) this;
        }

        public Criteria andSendAddressLessThanOrEqualTo(String value) {
            addCriterion("C_SEND_ADDRESS <=", value, "sendAddress");
            return (Criteria) this;
        }

        public Criteria andSendAddressLike(String value) {
            addCriterion("C_SEND_ADDRESS like", value, "sendAddress");
            return (Criteria) this;
        }

        public Criteria andSendAddressNotLike(String value) {
            addCriterion("C_SEND_ADDRESS not like", value, "sendAddress");
            return (Criteria) this;
        }

        public Criteria andSendAddressIn(List<String> values) {
            addCriterion("C_SEND_ADDRESS in", values, "sendAddress");
            return (Criteria) this;
        }

        public Criteria andSendAddressNotIn(List<String> values) {
            addCriterion("C_SEND_ADDRESS not in", values, "sendAddress");
            return (Criteria) this;
        }

        public Criteria andSendAddressBetween(String value1, String value2) {
            addCriterion("C_SEND_ADDRESS between", value1, value2, "sendAddress");
            return (Criteria) this;
        }

        public Criteria andSendAddressNotBetween(String value1, String value2) {
            addCriterion("C_SEND_ADDRESS not between", value1, value2, "sendAddress");
            return (Criteria) this;
        }

        public Criteria andSendCompanyIsNull() {
            addCriterion("C_SEND_COMPANY is null");
            return (Criteria) this;
        }

        public Criteria andSendCompanyIsNotNull() {
            addCriterion("C_SEND_COMPANY is not null");
            return (Criteria) this;
        }

        public Criteria andSendCompanyEqualTo(String value) {
            addCriterion("C_SEND_COMPANY =", value, "sendCompany");
            return (Criteria) this;
        }

        public Criteria andSendCompanyNotEqualTo(String value) {
            addCriterion("C_SEND_COMPANY <>", value, "sendCompany");
            return (Criteria) this;
        }

        public Criteria andSendCompanyGreaterThan(String value) {
            addCriterion("C_SEND_COMPANY >", value, "sendCompany");
            return (Criteria) this;
        }

        public Criteria andSendCompanyGreaterThanOrEqualTo(String value) {
            addCriterion("C_SEND_COMPANY >=", value, "sendCompany");
            return (Criteria) this;
        }

        public Criteria andSendCompanyLessThan(String value) {
            addCriterion("C_SEND_COMPANY <", value, "sendCompany");
            return (Criteria) this;
        }

        public Criteria andSendCompanyLessThanOrEqualTo(String value) {
            addCriterion("C_SEND_COMPANY <=", value, "sendCompany");
            return (Criteria) this;
        }

        public Criteria andSendCompanyLike(String value) {
            addCriterion("C_SEND_COMPANY like", value, "sendCompany");
            return (Criteria) this;
        }

        public Criteria andSendCompanyNotLike(String value) {
            addCriterion("C_SEND_COMPANY not like", value, "sendCompany");
            return (Criteria) this;
        }

        public Criteria andSendCompanyIn(List<String> values) {
            addCriterion("C_SEND_COMPANY in", values, "sendCompany");
            return (Criteria) this;
        }

        public Criteria andSendCompanyNotIn(List<String> values) {
            addCriterion("C_SEND_COMPANY not in", values, "sendCompany");
            return (Criteria) this;
        }

        public Criteria andSendCompanyBetween(String value1, String value2) {
            addCriterion("C_SEND_COMPANY between", value1, value2, "sendCompany");
            return (Criteria) this;
        }

        public Criteria andSendCompanyNotBetween(String value1, String value2) {
            addCriterion("C_SEND_COMPANY not between", value1, value2, "sendCompany");
            return (Criteria) this;
        }

        public Criteria andSendMobileIsNull() {
            addCriterion("C_SEND_MOBILE is null");
            return (Criteria) this;
        }

        public Criteria andSendMobileIsNotNull() {
            addCriterion("C_SEND_MOBILE is not null");
            return (Criteria) this;
        }

        public Criteria andSendMobileEqualTo(String value) {
            addCriterion("C_SEND_MOBILE =", value, "sendMobile");
            return (Criteria) this;
        }

        public Criteria andSendMobileNotEqualTo(String value) {
            addCriterion("C_SEND_MOBILE <>", value, "sendMobile");
            return (Criteria) this;
        }

        public Criteria andSendMobileGreaterThan(String value) {
            addCriterion("C_SEND_MOBILE >", value, "sendMobile");
            return (Criteria) this;
        }

        public Criteria andSendMobileGreaterThanOrEqualTo(String value) {
            addCriterion("C_SEND_MOBILE >=", value, "sendMobile");
            return (Criteria) this;
        }

        public Criteria andSendMobileLessThan(String value) {
            addCriterion("C_SEND_MOBILE <", value, "sendMobile");
            return (Criteria) this;
        }

        public Criteria andSendMobileLessThanOrEqualTo(String value) {
            addCriterion("C_SEND_MOBILE <=", value, "sendMobile");
            return (Criteria) this;
        }

        public Criteria andSendMobileLike(String value) {
            addCriterion("C_SEND_MOBILE like", value, "sendMobile");
            return (Criteria) this;
        }

        public Criteria andSendMobileNotLike(String value) {
            addCriterion("C_SEND_MOBILE not like", value, "sendMobile");
            return (Criteria) this;
        }

        public Criteria andSendMobileIn(List<String> values) {
            addCriterion("C_SEND_MOBILE in", values, "sendMobile");
            return (Criteria) this;
        }

        public Criteria andSendMobileNotIn(List<String> values) {
            addCriterion("C_SEND_MOBILE not in", values, "sendMobile");
            return (Criteria) this;
        }

        public Criteria andSendMobileBetween(String value1, String value2) {
            addCriterion("C_SEND_MOBILE between", value1, value2, "sendMobile");
            return (Criteria) this;
        }

        public Criteria andSendMobileNotBetween(String value1, String value2) {
            addCriterion("C_SEND_MOBILE not between", value1, value2, "sendMobile");
            return (Criteria) this;
        }

        public Criteria andSendNameIsNull() {
            addCriterion("C_SEND_NAME is null");
            return (Criteria) this;
        }

        public Criteria andSendNameIsNotNull() {
            addCriterion("C_SEND_NAME is not null");
            return (Criteria) this;
        }

        public Criteria andSendNameEqualTo(String value) {
            addCriterion("C_SEND_NAME =", value, "sendName");
            return (Criteria) this;
        }

        public Criteria andSendNameNotEqualTo(String value) {
            addCriterion("C_SEND_NAME <>", value, "sendName");
            return (Criteria) this;
        }

        public Criteria andSendNameGreaterThan(String value) {
            addCriterion("C_SEND_NAME >", value, "sendName");
            return (Criteria) this;
        }

        public Criteria andSendNameGreaterThanOrEqualTo(String value) {
            addCriterion("C_SEND_NAME >=", value, "sendName");
            return (Criteria) this;
        }

        public Criteria andSendNameLessThan(String value) {
            addCriterion("C_SEND_NAME <", value, "sendName");
            return (Criteria) this;
        }

        public Criteria andSendNameLessThanOrEqualTo(String value) {
            addCriterion("C_SEND_NAME <=", value, "sendName");
            return (Criteria) this;
        }

        public Criteria andSendNameLike(String value) {
            addCriterion("C_SEND_NAME like", value, "sendName");
            return (Criteria) this;
        }

        public Criteria andSendNameNotLike(String value) {
            addCriterion("C_SEND_NAME not like", value, "sendName");
            return (Criteria) this;
        }

        public Criteria andSendNameIn(List<String> values) {
            addCriterion("C_SEND_NAME in", values, "sendName");
            return (Criteria) this;
        }

        public Criteria andSendNameNotIn(List<String> values) {
            addCriterion("C_SEND_NAME not in", values, "sendName");
            return (Criteria) this;
        }

        public Criteria andSendNameBetween(String value1, String value2) {
            addCriterion("C_SEND_NAME between", value1, value2, "sendName");
            return (Criteria) this;
        }

        public Criteria andSendNameNotBetween(String value1, String value2) {
            addCriterion("C_SEND_NAME not between", value1, value2, "sendName");
            return (Criteria) this;
        }

        public Criteria andSendProNumIsNull() {
            addCriterion("C_SEND_PRO_NUM is null");
            return (Criteria) this;
        }

        public Criteria andSendProNumIsNotNull() {
            addCriterion("C_SEND_PRO_NUM is not null");
            return (Criteria) this;
        }

        public Criteria andSendProNumEqualTo(String value) {
            addCriterion("C_SEND_PRO_NUM =", value, "sendProNum");
            return (Criteria) this;
        }

        public Criteria andSendProNumNotEqualTo(String value) {
            addCriterion("C_SEND_PRO_NUM <>", value, "sendProNum");
            return (Criteria) this;
        }

        public Criteria andSendProNumGreaterThan(String value) {
            addCriterion("C_SEND_PRO_NUM >", value, "sendProNum");
            return (Criteria) this;
        }

        public Criteria andSendProNumGreaterThanOrEqualTo(String value) {
            addCriterion("C_SEND_PRO_NUM >=", value, "sendProNum");
            return (Criteria) this;
        }

        public Criteria andSendProNumLessThan(String value) {
            addCriterion("C_SEND_PRO_NUM <", value, "sendProNum");
            return (Criteria) this;
        }

        public Criteria andSendProNumLessThanOrEqualTo(String value) {
            addCriterion("C_SEND_PRO_NUM <=", value, "sendProNum");
            return (Criteria) this;
        }

        public Criteria andSendProNumLike(String value) {
            addCriterion("C_SEND_PRO_NUM like", value, "sendProNum");
            return (Criteria) this;
        }

        public Criteria andSendProNumNotLike(String value) {
            addCriterion("C_SEND_PRO_NUM not like", value, "sendProNum");
            return (Criteria) this;
        }

        public Criteria andSendProNumIn(List<String> values) {
            addCriterion("C_SEND_PRO_NUM in", values, "sendProNum");
            return (Criteria) this;
        }

        public Criteria andSendProNumNotIn(List<String> values) {
            addCriterion("C_SEND_PRO_NUM not in", values, "sendProNum");
            return (Criteria) this;
        }

        public Criteria andSendProNumBetween(String value1, String value2) {
            addCriterion("C_SEND_PRO_NUM between", value1, value2, "sendProNum");
            return (Criteria) this;
        }

        public Criteria andSendProNumNotBetween(String value1, String value2) {
            addCriterion("C_SEND_PRO_NUM not between", value1, value2, "sendProNum");
            return (Criteria) this;
        }

        public Criteria andSignStatusIsNull() {
            addCriterion("C_SIGN_STATUS is null");
            return (Criteria) this;
        }

        public Criteria andSignStatusIsNotNull() {
            addCriterion("C_SIGN_STATUS is not null");
            return (Criteria) this;
        }

        public Criteria andSignStatusEqualTo(Integer value) {
            addCriterion("C_SIGN_STATUS =", value, "signStatus");
            return (Criteria) this;
        }

        public Criteria andSignStatusNotEqualTo(Integer value) {
            addCriterion("C_SIGN_STATUS <>", value, "signStatus");
            return (Criteria) this;
        }

        public Criteria andSignStatusGreaterThan(Integer value) {
            addCriterion("C_SIGN_STATUS >", value, "signStatus");
            return (Criteria) this;
        }

        public Criteria andSignStatusGreaterThanOrEqualTo(Integer value) {
            addCriterion("C_SIGN_STATUS >=", value, "signStatus");
            return (Criteria) this;
        }

        public Criteria andSignStatusLessThan(Integer value) {
            addCriterion("C_SIGN_STATUS <", value, "signStatus");
            return (Criteria) this;
        }

        public Criteria andSignStatusLessThanOrEqualTo(Integer value) {
            addCriterion("C_SIGN_STATUS <=", value, "signStatus");
            return (Criteria) this;
        }

        public Criteria andSignStatusIn(List<Integer> values) {
            addCriterion("C_SIGN_STATUS in", values, "signStatus");
            return (Criteria) this;
        }

        public Criteria andSignStatusNotIn(List<Integer> values) {
            addCriterion("C_SIGN_STATUS not in", values, "signStatus");
            return (Criteria) this;
        }

        public Criteria andSignStatusBetween(Integer value1, Integer value2) {
            addCriterion("C_SIGN_STATUS between", value1, value2, "signStatus");
            return (Criteria) this;
        }

        public Criteria andSignStatusNotBetween(Integer value1, Integer value2) {
            addCriterion("C_SIGN_STATUS not between", value1, value2, "signStatus");
            return (Criteria) this;
        }

        public Criteria andVolIsNull() {
            addCriterion("C_VOL is null");
            return (Criteria) this;
        }

        public Criteria andVolIsNotNull() {
            addCriterion("C_VOL is not null");
            return (Criteria) this;
        }

        public Criteria andVolEqualTo(String value) {
            addCriterion("C_VOL =", value, "vol");
            return (Criteria) this;
        }

        public Criteria andVolNotEqualTo(String value) {
            addCriterion("C_VOL <>", value, "vol");
            return (Criteria) this;
        }

        public Criteria andVolGreaterThan(String value) {
            addCriterion("C_VOL >", value, "vol");
            return (Criteria) this;
        }

        public Criteria andVolGreaterThanOrEqualTo(String value) {
            addCriterion("C_VOL >=", value, "vol");
            return (Criteria) this;
        }

        public Criteria andVolLessThan(String value) {
            addCriterion("C_VOL <", value, "vol");
            return (Criteria) this;
        }

        public Criteria andVolLessThanOrEqualTo(String value) {
            addCriterion("C_VOL <=", value, "vol");
            return (Criteria) this;
        }

        public Criteria andVolLike(String value) {
            addCriterion("C_VOL like", value, "vol");
            return (Criteria) this;
        }

        public Criteria andVolNotLike(String value) {
            addCriterion("C_VOL not like", value, "vol");
            return (Criteria) this;
        }

        public Criteria andVolIn(List<String> values) {
            addCriterion("C_VOL in", values, "vol");
            return (Criteria) this;
        }

        public Criteria andVolNotIn(List<String> values) {
            addCriterion("C_VOL not in", values, "vol");
            return (Criteria) this;
        }

        public Criteria andVolBetween(String value1, String value2) {
            addCriterion("C_VOL between", value1, value2, "vol");
            return (Criteria) this;
        }

        public Criteria andVolNotBetween(String value1, String value2) {
            addCriterion("C_VOL not between", value1, value2, "vol");
            return (Criteria) this;
        }

        public Criteria andWayBillNumIsNull() {
            addCriterion("C_WAY_BILL_NUM is null");
            return (Criteria) this;
        }

        public Criteria andWayBillNumIsNotNull() {
            addCriterion("C_WAY_BILL_NUM is not null");
            return (Criteria) this;
        }

        public Criteria andWayBillNumEqualTo(String value) {
            addCriterion("C_WAY_BILL_NUM =", value, "wayBillNum");
            return (Criteria) this;
        }

        public Criteria andWayBillNumNotEqualTo(String value) {
            addCriterion("C_WAY_BILL_NUM <>", value, "wayBillNum");
            return (Criteria) this;
        }

        public Criteria andWayBillNumGreaterThan(String value) {
            addCriterion("C_WAY_BILL_NUM >", value, "wayBillNum");
            return (Criteria) this;
        }

        public Criteria andWayBillNumGreaterThanOrEqualTo(String value) {
            addCriterion("C_WAY_BILL_NUM >=", value, "wayBillNum");
            return (Criteria) this;
        }

        public Criteria andWayBillNumLessThan(String value) {
            addCriterion("C_WAY_BILL_NUM <", value, "wayBillNum");
            return (Criteria) this;
        }

        public Criteria andWayBillNumLessThanOrEqualTo(String value) {
            addCriterion("C_WAY_BILL_NUM <=", value, "wayBillNum");
            return (Criteria) this;
        }

        public Criteria andWayBillNumLike(String value) {
            addCriterion("C_WAY_BILL_NUM like", value, "wayBillNum");
            return (Criteria) this;
        }

        public Criteria andWayBillNumNotLike(String value) {
            addCriterion("C_WAY_BILL_NUM not like", value, "wayBillNum");
            return (Criteria) this;
        }

        public Criteria andWayBillNumIn(List<String> values) {
            addCriterion("C_WAY_BILL_NUM in", values, "wayBillNum");
            return (Criteria) this;
        }

        public Criteria andWayBillNumNotIn(List<String> values) {
            addCriterion("C_WAY_BILL_NUM not in", values, "wayBillNum");
            return (Criteria) this;
        }

        public Criteria andWayBillNumBetween(String value1, String value2) {
            addCriterion("C_WAY_BILL_NUM between", value1, value2, "wayBillNum");
            return (Criteria) this;
        }

        public Criteria andWayBillNumNotBetween(String value1, String value2) {
            addCriterion("C_WAY_BILL_NUM not between", value1, value2, "wayBillNum");
            return (Criteria) this;
        }

        public Criteria andWayBillTypeIsNull() {
            addCriterion("C_WAY_BILL_TYPE is null");
            return (Criteria) this;
        }

        public Criteria andWayBillTypeIsNotNull() {
            addCriterion("C_WAY_BILL_TYPE is not null");
            return (Criteria) this;
        }

        public Criteria andWayBillTypeEqualTo(String value) {
            addCriterion("C_WAY_BILL_TYPE =", value, "wayBillType");
            return (Criteria) this;
        }

        public Criteria andWayBillTypeNotEqualTo(String value) {
            addCriterion("C_WAY_BILL_TYPE <>", value, "wayBillType");
            return (Criteria) this;
        }

        public Criteria andWayBillTypeGreaterThan(String value) {
            addCriterion("C_WAY_BILL_TYPE >", value, "wayBillType");
            return (Criteria) this;
        }

        public Criteria andWayBillTypeGreaterThanOrEqualTo(String value) {
            addCriterion("C_WAY_BILL_TYPE >=", value, "wayBillType");
            return (Criteria) this;
        }

        public Criteria andWayBillTypeLessThan(String value) {
            addCriterion("C_WAY_BILL_TYPE <", value, "wayBillType");
            return (Criteria) this;
        }

        public Criteria andWayBillTypeLessThanOrEqualTo(String value) {
            addCriterion("C_WAY_BILL_TYPE <=", value, "wayBillType");
            return (Criteria) this;
        }

        public Criteria andWayBillTypeLike(String value) {
            addCriterion("C_WAY_BILL_TYPE like", value, "wayBillType");
            return (Criteria) this;
        }

        public Criteria andWayBillTypeNotLike(String value) {
            addCriterion("C_WAY_BILL_TYPE not like", value, "wayBillType");
            return (Criteria) this;
        }

        public Criteria andWayBillTypeIn(List<String> values) {
            addCriterion("C_WAY_BILL_TYPE in", values, "wayBillType");
            return (Criteria) this;
        }

        public Criteria andWayBillTypeNotIn(List<String> values) {
            addCriterion("C_WAY_BILL_TYPE not in", values, "wayBillType");
            return (Criteria) this;
        }

        public Criteria andWayBillTypeBetween(String value1, String value2) {
            addCriterion("C_WAY_BILL_TYPE between", value1, value2, "wayBillType");
            return (Criteria) this;
        }

        public Criteria andWayBillTypeNotBetween(String value1, String value2) {
            addCriterion("C_WAY_BILL_TYPE not between", value1, value2, "wayBillType");
            return (Criteria) this;
        }

        public Criteria andWeightIsNull() {
            addCriterion("C_WEIGHT is null");
            return (Criteria) this;
        }

        public Criteria andWeightIsNotNull() {
            addCriterion("C_WEIGHT is not null");
            return (Criteria) this;
        }

        public Criteria andWeightEqualTo(Double value) {
            addCriterion("C_WEIGHT =", value, "weight");
            return (Criteria) this;
        }

        public Criteria andWeightNotEqualTo(Double value) {
            addCriterion("C_WEIGHT <>", value, "weight");
            return (Criteria) this;
        }

        public Criteria andWeightGreaterThan(Double value) {
            addCriterion("C_WEIGHT >", value, "weight");
            return (Criteria) this;
        }

        public Criteria andWeightGreaterThanOrEqualTo(Double value) {
            addCriterion("C_WEIGHT >=", value, "weight");
            return (Criteria) this;
        }

        public Criteria andWeightLessThan(Double value) {
            addCriterion("C_WEIGHT <", value, "weight");
            return (Criteria) this;
        }

        public Criteria andWeightLessThanOrEqualTo(Double value) {
            addCriterion("C_WEIGHT <=", value, "weight");
            return (Criteria) this;
        }

        public Criteria andWeightIn(List<Double> values) {
            addCriterion("C_WEIGHT in", values, "weight");
            return (Criteria) this;
        }

        public Criteria andWeightNotIn(List<Double> values) {
            addCriterion("C_WEIGHT not in", values, "weight");
            return (Criteria) this;
        }

        public Criteria andWeightBetween(Double value1, Double value2) {
            addCriterion("C_WEIGHT between", value1, value2, "weight");
            return (Criteria) this;
        }

        public Criteria andWeightNotBetween(Double value1, Double value2) {
            addCriterion("C_WEIGHT not between", value1, value2, "weight");
            return (Criteria) this;
        }

        public Criteria andOrderIdIsNull() {
            addCriterion("C_ORDER_ID is null");
            return (Criteria) this;
        }

        public Criteria andOrderIdIsNotNull() {
            addCriterion("C_ORDER_ID is not null");
            return (Criteria) this;
        }

        public Criteria andOrderIdEqualTo(Integer value) {
            addCriterion("C_ORDER_ID =", value, "orderId");
            return (Criteria) this;
        }

        public Criteria andOrderIdNotEqualTo(Integer value) {
            addCriterion("C_ORDER_ID <>", value, "orderId");
            return (Criteria) this;
        }

        public Criteria andOrderIdGreaterThan(Integer value) {
            addCriterion("C_ORDER_ID >", value, "orderId");
            return (Criteria) this;
        }

        public Criteria andOrderIdGreaterThanOrEqualTo(Integer value) {
            addCriterion("C_ORDER_ID >=", value, "orderId");
            return (Criteria) this;
        }

        public Criteria andOrderIdLessThan(Integer value) {
            addCriterion("C_ORDER_ID <", value, "orderId");
            return (Criteria) this;
        }

        public Criteria andOrderIdLessThanOrEqualTo(Integer value) {
            addCriterion("C_ORDER_ID <=", value, "orderId");
            return (Criteria) this;
        }

        public Criteria andOrderIdIn(List<Integer> values) {
            addCriterion("C_ORDER_ID in", values, "orderId");
            return (Criteria) this;
        }

        public Criteria andOrderIdNotIn(List<Integer> values) {
            addCriterion("C_ORDER_ID not in", values, "orderId");
            return (Criteria) this;
        }

        public Criteria andOrderIdBetween(Integer value1, Integer value2) {
            addCriterion("C_ORDER_ID between", value1, value2, "orderId");
            return (Criteria) this;
        }

        public Criteria andOrderIdNotBetween(Integer value1, Integer value2) {
            addCriterion("C_ORDER_ID not between", value1, value2, "orderId");
            return (Criteria) this;
        }

        public Criteria andRecAreaIdIsNull() {
            addCriterion("C_REC_AREA_ID is null");
            return (Criteria) this;
        }

        public Criteria andRecAreaIdIsNotNull() {
            addCriterion("C_REC_AREA_ID is not null");
            return (Criteria) this;
        }

        public Criteria andRecAreaIdEqualTo(String value) {
            addCriterion("C_REC_AREA_ID =", value, "recAreaId");
            return (Criteria) this;
        }

        public Criteria andRecAreaIdNotEqualTo(String value) {
            addCriterion("C_REC_AREA_ID <>", value, "recAreaId");
            return (Criteria) this;
        }

        public Criteria andRecAreaIdGreaterThan(String value) {
            addCriterion("C_REC_AREA_ID >", value, "recAreaId");
            return (Criteria) this;
        }

        public Criteria andRecAreaIdGreaterThanOrEqualTo(String value) {
            addCriterion("C_REC_AREA_ID >=", value, "recAreaId");
            return (Criteria) this;
        }

        public Criteria andRecAreaIdLessThan(String value) {
            addCriterion("C_REC_AREA_ID <", value, "recAreaId");
            return (Criteria) this;
        }

        public Criteria andRecAreaIdLessThanOrEqualTo(String value) {
            addCriterion("C_REC_AREA_ID <=", value, "recAreaId");
            return (Criteria) this;
        }

        public Criteria andRecAreaIdLike(String value) {
            addCriterion("C_REC_AREA_ID like", value, "recAreaId");
            return (Criteria) this;
        }

        public Criteria andRecAreaIdNotLike(String value) {
            addCriterion("C_REC_AREA_ID not like", value, "recAreaId");
            return (Criteria) this;
        }

        public Criteria andRecAreaIdIn(List<String> values) {
            addCriterion("C_REC_AREA_ID in", values, "recAreaId");
            return (Criteria) this;
        }

        public Criteria andRecAreaIdNotIn(List<String> values) {
            addCriterion("C_REC_AREA_ID not in", values, "recAreaId");
            return (Criteria) this;
        }

        public Criteria andRecAreaIdBetween(String value1, String value2) {
            addCriterion("C_REC_AREA_ID between", value1, value2, "recAreaId");
            return (Criteria) this;
        }

        public Criteria andRecAreaIdNotBetween(String value1, String value2) {
            addCriterion("C_REC_AREA_ID not between", value1, value2, "recAreaId");
            return (Criteria) this;
        }

        public Criteria andSendAreaIdIsNull() {
            addCriterion("C_SEND_AREA_ID is null");
            return (Criteria) this;
        }

        public Criteria andSendAreaIdIsNotNull() {
            addCriterion("C_SEND_AREA_ID is not null");
            return (Criteria) this;
        }

        public Criteria andSendAreaIdEqualTo(String value) {
            addCriterion("C_SEND_AREA_ID =", value, "sendAreaId");
            return (Criteria) this;
        }

        public Criteria andSendAreaIdNotEqualTo(String value) {
            addCriterion("C_SEND_AREA_ID <>", value, "sendAreaId");
            return (Criteria) this;
        }

        public Criteria andSendAreaIdGreaterThan(String value) {
            addCriterion("C_SEND_AREA_ID >", value, "sendAreaId");
            return (Criteria) this;
        }

        public Criteria andSendAreaIdGreaterThanOrEqualTo(String value) {
            addCriterion("C_SEND_AREA_ID >=", value, "sendAreaId");
            return (Criteria) this;
        }

        public Criteria andSendAreaIdLessThan(String value) {
            addCriterion("C_SEND_AREA_ID <", value, "sendAreaId");
            return (Criteria) this;
        }

        public Criteria andSendAreaIdLessThanOrEqualTo(String value) {
            addCriterion("C_SEND_AREA_ID <=", value, "sendAreaId");
            return (Criteria) this;
        }

        public Criteria andSendAreaIdLike(String value) {
            addCriterion("C_SEND_AREA_ID like", value, "sendAreaId");
            return (Criteria) this;
        }

        public Criteria andSendAreaIdNotLike(String value) {
            addCriterion("C_SEND_AREA_ID not like", value, "sendAreaId");
            return (Criteria) this;
        }

        public Criteria andSendAreaIdIn(List<String> values) {
            addCriterion("C_SEND_AREA_ID in", values, "sendAreaId");
            return (Criteria) this;
        }

        public Criteria andSendAreaIdNotIn(List<String> values) {
            addCriterion("C_SEND_AREA_ID not in", values, "sendAreaId");
            return (Criteria) this;
        }

        public Criteria andSendAreaIdBetween(String value1, String value2) {
            addCriterion("C_SEND_AREA_ID between", value1, value2, "sendAreaId");
            return (Criteria) this;
        }

        public Criteria andSendAreaIdNotBetween(String value1, String value2) {
            addCriterion("C_SEND_AREA_ID not between", value1, value2, "sendAreaId");
            return (Criteria) this;
        }
    }

    public static class Criteria extends GeneratedCriteria {

        protected Criteria() {
            super();
        }
    }

    public static class Criterion {
        private String condition;

        private Object value;

        private Object secondValue;

        private boolean noValue;

        private boolean singleValue;

        private boolean betweenValue;

        private boolean listValue;

        private String typeHandler;

        public String getCondition() {
            return condition;
        }

        public Object getValue() {
            return value;
        }

        public Object getSecondValue() {
            return secondValue;
        }

        public boolean isNoValue() {
            return noValue;
        }

        public boolean isSingleValue() {
            return singleValue;
        }

        public boolean isBetweenValue() {
            return betweenValue;
        }

        public boolean isListValue() {
            return listValue;
        }

        public String getTypeHandler() {
            return typeHandler;
        }

        protected Criterion(String condition) {
            super();
            this.condition = condition;
            this.typeHandler = null;
            this.noValue = true;
        }

        protected Criterion(String condition, Object value, String typeHandler) {
            super();
            this.condition = condition;
            this.value = value;
            this.typeHandler = typeHandler;
            if (value instanceof List<?>) {
                this.listValue = true;
            } else {
                this.singleValue = true;
            }
        }

        protected Criterion(String condition, Object value) {
            this(condition, value, null);
        }

        protected Criterion(String condition, Object value, Object secondValue, String typeHandler) {
            super();
            this.condition = condition;
            this.value = value;
            this.secondValue = secondValue;
            this.typeHandler = typeHandler;
            this.betweenValue = true;
        }

        protected Criterion(String condition, Object value, Object secondValue) {
            this(condition, value, secondValue, null);
        }
    }
}