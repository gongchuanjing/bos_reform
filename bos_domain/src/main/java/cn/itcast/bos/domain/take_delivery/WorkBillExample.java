package cn.itcast.bos.domain.take_delivery;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class WorkBillExample {
    protected String orderByClause;

    protected boolean distinct;

    protected List<Criteria> oredCriteria;

    public WorkBillExample() {
        oredCriteria = new ArrayList<Criteria>();
    }

    public void setOrderByClause(String orderByClause) {
        this.orderByClause = orderByClause;
    }

    public String getOrderByClause() {
        return orderByClause;
    }

    public void setDistinct(boolean distinct) {
        this.distinct = distinct;
    }

    public boolean isDistinct() {
        return distinct;
    }

    public List<Criteria> getOredCriteria() {
        return oredCriteria;
    }

    public void or(Criteria criteria) {
        oredCriteria.add(criteria);
    }

    public Criteria or() {
        Criteria criteria = createCriteriaInternal();
        oredCriteria.add(criteria);
        return criteria;
    }

    public Criteria createCriteria() {
        Criteria criteria = createCriteriaInternal();
        if (oredCriteria.size() == 0) {
            oredCriteria.add(criteria);
        }
        return criteria;
    }

    protected Criteria createCriteriaInternal() {
        Criteria criteria = new Criteria();
        return criteria;
    }

    public void clear() {
        oredCriteria.clear();
        orderByClause = null;
        distinct = false;
    }

    protected abstract static class GeneratedCriteria {
        protected List<Criterion> criteria;

        protected GeneratedCriteria() {
            super();
            criteria = new ArrayList<Criterion>();
        }

        public boolean isValid() {
            return criteria.size() > 0;
        }

        public List<Criterion> getAllCriteria() {
            return criteria;
        }

        public List<Criterion> getCriteria() {
            return criteria;
        }

        protected void addCriterion(String condition) {
            if (condition == null) {
                throw new RuntimeException("Value for condition cannot be null");
            }
            criteria.add(new Criterion(condition));
        }

        protected void addCriterion(String condition, Object value, String property) {
            if (value == null) {
                throw new RuntimeException("Value for " + property + " cannot be null");
            }
            criteria.add(new Criterion(condition, value));
        }

        protected void addCriterion(String condition, Object value1, Object value2, String property) {
            if (value1 == null || value2 == null) {
                throw new RuntimeException("Between values for " + property + " cannot be null");
            }
            criteria.add(new Criterion(condition, value1, value2));
        }

        public Criteria andIdIsNull() {
            addCriterion("C_ID is null");
            return (Criteria) this;
        }

        public Criteria andIdIsNotNull() {
            addCriterion("C_ID is not null");
            return (Criteria) this;
        }

        public Criteria andIdEqualTo(Integer value) {
            addCriterion("C_ID =", value, "id");
            return (Criteria) this;
        }

        public Criteria andIdNotEqualTo(Integer value) {
            addCriterion("C_ID <>", value, "id");
            return (Criteria) this;
        }

        public Criteria andIdGreaterThan(Integer value) {
            addCriterion("C_ID >", value, "id");
            return (Criteria) this;
        }

        public Criteria andIdGreaterThanOrEqualTo(Integer value) {
            addCriterion("C_ID >=", value, "id");
            return (Criteria) this;
        }

        public Criteria andIdLessThan(Integer value) {
            addCriterion("C_ID <", value, "id");
            return (Criteria) this;
        }

        public Criteria andIdLessThanOrEqualTo(Integer value) {
            addCriterion("C_ID <=", value, "id");
            return (Criteria) this;
        }

        public Criteria andIdIn(List<Integer> values) {
            addCriterion("C_ID in", values, "id");
            return (Criteria) this;
        }

        public Criteria andIdNotIn(List<Integer> values) {
            addCriterion("C_ID not in", values, "id");
            return (Criteria) this;
        }

        public Criteria andIdBetween(Integer value1, Integer value2) {
            addCriterion("C_ID between", value1, value2, "id");
            return (Criteria) this;
        }

        public Criteria andIdNotBetween(Integer value1, Integer value2) {
            addCriterion("C_ID not between", value1, value2, "id");
            return (Criteria) this;
        }

        public Criteria andAttachbilltimesIsNull() {
            addCriterion("C_ATTACHBILLTIMES is null");
            return (Criteria) this;
        }

        public Criteria andAttachbilltimesIsNotNull() {
            addCriterion("C_ATTACHBILLTIMES is not null");
            return (Criteria) this;
        }

        public Criteria andAttachbilltimesEqualTo(Integer value) {
            addCriterion("C_ATTACHBILLTIMES =", value, "attachbilltimes");
            return (Criteria) this;
        }

        public Criteria andAttachbilltimesNotEqualTo(Integer value) {
            addCriterion("C_ATTACHBILLTIMES <>", value, "attachbilltimes");
            return (Criteria) this;
        }

        public Criteria andAttachbilltimesGreaterThan(Integer value) {
            addCriterion("C_ATTACHBILLTIMES >", value, "attachbilltimes");
            return (Criteria) this;
        }

        public Criteria andAttachbilltimesGreaterThanOrEqualTo(Integer value) {
            addCriterion("C_ATTACHBILLTIMES >=", value, "attachbilltimes");
            return (Criteria) this;
        }

        public Criteria andAttachbilltimesLessThan(Integer value) {
            addCriterion("C_ATTACHBILLTIMES <", value, "attachbilltimes");
            return (Criteria) this;
        }

        public Criteria andAttachbilltimesLessThanOrEqualTo(Integer value) {
            addCriterion("C_ATTACHBILLTIMES <=", value, "attachbilltimes");
            return (Criteria) this;
        }

        public Criteria andAttachbilltimesIn(List<Integer> values) {
            addCriterion("C_ATTACHBILLTIMES in", values, "attachbilltimes");
            return (Criteria) this;
        }

        public Criteria andAttachbilltimesNotIn(List<Integer> values) {
            addCriterion("C_ATTACHBILLTIMES not in", values, "attachbilltimes");
            return (Criteria) this;
        }

        public Criteria andAttachbilltimesBetween(Integer value1, Integer value2) {
            addCriterion("C_ATTACHBILLTIMES between", value1, value2, "attachbilltimes");
            return (Criteria) this;
        }

        public Criteria andAttachbilltimesNotBetween(Integer value1, Integer value2) {
            addCriterion("C_ATTACHBILLTIMES not between", value1, value2, "attachbilltimes");
            return (Criteria) this;
        }

        public Criteria andBuildtimeIsNull() {
            addCriterion("C_BUILDTIME is null");
            return (Criteria) this;
        }

        public Criteria andBuildtimeIsNotNull() {
            addCriterion("C_BUILDTIME is not null");
            return (Criteria) this;
        }

        public Criteria andBuildtimeEqualTo(Date value) {
            addCriterion("C_BUILDTIME =", value, "buildtime");
            return (Criteria) this;
        }

        public Criteria andBuildtimeNotEqualTo(Date value) {
            addCriterion("C_BUILDTIME <>", value, "buildtime");
            return (Criteria) this;
        }

        public Criteria andBuildtimeGreaterThan(Date value) {
            addCriterion("C_BUILDTIME >", value, "buildtime");
            return (Criteria) this;
        }

        public Criteria andBuildtimeGreaterThanOrEqualTo(Date value) {
            addCriterion("C_BUILDTIME >=", value, "buildtime");
            return (Criteria) this;
        }

        public Criteria andBuildtimeLessThan(Date value) {
            addCriterion("C_BUILDTIME <", value, "buildtime");
            return (Criteria) this;
        }

        public Criteria andBuildtimeLessThanOrEqualTo(Date value) {
            addCriterion("C_BUILDTIME <=", value, "buildtime");
            return (Criteria) this;
        }

        public Criteria andBuildtimeIn(List<Date> values) {
            addCriterion("C_BUILDTIME in", values, "buildtime");
            return (Criteria) this;
        }

        public Criteria andBuildtimeNotIn(List<Date> values) {
            addCriterion("C_BUILDTIME not in", values, "buildtime");
            return (Criteria) this;
        }

        public Criteria andBuildtimeBetween(Date value1, Date value2) {
            addCriterion("C_BUILDTIME between", value1, value2, "buildtime");
            return (Criteria) this;
        }

        public Criteria andBuildtimeNotBetween(Date value1, Date value2) {
            addCriterion("C_BUILDTIME not between", value1, value2, "buildtime");
            return (Criteria) this;
        }

        public Criteria andPickstateIsNull() {
            addCriterion("C_PICKSTATE is null");
            return (Criteria) this;
        }

        public Criteria andPickstateIsNotNull() {
            addCriterion("C_PICKSTATE is not null");
            return (Criteria) this;
        }

        public Criteria andPickstateEqualTo(String value) {
            addCriterion("C_PICKSTATE =", value, "pickstate");
            return (Criteria) this;
        }

        public Criteria andPickstateNotEqualTo(String value) {
            addCriterion("C_PICKSTATE <>", value, "pickstate");
            return (Criteria) this;
        }

        public Criteria andPickstateGreaterThan(String value) {
            addCriterion("C_PICKSTATE >", value, "pickstate");
            return (Criteria) this;
        }

        public Criteria andPickstateGreaterThanOrEqualTo(String value) {
            addCriterion("C_PICKSTATE >=", value, "pickstate");
            return (Criteria) this;
        }

        public Criteria andPickstateLessThan(String value) {
            addCriterion("C_PICKSTATE <", value, "pickstate");
            return (Criteria) this;
        }

        public Criteria andPickstateLessThanOrEqualTo(String value) {
            addCriterion("C_PICKSTATE <=", value, "pickstate");
            return (Criteria) this;
        }

        public Criteria andPickstateLike(String value) {
            addCriterion("C_PICKSTATE like", value, "pickstate");
            return (Criteria) this;
        }

        public Criteria andPickstateNotLike(String value) {
            addCriterion("C_PICKSTATE not like", value, "pickstate");
            return (Criteria) this;
        }

        public Criteria andPickstateIn(List<String> values) {
            addCriterion("C_PICKSTATE in", values, "pickstate");
            return (Criteria) this;
        }

        public Criteria andPickstateNotIn(List<String> values) {
            addCriterion("C_PICKSTATE not in", values, "pickstate");
            return (Criteria) this;
        }

        public Criteria andPickstateBetween(String value1, String value2) {
            addCriterion("C_PICKSTATE between", value1, value2, "pickstate");
            return (Criteria) this;
        }

        public Criteria andPickstateNotBetween(String value1, String value2) {
            addCriterion("C_PICKSTATE not between", value1, value2, "pickstate");
            return (Criteria) this;
        }

        public Criteria andRemarkIsNull() {
            addCriterion("C_REMARK is null");
            return (Criteria) this;
        }

        public Criteria andRemarkIsNotNull() {
            addCriterion("C_REMARK is not null");
            return (Criteria) this;
        }

        public Criteria andRemarkEqualTo(String value) {
            addCriterion("C_REMARK =", value, "remark");
            return (Criteria) this;
        }

        public Criteria andRemarkNotEqualTo(String value) {
            addCriterion("C_REMARK <>", value, "remark");
            return (Criteria) this;
        }

        public Criteria andRemarkGreaterThan(String value) {
            addCriterion("C_REMARK >", value, "remark");
            return (Criteria) this;
        }

        public Criteria andRemarkGreaterThanOrEqualTo(String value) {
            addCriterion("C_REMARK >=", value, "remark");
            return (Criteria) this;
        }

        public Criteria andRemarkLessThan(String value) {
            addCriterion("C_REMARK <", value, "remark");
            return (Criteria) this;
        }

        public Criteria andRemarkLessThanOrEqualTo(String value) {
            addCriterion("C_REMARK <=", value, "remark");
            return (Criteria) this;
        }

        public Criteria andRemarkLike(String value) {
            addCriterion("C_REMARK like", value, "remark");
            return (Criteria) this;
        }

        public Criteria andRemarkNotLike(String value) {
            addCriterion("C_REMARK not like", value, "remark");
            return (Criteria) this;
        }

        public Criteria andRemarkIn(List<String> values) {
            addCriterion("C_REMARK in", values, "remark");
            return (Criteria) this;
        }

        public Criteria andRemarkNotIn(List<String> values) {
            addCriterion("C_REMARK not in", values, "remark");
            return (Criteria) this;
        }

        public Criteria andRemarkBetween(String value1, String value2) {
            addCriterion("C_REMARK between", value1, value2, "remark");
            return (Criteria) this;
        }

        public Criteria andRemarkNotBetween(String value1, String value2) {
            addCriterion("C_REMARK not between", value1, value2, "remark");
            return (Criteria) this;
        }

        public Criteria andSmsnumberIsNull() {
            addCriterion("C_SMSNUMBER is null");
            return (Criteria) this;
        }

        public Criteria andSmsnumberIsNotNull() {
            addCriterion("C_SMSNUMBER is not null");
            return (Criteria) this;
        }

        public Criteria andSmsnumberEqualTo(String value) {
            addCriterion("C_SMSNUMBER =", value, "smsnumber");
            return (Criteria) this;
        }

        public Criteria andSmsnumberNotEqualTo(String value) {
            addCriterion("C_SMSNUMBER <>", value, "smsnumber");
            return (Criteria) this;
        }

        public Criteria andSmsnumberGreaterThan(String value) {
            addCriterion("C_SMSNUMBER >", value, "smsnumber");
            return (Criteria) this;
        }

        public Criteria andSmsnumberGreaterThanOrEqualTo(String value) {
            addCriterion("C_SMSNUMBER >=", value, "smsnumber");
            return (Criteria) this;
        }

        public Criteria andSmsnumberLessThan(String value) {
            addCriterion("C_SMSNUMBER <", value, "smsnumber");
            return (Criteria) this;
        }

        public Criteria andSmsnumberLessThanOrEqualTo(String value) {
            addCriterion("C_SMSNUMBER <=", value, "smsnumber");
            return (Criteria) this;
        }

        public Criteria andSmsnumberLike(String value) {
            addCriterion("C_SMSNUMBER like", value, "smsnumber");
            return (Criteria) this;
        }

        public Criteria andSmsnumberNotLike(String value) {
            addCriterion("C_SMSNUMBER not like", value, "smsnumber");
            return (Criteria) this;
        }

        public Criteria andSmsnumberIn(List<String> values) {
            addCriterion("C_SMSNUMBER in", values, "smsnumber");
            return (Criteria) this;
        }

        public Criteria andSmsnumberNotIn(List<String> values) {
            addCriterion("C_SMSNUMBER not in", values, "smsnumber");
            return (Criteria) this;
        }

        public Criteria andSmsnumberBetween(String value1, String value2) {
            addCriterion("C_SMSNUMBER between", value1, value2, "smsnumber");
            return (Criteria) this;
        }

        public Criteria andSmsnumberNotBetween(String value1, String value2) {
            addCriterion("C_SMSNUMBER not between", value1, value2, "smsnumber");
            return (Criteria) this;
        }

        public Criteria andTypeIsNull() {
            addCriterion("C_TYPE is null");
            return (Criteria) this;
        }

        public Criteria andTypeIsNotNull() {
            addCriterion("C_TYPE is not null");
            return (Criteria) this;
        }

        public Criteria andTypeEqualTo(String value) {
            addCriterion("C_TYPE =", value, "type");
            return (Criteria) this;
        }

        public Criteria andTypeNotEqualTo(String value) {
            addCriterion("C_TYPE <>", value, "type");
            return (Criteria) this;
        }

        public Criteria andTypeGreaterThan(String value) {
            addCriterion("C_TYPE >", value, "type");
            return (Criteria) this;
        }

        public Criteria andTypeGreaterThanOrEqualTo(String value) {
            addCriterion("C_TYPE >=", value, "type");
            return (Criteria) this;
        }

        public Criteria andTypeLessThan(String value) {
            addCriterion("C_TYPE <", value, "type");
            return (Criteria) this;
        }

        public Criteria andTypeLessThanOrEqualTo(String value) {
            addCriterion("C_TYPE <=", value, "type");
            return (Criteria) this;
        }

        public Criteria andTypeLike(String value) {
            addCriterion("C_TYPE like", value, "type");
            return (Criteria) this;
        }

        public Criteria andTypeNotLike(String value) {
            addCriterion("C_TYPE not like", value, "type");
            return (Criteria) this;
        }

        public Criteria andTypeIn(List<String> values) {
            addCriterion("C_TYPE in", values, "type");
            return (Criteria) this;
        }

        public Criteria andTypeNotIn(List<String> values) {
            addCriterion("C_TYPE not in", values, "type");
            return (Criteria) this;
        }

        public Criteria andTypeBetween(String value1, String value2) {
            addCriterion("C_TYPE between", value1, value2, "type");
            return (Criteria) this;
        }

        public Criteria andTypeNotBetween(String value1, String value2) {
            addCriterion("C_TYPE not between", value1, value2, "type");
            return (Criteria) this;
        }

        public Criteria andCourierIsNull() {
            addCriterion("C_COURIER is null");
            return (Criteria) this;
        }

        public Criteria andCourierIsNotNull() {
            addCriterion("C_COURIER is not null");
            return (Criteria) this;
        }

        public Criteria andCourierEqualTo(Integer value) {
            addCriterion("C_COURIER =", value, "courier");
            return (Criteria) this;
        }

        public Criteria andCourierNotEqualTo(Integer value) {
            addCriterion("C_COURIER <>", value, "courier");
            return (Criteria) this;
        }

        public Criteria andCourierGreaterThan(Integer value) {
            addCriterion("C_COURIER >", value, "courier");
            return (Criteria) this;
        }

        public Criteria andCourierGreaterThanOrEqualTo(Integer value) {
            addCriterion("C_COURIER >=", value, "courier");
            return (Criteria) this;
        }

        public Criteria andCourierLessThan(Integer value) {
            addCriterion("C_COURIER <", value, "courier");
            return (Criteria) this;
        }

        public Criteria andCourierLessThanOrEqualTo(Integer value) {
            addCriterion("C_COURIER <=", value, "courier");
            return (Criteria) this;
        }

        public Criteria andCourierIn(List<Integer> values) {
            addCriterion("C_COURIER in", values, "courier");
            return (Criteria) this;
        }

        public Criteria andCourierNotIn(List<Integer> values) {
            addCriterion("C_COURIER not in", values, "courier");
            return (Criteria) this;
        }

        public Criteria andCourierBetween(Integer value1, Integer value2) {
            addCriterion("C_COURIER between", value1, value2, "courier");
            return (Criteria) this;
        }

        public Criteria andCourierNotBetween(Integer value1, Integer value2) {
            addCriterion("C_COURIER not between", value1, value2, "courier");
            return (Criteria) this;
        }

        public Criteria andOrderIdIsNull() {
            addCriterion("C_ORDER_ID is null");
            return (Criteria) this;
        }

        public Criteria andOrderIdIsNotNull() {
            addCriterion("C_ORDER_ID is not null");
            return (Criteria) this;
        }

        public Criteria andOrderIdEqualTo(Integer value) {
            addCriterion("C_ORDER_ID =", value, "orderId");
            return (Criteria) this;
        }

        public Criteria andOrderIdNotEqualTo(Integer value) {
            addCriterion("C_ORDER_ID <>", value, "orderId");
            return (Criteria) this;
        }

        public Criteria andOrderIdGreaterThan(Integer value) {
            addCriterion("C_ORDER_ID >", value, "orderId");
            return (Criteria) this;
        }

        public Criteria andOrderIdGreaterThanOrEqualTo(Integer value) {
            addCriterion("C_ORDER_ID >=", value, "orderId");
            return (Criteria) this;
        }

        public Criteria andOrderIdLessThan(Integer value) {
            addCriterion("C_ORDER_ID <", value, "orderId");
            return (Criteria) this;
        }

        public Criteria andOrderIdLessThanOrEqualTo(Integer value) {
            addCriterion("C_ORDER_ID <=", value, "orderId");
            return (Criteria) this;
        }

        public Criteria andOrderIdIn(List<Integer> values) {
            addCriterion("C_ORDER_ID in", values, "orderId");
            return (Criteria) this;
        }

        public Criteria andOrderIdNotIn(List<Integer> values) {
            addCriterion("C_ORDER_ID not in", values, "orderId");
            return (Criteria) this;
        }

        public Criteria andOrderIdBetween(Integer value1, Integer value2) {
            addCriterion("C_ORDER_ID between", value1, value2, "orderId");
            return (Criteria) this;
        }

        public Criteria andOrderIdNotBetween(Integer value1, Integer value2) {
            addCriterion("C_ORDER_ID not between", value1, value2, "orderId");
            return (Criteria) this;
        }
    }

    public static class Criteria extends GeneratedCriteria {

        protected Criteria() {
            super();
        }
    }

    public static class Criterion {
        private String condition;

        private Object value;

        private Object secondValue;

        private boolean noValue;

        private boolean singleValue;

        private boolean betweenValue;

        private boolean listValue;

        private String typeHandler;

        public String getCondition() {
            return condition;
        }

        public Object getValue() {
            return value;
        }

        public Object getSecondValue() {
            return secondValue;
        }

        public boolean isNoValue() {
            return noValue;
        }

        public boolean isSingleValue() {
            return singleValue;
        }

        public boolean isBetweenValue() {
            return betweenValue;
        }

        public boolean isListValue() {
            return listValue;
        }

        public String getTypeHandler() {
            return typeHandler;
        }

        protected Criterion(String condition) {
            super();
            this.condition = condition;
            this.typeHandler = null;
            this.noValue = true;
        }

        protected Criterion(String condition, Object value, String typeHandler) {
            super();
            this.condition = condition;
            this.value = value;
            this.typeHandler = typeHandler;
            if (value instanceof List<?>) {
                this.listValue = true;
            } else {
                this.singleValue = true;
            }
        }

        protected Criterion(String condition, Object value) {
            this(condition, value, null);
        }

        protected Criterion(String condition, Object value, Object secondValue, String typeHandler) {
            super();
            this.condition = condition;
            this.value = value;
            this.secondValue = secondValue;
            this.typeHandler = typeHandler;
            this.betweenValue = true;
        }

        protected Criterion(String condition, Object value, Object secondValue) {
            this(condition, value, secondValue, null);
        }
    }
}