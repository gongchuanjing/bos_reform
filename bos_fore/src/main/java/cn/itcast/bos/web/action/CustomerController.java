package cn.itcast.bos.web.action;

import cn.itcast.bos.constant.Constants;
import cn.itcast.crm.domain.Customer;
import org.apache.cxf.jaxrs.client.WebClient;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

import javax.servlet.http.HttpServletRequest;
import javax.ws.rs.core.MediaType;

/**
 * @author ChuanJing
 * @date 2018年6月5日 上午10:48:19
 * @version 1.0
 */
@Controller
public class CustomerController {

    @RequestMapping("/customer_login")
    public String login(Customer model, HttpServletRequest request) {
        Customer customer = WebClient.create(Constants.CRM_MANAGEMENT_URL
                + "/services/customerService/customer/login?telephone="
                + model.getTelephone() + "&password="
                + model.getPassword())
                .accept(MediaType.APPLICATION_JSON)
                .get(Customer.class);

        if (customer == null) {
            // 登录失败
            return "redirect:/login.html";
        } else {
            // 登录成功
            request.getSession().setAttribute("customer", customer);
            return "redirect:/index.html#/myhome";
        }
    }
}
