package cn.itcast.bos.web.action;

import cn.itcast.bos.constant.Constants;
import cn.itcast.bos.domain.base.Area;
import cn.itcast.bos.domain.take_delivery.Order;
import cn.itcast.crm.domain.Customer;
import org.apache.cxf.jaxrs.client.WebClient;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

import javax.servlet.http.HttpServletRequest;
import javax.ws.rs.core.MediaType;

/**
 * @author ChuanJing
 * @date 2018/6/5 10:59
 */
@Controller
public class OrderController {

    /**
     * @param order
     * @param sendAreaInfo  发件人省市区 信息
     * @param recAreaInfo   收件人 省市区信息
     * @param request
     * @return
     */
    @RequestMapping("/order_add")
    public String add(Order order, String sendAreaInfo, String recAreaInfo, HttpServletRequest request) {
        // 手动封装 Area关联
        Area sendArea = new Area();
        String[] sendAreaData = sendAreaInfo.split("/");
        sendArea.setProvince(sendAreaData[0]);
        sendArea.setCity(sendAreaData[1]);
        sendArea.setDistrict(sendAreaData[2]);

        Area recArea = new Area();
        String[] recAreaData = recAreaInfo.split("/");
        recArea.setProvince(recAreaData[0]);
        recArea.setCity(recAreaData[1]);
        recArea.setDistrict(recAreaData[2]);

        order.setSendArea(sendArea);
        order.setRecArea(recArea);

        // 关联当前登录客户
        Customer customer = (Customer) request.getSession().getAttribute("customer");
        order.setCustomer_id(customer.getId());

        // 调用WebService 将数据传递 bos_management系统
        WebClient.create(Constants.BOS_MANAGEMENT_URL + "/services/orderService/order")
                .type(MediaType.APPLICATION_JSON)
                .post(order);

        return "redirect:/index.html";
    }
}
