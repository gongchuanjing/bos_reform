package cn.itcast.bos.dao.base;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;

import cn.itcast.bos.domain.base.Area;

/**
 * @author ChuanJing
 * @date 2017年10月7日 下午5:19:03
 * @version 1.0
 */
public interface AreaRepository extends JpaRepository<Area, String>, JpaSpecificationExecutor<Area> {

	Area findByProvinceAndCityAndDistrict(String province, String city, String district);

}
