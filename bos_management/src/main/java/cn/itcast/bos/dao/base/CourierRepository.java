package cn.itcast.bos.dao.base;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;

import cn.itcast.bos.domain.base.Courier;

/**
 * @author ChuanJing
 * @date 2017年9月4日 下午4:16:45
 * @version 1.0
 */
public interface CourierRepository extends JpaRepository<Courier, Integer>, JpaSpecificationExecutor<Courier> {

	//作废
	@Query(value = "update Courier set deltag = '1' where id = ?")
	@Modifying
	public void updateDelTag(Integer id);

	//还原
	@Query(value = "update Courier set deltag = null where id = ?")
	@Modifying
	public void updateRestoreTag(Integer id);

}
