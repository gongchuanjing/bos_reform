package cn.itcast.bos.dao.base;

import cn.itcast.bos.domain.base.FixedArea;
import cn.itcast.bos.domain.base.SubArea;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Query;

import java.util.List;
import java.util.Map;

import java.util.List;

public interface SubAreaRepository  extends JpaRepository<SubArea, String>, JpaSpecificationExecutor<SubArea> {

    /**
     * 根据fixedAreaId查找
     * @param fixedArea
     * @return
     */
    public List<SubArea> findByFixedArea(FixedArea fixedArea);

    @Query(value = "select a.c_shortcode as shortcode,a.c_province as province,a.c_city as city,a.c_district as district" +
            " from T_AREA a  where a.c_shortcode = ?1", nativeQuery = true)
    List<Object[]> findAreaNameByShortCode(String shortCode);
}
