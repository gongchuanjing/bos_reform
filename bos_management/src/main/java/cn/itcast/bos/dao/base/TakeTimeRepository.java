package cn.itcast.bos.dao.base;

import org.springframework.data.jpa.repository.JpaRepository;

import cn.itcast.bos.domain.base.TakeTime;

/**
 * @author ChuanJing
 * @date 2017年10月16日 上午3:13:11
 * @version 1.0
 */
public interface TakeTimeRepository extends JpaRepository<TakeTime, Integer> {

}
