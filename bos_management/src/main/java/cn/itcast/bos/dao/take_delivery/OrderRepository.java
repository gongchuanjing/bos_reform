package cn.itcast.bos.dao.take_delivery;

import org.springframework.data.jpa.repository.JpaRepository;

import cn.itcast.bos.domain.take_delivery.Order;

/**
 * @author ChuanJing
 * @date 2018年1月26日 下午9:55:24
 * @version 1.0
 */
public interface OrderRepository extends JpaRepository<Order, Integer> {
	
	Order findByOrderNum(String orderNum);
}
