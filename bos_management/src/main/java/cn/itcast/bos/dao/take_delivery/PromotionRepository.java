package cn.itcast.bos.dao.take_delivery;

import java.util.Date;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;

import cn.itcast.bos.domain.take_delivery.Promotion;

/**
 * @author ChuanJing
 * @date 2018年1月20日 下午5:40:05
 * @version 1.0
 */
public interface PromotionRepository extends JpaRepository<Promotion, Integer> {

	@Query("update Promotion set status='2' where endDate<? and status='1'")
	@Modifying
	void updateStatus(Date date);

}
