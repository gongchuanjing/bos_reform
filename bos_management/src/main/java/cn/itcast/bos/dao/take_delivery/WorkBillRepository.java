package cn.itcast.bos.dao.take_delivery;

import org.springframework.data.jpa.repository.JpaRepository;

import cn.itcast.bos.domain.take_delivery.WorkBill;

/**
 * @author ChuanJing
 * @date 2018年1月26日 下午9:58:10
 * @version 1.0
 */
public interface WorkBillRepository extends JpaRepository<WorkBill, Integer> {

}
