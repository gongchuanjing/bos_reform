package cn.itcast.bos.mapper;

import cn.itcast.bos.domain.system.Menu;
import cn.itcast.bos.domain.system.User;

import java.util.List;
import java.util.Map;

public interface MenuMapper {

    List<Menu> menuList();

    List<Menu> findPageMenu(Menu menu);

    Menu selectMenuById(int id);

    List<Map<String,Object>> parentMenuList();

    void insert(Menu menu);

    void update(Menu menu);

    Menu findMenuByParentId(Integer id);

    void delete(Integer id);

    List<Menu> menuListByUsername(User user);
}
