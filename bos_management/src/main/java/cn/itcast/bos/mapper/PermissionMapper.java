package cn.itcast.bos.mapper;

import cn.itcast.bos.domain.system.Permission;
import cn.itcast.bos.domain.system.Role;

import java.util.List;
import java.util.Map;

public interface PermissionMapper {

    List<Permission> findByPage();

    Permission selectById(Integer id);

    void update(Permission permission);

    void insert(Permission permission);

    void delete(Integer id);

    List<Map<String,Object>> selectRoleByPermissionId(int id);

    void deleteRoleByPermissionId(int id);

    List<Permission> findByUser(Integer uesrId);
}
