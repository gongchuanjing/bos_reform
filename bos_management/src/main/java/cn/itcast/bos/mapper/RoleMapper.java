package cn.itcast.bos.mapper;

import cn.itcast.bos.domain.system.Role;
import org.apache.ibatis.annotations.Param;

import java.util.List;
import java.util.Map;

public interface RoleMapper {

    List<Role> findPageRole();

    void saveRole(Role role);

    void associatePermission(@Param("permissionId")Integer id, @Param("roleId")Integer integer);

    void associateMenu(@Param("menuId")Integer id, @Param("roleId")Integer integer);

    List<Map<String,Object>> showDataByRoleId(Integer id);

    void updateRole(Role role);

    List<Map<String,Object>> selectPermissionsByRoleId(Integer id);

    List<Map<String,Object>> selectMenusByRoleId(Integer id);

    void deletePermissionByRoleId(Integer id);

    void deleteMenuByRoleId(Integer id);

    void delete(Integer id);

    List<Role> findByUser(Integer uesrId);
}
