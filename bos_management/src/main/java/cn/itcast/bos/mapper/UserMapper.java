package cn.itcast.bos.mapper;

import cn.itcast.bos.domain.system.User;
import org.apache.ibatis.annotations.Param;

import java.util.List;
import java.util.Map;

public interface UserMapper {
    List<User> selectUserListByCondition();

    void save(User user);

    void saveRoleAndUser(@Param("roleId") Integer roleId, @Param("userId") Integer id);

    List<Map<String,Object>> selectUserById(Integer id);

    void update(User user);

    void delete(Integer id);

    User findByUsername(String username);
}
