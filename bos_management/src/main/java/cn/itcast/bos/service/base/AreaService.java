package cn.itcast.bos.service.base;

import java.util.List;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;

import cn.itcast.bos.domain.base.Area;

/**
 * @author ChuanJing
 * @date 2017年10月7日 下午4:45:37
 * @version 1.0
 */
public interface AreaService {

	// 批量保存区域实现
	public void saveBatch(List<Area> areas);

	// 条件分页查询
	public Page<Area> findPageData(Specification<Area> specification, Pageable pageable);

	void save(Area area);

    void delete(String[] ids);

    List<Area> findAll();
}
