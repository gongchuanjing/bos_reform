package cn.itcast.bos.service.base;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;

import cn.itcast.bos.domain.base.FixedArea;

/**
 * @author ChuanJing
 * @date 2017年10月10日 下午7:23:00
 * @version 1.0
 */
public interface FixedAreaService {

	void save(FixedArea fixedArea);

	Page<FixedArea> findPageData(Specification<FixedArea> specification, Pageable pageable);

	void associationCourierToFixedArea(FixedArea model, Integer courierId, Integer takeTimeId);

}
