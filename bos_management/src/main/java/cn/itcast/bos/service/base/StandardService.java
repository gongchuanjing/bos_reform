package cn.itcast.bos.service.base;

import cn.itcast.bos.domain.base.Standard;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import java.util.List;

/**
 * @author ChuanJing
 * @date 2017年9月1日 上午1:45:49
 * @version 1.0

收派标准管理 
 */
public interface StandardService {

	public void save(Standard standard);

	public Page<Standard> findPageData(Pageable pageable);

	public List<Standard> findAll();
}
