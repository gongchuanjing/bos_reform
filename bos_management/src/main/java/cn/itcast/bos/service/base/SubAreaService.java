package cn.itcast.bos.service.base;

import cn.itcast.bos.domain.base.FixedArea;
import cn.itcast.bos.domain.base.SubArea;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;

import java.util.List;

public interface SubAreaService {

    //新增分区
    void save(SubArea subArea);

    //修改分区
    void update(SubArea subArea);

    //删除分区
    void delete(String[] id);

    void findAll();

    Page<SubArea> findPageData(Specification<SubArea> specification, Pageable pageable);

    List<SubArea> findByFixedArea(FixedArea fixedArea);

    List<Object[]> findAreaNameByShortCode(String shortCode);


}
