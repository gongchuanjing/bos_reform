package cn.itcast.bos.service.base;

import java.util.List;

import cn.itcast.bos.domain.base.TakeTime;

/**
 * 收派时间 接口
 * 
 * @author ChuanJing
 * @date 2017年10月16日 上午3:10:18
 * @version 1.0
 */
public interface TakeTimeService {

	/**
	 * 查询所有收派时间
	 */
	List<TakeTime> findAll();

}
