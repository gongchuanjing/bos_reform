package cn.itcast.bos.service.base.impl;

import cn.itcast.bos.dao.base.CourierRepository;
import cn.itcast.bos.domain.base.Courier;
import cn.itcast.bos.service.base.CourierService;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;
import java.util.List;
import java.util.Set;

/**
 * @author ChuanJing
 * @date 2017年9月4日 下午4:11:53
 * @version 1.0
 */
@Service("courierServiceImpl")
public class CourierServiceImpl implements CourierService{

	// 注入DAO 对象
	@Autowired
	private CourierRepository courierRepository;

	@Override
	@RequiresPermissions("courier:add")
	public void save(Courier courier) {
		courierRepository.save(courier);
	}

//	@Override
//	public Page<Courier> findPageData(Pageable pageable) {
//		return courierRepository.findAll(pageable);
//	}
	
	@Override
	public Page<Courier> findPageData(Specification<Courier> spec, Pageable pageable) {
		return courierRepository.findAll(spec, pageable);
	}

	//作废
	@Transactional	// 视频中没有加，也行，但我的不行，因为要修改，所以要开启可写的事务
	@Override
	public void delBatch(String[] idArray) {
		// 调用DAO实现 update修改操作，将deltag 修改为1
		for (String idStr : idArray) {
			Integer id = Integer.parseInt(idStr);
			courierRepository.updateDelTag(id);
		}
	}

	//还原
	@Transactional	// 视频中没有加，也行，但我的不行，因为要修改，所以要开启可写的事务
	@Override
	public void restoreBatch(String[] idArray) {
		// 调用DAO实现 update修改操作，将deltag 修改为null
		for (String idStr : idArray) {
			Integer id = Integer.parseInt(idStr);
			courierRepository.updateRestoreTag(id);
		}
	}

	@Override
	public List<Courier> findNoAssociation() {
		// 封装Specification
		Specification<Courier> specification = new Specification<Courier>() {
			@Override
			public Predicate toPredicate(Root<Courier> root, CriteriaQuery<?> query, CriteriaBuilder cb) {
				// 查询条件，判定列表size为空
//				Predicate p = cb.isEmpty(root.get("fixedAreas").as(Collection.class));
				Predicate p = cb.isEmpty(root.get("fixedAreas").as(Set.class));
				return p;
			}
		};
		return courierRepository.findAll(specification);
	}
}
