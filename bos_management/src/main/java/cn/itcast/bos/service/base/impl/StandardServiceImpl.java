package cn.itcast.bos.service.base.impl;

import cn.itcast.bos.dao.base.StandardRepository;
import cn.itcast.bos.domain.base.Standard;
import cn.itcast.bos.service.base.StandardService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

/**
 * @author ChuanJing
 * @date 2017年9月1日 上午1:47:22
 * @version 1.0
 */

@Service("standardServiceImpl")
@Transactional
public class StandardServiceImpl implements StandardService {

	// 注入DAO
	@Autowired
	private StandardRepository standardRepository;
	
	@Override
	@CacheEvict(value = "standard", allEntries = true)//清除缓存区数据 --- 用于 增加、修改、删除 方法
	public void save(Standard standard) {
		standardRepository.save(standard);
	}

	// 分页查询 
	//针对数据在不同条件下进行不同缓存，设置@Cacheable 注解 key 属性
	@Override
	@Cacheable(value = "standard", key = "#pageable.pageNumber+'_'+#pageable.pageSize")
	public Page<Standard> findPageData(Pageable pageable) {
		return standardRepository.findAll(pageable);
	}

	@Override
	@Cacheable("standard")//应用缓存区，对方法返回结果进行缓存 ---- 用于查询方法
	public List<Standard> findAll() {
		return standardRepository.findAll();
	}
}
