package cn.itcast.bos.service.base.impl;

import cn.itcast.bos.dao.base.SubAreaRepository;
import cn.itcast.bos.domain.base.FixedArea;
import cn.itcast.bos.domain.base.SubArea;
import cn.itcast.bos.service.base.SubAreaService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;

import java.util.List;


@Service
public class SubAreaServiceImp implements SubAreaService {

    @Autowired
    private SubAreaRepository subAreaRepository;

    @Override
    public void save(SubArea subArea) {
        subAreaRepository.save(subArea);
    }

    @Override
    public void update(SubArea subArea) {
//        subAreaRepository.
    }

    @Override
    public void delete(String[] ids) {
        for (String id : ids) {
            subAreaRepository.delete(id);
        }
    }

    @Override
    public void findAll() {

    }

    @Override
    public Page<SubArea> findPageData(Specification<SubArea> specification, Pageable pageable) {
        return subAreaRepository.findAll(specification, pageable);
    }

    /**
     * 查询关联当前定区的分区列表
     */
    @Override
    public List<SubArea> findByFixedArea(FixedArea fixedArea) {
        return subAreaRepository.findByFixedArea(fixedArea);
    }

    @Override
    public List<Object[]> findAreaNameByShortCode(String shortCode) {
        return subAreaRepository.findAreaNameByShortCode(shortCode);
    }
}
