package cn.itcast.bos.service.system;

import java.util.List;
import java.util.Map;

import cn.itcast.bos.domain.system.Menu;
import cn.itcast.bos.domain.system.User;
import cn.itcast.bos.pagehelper.PageResult;

public interface MenuService {

	List<Menu> menuList(User user);

    List<Menu> menuList();

	/**
	 * 和pageHelper中的参数一样对应起来
	 *
	 * @param pageNum  页码
	 * @param pageSize 每页显示数量
	 */
	PageResult findPageMenu(int pageNum, int pageSize,Menu menu);

	/**
	 * 根据id查询数据,进行回显
	 * @param id
	 * @return
	 */
	Menu selectMenuById(int id);

	List<Map<String,Object>> parentMenuList();

    void insert(Menu menu);

	void update(Menu menu);

	void delete(String[] ids);

}
