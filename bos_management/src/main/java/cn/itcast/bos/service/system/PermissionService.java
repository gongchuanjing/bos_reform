package cn.itcast.bos.service.system;

import java.util.List;

import cn.itcast.bos.domain.system.Permission;
import cn.itcast.bos.domain.system.User;
import cn.itcast.bos.pagehelper.PageResult;

public interface PermissionService {
	/**
	 * =============================================================
	 * @param pageNum
	 * @param pageSize
	 * @return
	 */

	PageResult findByPage(int pageNum, int pageSize);

	Permission selectById(Integer id);

	void update(Permission permission);
	void insert(Permission permission);

	void delete(String[] ids);

    List<Permission> permission_list();

    List<Permission> findByUser(Integer userID);
}
