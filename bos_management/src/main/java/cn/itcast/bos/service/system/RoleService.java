package cn.itcast.bos.service.system;

import java.util.List;
import java.util.Map;

import cn.itcast.bos.domain.system.Role;
import cn.itcast.bos.domain.system.User;
import cn.itcast.bos.pagehelper.PageResult;

public interface RoleService {

	PageResult findPageRole(int pageNum, int pageSize);

	void save(Role role, String permissionIds, String menuIds);

	void update(Role role, String permissionIds, String menuIds);

	List<Map<String,Object>> selectRoleById(Integer id);

	void delete(String[] ids);

    List<Role> selectRoleAll();

	List<Role> findByUser(Integer uesrId);
}
