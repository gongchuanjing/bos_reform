package cn.itcast.bos.service.system;

import java.util.List;
import java.util.Map;

import cn.itcast.bos.domain.system.User;
import cn.itcast.bos.pagehelper.PageResult;

public interface UserService {
	/**
	 * ====================================
	 * @param page
	 * @param rows
	 * @return
	 */


    PageResult userListAndCondition(int page, int rows);

	void saveUser(User user, String[] roleIds);

	List<Map<String,Object>> selectUserById(Integer id);

	void updateUser(User user, String[] roleIds);

	User findByUsername(String username);
}