package cn.itcast.bos.service.system.impl;

import cn.itcast.bos.domain.system.Role;
import cn.itcast.bos.mapper.RoleMapper;
import cn.itcast.bos.pagehelper.PageResult;
import cn.itcast.bos.service.system.RoleService;
import com.github.pagehelper.Page;
import com.github.pagehelper.PageHelper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Map;

@Service("roleServiceImpl")
@Transactional
public class RoleServiceImpl implements RoleService {

	@Autowired
	private RoleMapper roleMapper;

	/**
	 * ===============================================================================
	 */
	@Override
	public PageResult findPageRole(int pageNum, int pageSize) {
		PageHelper.startPage(pageNum,pageSize);
		Page<Role> pages = (Page<Role>) roleMapper.findPageRole();
		return new PageResult(pages.getTotal(),pages.getResult());
	}

	/**
	 * 添加角色的方法
	 * @param role
	 * @param permissionIds
	 * @param menuIds
	 */
	@Override
	public void save(Role role, String permissionIds, String menuIds) {
		//保存角色:
		roleMapper.saveRole(role);
		System.out.println("插入角色之后返回的主键ID:" + role.getId());
		//关联权限:
		if(permissionIds != null && permissionIds != ""){
			String[] permissionId = permissionIds.split(",");
			for(String id : permissionId){
				roleMapper.associatePermission(Integer.parseInt(id),role != null ? role.getId() : null);
			}
		}
		//关联菜单:
		if(menuIds != null && menuIds != "") {
			String[] menuId = menuIds.split(",");
			for(String id : menuId){
				roleMapper.associateMenu(Integer.parseInt(id),role != null ? role.getId() : null);
			}

		}
	}


	/**
	 * 修改角色的方法:
	 * @param role
	 * @param permissionIds
	 * @param menuIds
	 */
	@Override
	public void update(Role role, String permissionIds, String menuIds) {
		//1.直接修改t_role数据:
		roleMapper.updateRole(role);
		//2.修改之后先根据角色id,删除权限数据,在重新添加
		List<Map<String,Object>> permissions = roleMapper.selectPermissionsByRoleId(role != null ? role.getId() : null);
		if(permissions != null && permissions.size() > 0 && permissions.get(0).get("C_ROLE_ID") != null){
			//删除已经关联的权限数据:
			roleMapper.deletePermissionByRoleId(role != null ? role.getId() : null);
			//删除之后进行重新添加权限数据:
		}
		//关联权限:
		if(permissionIds != null && permissionIds != ""){
			String[] permissionId = permissionIds.split(",");
			for(String id : permissionId){
				roleMapper.associatePermission(Integer.parseInt(id),role != null ? role.getId() : null);
			}
		}
		//3.修改之后先根据角色id,删除菜单数据,在重新添加
		List<Map<String,Object>> menus = roleMapper.selectMenusByRoleId(role != null ? role.getId() : null);
		if(menus != null && menus.size() > 0 && menus.get(0).get("C_ROLE_ID") != null){
			roleMapper.deleteMenuByRoleId(role != null ? role.getId() : null);
			//删除之后进行重新添加权限数据:
		}
		//关联菜单:
		if(menuIds != null && menuIds != "") {
			String[] menuId = menuIds.split(",");
			for(String id : menuId){
				roleMapper.associateMenu(Integer.parseInt(id),role != null ? role.getId() : null);
			}
		}
	}

	@Override
	public List<Map<String, Object>> selectRoleById(Integer id) {
		return roleMapper.showDataByRoleId(id);
	}

	@Override
	public void delete(String[] ids) {
		if(ids != null && ids.length > 0){
			for(String s : ids){
				Integer id = Integer.parseInt(s);
				//删除角色之前先删除关联的数据:
				List<Map<String,Object>> permissions = roleMapper.selectPermissionsByRoleId(id);
				if(permissions != null && permissions.size() > 0){
					//删除已经关联的权限数据:
					roleMapper.deletePermissionByRoleId(id);
				}

				List<Map<String,Object>> menus = roleMapper.selectMenusByRoleId(id);
				if(menus != null && menus.size() > 0){
					roleMapper.deleteMenuByRoleId(id);
				}

				//删除完之后再删除角色数据:
				roleMapper.delete(id);
			}
		}
	}

	@Override
	public List<Role> selectRoleAll() {
		return roleMapper.findPageRole();
	}

	@Override
	public List<Role> findByUser(Integer uesrId) {
		return roleMapper.findByUser(uesrId);
	}


}