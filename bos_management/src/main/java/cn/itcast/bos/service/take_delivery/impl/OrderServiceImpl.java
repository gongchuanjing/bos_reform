package cn.itcast.bos.service.take_delivery.impl;

import cn.itcast.bos.constant.Constants;
import cn.itcast.bos.dao.base.AreaRepository;
import cn.itcast.bos.dao.base.FixedAreaRepository;
import cn.itcast.bos.dao.take_delivery.OrderRepository;
import cn.itcast.bos.dao.take_delivery.WorkBillRepository;
import cn.itcast.bos.domain.base.Area;
import cn.itcast.bos.domain.base.Courier;
import cn.itcast.bos.domain.base.FixedArea;
import cn.itcast.bos.domain.base.SubArea;
import cn.itcast.bos.domain.take_delivery.Order;
import cn.itcast.bos.domain.take_delivery.WorkBill;
import cn.itcast.bos.service.take_delivery.OrderService;
import org.apache.commons.lang.RandomStringUtils;
import org.apache.cxf.jaxrs.client.WebClient;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.jms.core.JmsTemplate;
import org.springframework.jms.core.MessageCreator;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.jms.JMSException;
import javax.jms.MapMessage;
import javax.jms.Message;
import javax.jms.Session;
import javax.ws.rs.core.MediaType;
import java.util.Date;
import java.util.Iterator;
import java.util.UUID;

/**
 * @author ChuanJing
 * @date 2018年1月25日 下午6:15:18
 * @version 1.0
 */
@Service("orderServiceImpl")
@Transactional
public class OrderServiceImpl implements OrderService {

	@Autowired
	private OrderRepository orderRepository;
	
	@Autowired
	private FixedAreaRepository fixedAreaRepository;
	
	@Autowired
	private AreaRepository areaRepository;
	
	@Autowired
	private WorkBillRepository workBillRepository;

	@Autowired
	@Qualifier("jmsQueueTemplate")
	private JmsTemplate jmsTemplate;
	
	@Override
	public void saveOrder(Order order) {
		// =========================设置订单的基本属性start==============================
		order.setStatus("1"); 							 // 待取件
		order.setOrderTime(new Date());					 // 设置下单时间
		order.setOrderNum(UUID.randomUUID().toString()); // 设置订单号
		
		// 寄件人 省市区
		Area area = order.getSendArea();
		Area persistArea = areaRepository.findByProvinceAndCityAndDistrict(area.getProvince(), area.getCity(), area.getDistrict());
		
		// 收件人 省市区
		Area recArea = order.getRecArea();
		Area persistRecArea = areaRepository.findByProvinceAndCityAndDistrict(recArea.getProvince(), recArea.getCity(), recArea.getDistrict());
		order.setSendArea(persistArea);
		order.setRecArea(persistRecArea);
		// =========================设置订单的基本属性end==============================
		
		// =========================自动分单逻辑start==========================
		/*
		 * 自动分单方案一：基于CRM地址库完全匹配，获取定区，匹配快递员
		 * 
		 * 在基础数据模块，实现了定区关联客户，客户【地址】已经与定区【快递员】进行过关联，当
		 * 下单地址  与客户地址完全一致时，找到下单客户信息，找到匹配定区，找到快递员 ，自动分单成功。
		 */
		// 获取 定区Id
		String fixedAreaId = WebClient.create(Constants.CRM_MANAGEMENT_URL + "/services/customerService/customer/findFixedAreaIdByAddress?address=" + order.getSendAddress())
									  .accept(MediaType.APPLICATION_JSON)
									  .get(String.class);
		
		if (fixedAreaId != null) {
			//根据定区Id查找定区
			FixedArea fixedArea = fixedAreaRepository.findOne(fixedAreaId);
			
			// 根据定区获取这个定区关联的所有快递员，然后根据排班分配给具体的哪一个快递员，但是这里就先简单的分配给第一个快递员
			Iterator<Courier> iterator = fixedArea.getCouriers().iterator();
			if (iterator.hasNext()) {
				Courier courier = iterator.next();
				if (courier != null) {
					// 自动分单方案一成功
					System.out.println("自动分单方案一，分单成功....");
					
					saveOrder(order, courier);

					// 生成工单 发送短信
					generateWorkBill(order);
					return;
				}
			} else {
				System.out.println("自动分单方案一，该定区没有关联快递员！！！");
			}
		}
		
		/*
		 * 自动分单方案二：通过省市区 ，查询分区关键字，匹配地址，基于分区实现自动分单
		 * 
		 * 如果下单地址没有与 CRM 客户地址完全一致，从下单信息中 省、市、区 匹配区域信息，分区属于区域 ，
		 * 获取区域中所有分区信息 ，获取分区关键字、辅助关键字，下单地址匹配分区关键字/辅助关键字，找到
		 * 分区，通过分区找到定区，找到匹配快递员， 自动分单成功。
		 */
		// 自动分单方案二，【匹配分区关键字】
		for (SubArea subArea : persistArea.getSubareas()) {
			// 当前客户的下单地址 是否包含【分区关键字】
			if (order.getSendAddress().contains(subArea.getKeyWords())) {
				// 包含分区 关键字，根据分区找到定区，根据定区找到所有快递员
				Iterator<Courier> iterator = subArea.getFixedArea().getCouriers().iterator();
				
				// 然后根据排班分配给具体的哪一个快递员，但是这里就先简单的分配给第一个快递员
				if (iterator.hasNext()) {
					Courier courier = iterator.next();
					if (courier != null) {
						// 自动分单成功
						System.out.println("自动分单方案二，匹配分区关键字成功，分单成功....");
						
						saveOrder(order, courier);

						// 生成工单 发送短信
						generateWorkBill(order);
						return;
					}
				} else {
					System.out.println("自动分单方案二，匹配分区关键字成功，但该定区没有关联快递员！！！");
				}
			}
		}
		
		// 自动分单方案二，【匹配分区辅助关键字】
		for (SubArea subArea : persistArea.getSubareas()) {
			// 当前客户的下单地址 是否包含【分区辅助关键字】
			if (order.getSendAddress().contains(subArea.getAssistKeyWords())) {
				// 包含分区辅助关键字，根据分区找到定区，根据定区找到所有快递员
				Iterator<Courier> iterator = subArea.getFixedArea().getCouriers().iterator();
				
				// 然后根据排班分配给具体的哪一个快递员，但是这里就先简单的分配给第一个快递员
				if (iterator.hasNext()) {
					Courier courier = iterator.next();
					if (courier != null) {
						// 自动分单成功
						System.out.println("自动分单方案二，匹配分区【辅助】关键字成功，分单成功....");
						
						saveOrder(order, courier);

						// 生成工单 发送短信
						generateWorkBill(order);
						return;
					}
				} else {
					System.out.println("自动分单方案二，匹配分区【辅助】关键字成功，但该定区没有关联快递员！！！");
				}
			}
		}
		// =========================自动分单逻辑 end==========================
		
		// 上面  两种自动分单  业务逻辑都没有实现， 则进入人工调度，即人工分单
		order.setOrderType("2");
		orderRepository.save(order);
	}
	
	/**
	 * 生成工单，发送短信
	 * 当订单匹配快递员成功后，系统都将自动生成工单信息【快递员取件任务】，将以短信方式，通知快递员，进行取件
	 */
	private void generateWorkBill(final Order order) {
		// ====================================生成工单，并设置工单基本属性====================
		WorkBill workBill = new WorkBill();
		workBill.setType("新");
		workBill.setPickstate("新单");
		workBill.setBuildtime(new Date());
		workBill.setRemark(order.getRemark());
		final String smsNumber = RandomStringUtils.randomNumeric(4);
		workBill.setSmsNumber(smsNumber); // 短信序号
		workBill.setOrder(order);
		workBill.setCourier(order.getCourier());
		workBillRepository.save(workBill);

		// 发送短信：调用MQ服务，给快递员发送一条消息
		jmsTemplate.send("bos_sms", new MessageCreator() {
			@Override
			public Message createMessage(Session session) throws JMSException {
				MapMessage mapMessage = session.createMapMessage();
				mapMessage.setString("telephone", order.getCourier().getTelephone());
				mapMessage.setString("msg", "快递小哥，来活了！短信序号：" + smsNumber +
											"，取件地址：" + order.getSendAddress() +
											"，联系人:" + order.getSendName() +
											"，手机：" + order.getSendMobile() +
											"，给你捎的话：" + order.getSendMobileMsg());
				return mapMessage;
			}
		});

		// 修改工单状态
		workBill.setPickstate("已通知");
	}
	
	/**
	 * 自动分单成功，保存订单
	 */
	private void saveOrder(Order order, Courier courier) {
		// 将快递员关联订单上
		order.setCourier(courier);
		// 设置自动分单
		order.setOrderType("1");
		// 保存订单
		orderRepository.save(order);
	}

	@Override
	public Order findByOrderNum(String orderNum) {
		return orderRepository.findByOrderNum(orderNum);
	}

}
