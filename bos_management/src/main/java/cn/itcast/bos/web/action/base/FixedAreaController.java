package cn.itcast.bos.web.action.base;

import cn.itcast.bos.domain.base.FixedArea;
import cn.itcast.bos.pagehelper.PageResult;
import cn.itcast.bos.service.base.FixedAreaService;
import cn.itcast.bos.service.base.SubAreaService;
import cn.itcast.crm.domain.Customer;
import org.apache.commons.lang.StringUtils;
import org.apache.cxf.jaxrs.client.WebClient;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;
import javax.ws.rs.core.MediaType;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

/**
 * @author ChuanJing
 * @date 2018/6/5 20:54
 */
@Controller
public class FixedAreaController {

    // 注入业务层对象
    @Autowired
    private FixedAreaService fixedAreaService;

    /**
     * 保存定区
     * @param fixedArea
     * @return
     */
    @RequestMapping("/fixedArea_save")
    public String save(FixedArea fixedArea) {
        // 调用业务层，保存定区
        fixedAreaService.save(fixedArea);
        return "redirect:/pages/base/fixed_area.html";
    }

    /**
     * 分页查询
     * @param page
     * @param rows
     * @param fixedArea
     * @return
     */
    @RequestMapping("/fixedArea_pageQuery")
    @ResponseBody
    public PageResult pageQuery(int page, int rows, FixedArea fixedArea) {
        // 构造Pageable
        Pageable pageable = new PageRequest(page - 1, rows);

        // 构造条件查询对象
        Specification<FixedArea> specification = new Specification<FixedArea>() {
            @Override
            public Predicate toPredicate(Root<FixedArea> root, CriteriaQuery<?> query, CriteriaBuilder cb) {
                List<Predicate> list = new ArrayList<>();

                // 构造查询条件isNotBlank
                if (StringUtils.isNotBlank(fixedArea.getId())) {
                    // 根据 定区编号查询 等值
                    Predicate p1 = cb.equal(root.get("id").as(String.class), fixedArea.getId());
                    list.add(p1);
                }

                if (StringUtils.isNotBlank(fixedArea.getCompany())) {
                    // 根据公司查询 模糊
                    Predicate p2 = cb.like(root.get("company").as(String.class), "%" + fixedArea.getCompany() + "%");
                    list.add(p2);
                }

                return cb.and(list.toArray(new Predicate[0]));
            }
        };

        // 调用业务层，查询数据
        Page<FixedArea> pageData = fixedAreaService.findPageData(specification, pageable);

        PageResult pageResult = new PageResult(pageData.getTotalElements(), pageData.getContent());

        return pageResult;
    }

    /**
     * 查询所有未关联定区客户列表
     */
    @RequestMapping("/fixedArea_findNoAssociationCustomers")
    @ResponseBody
    public Collection findNoAssociationCustomers() {
        // 使用webClient调用 webService接口
        Collection<? extends Customer> collection
                = WebClient.create("http://localhost:9002/crm_management/services/customerService/noassociationcustomers")
                .accept(MediaType.APPLICATION_JSON)
                .getCollection(Customer.class);

        return collection;
    }

    /**
     * 查询关联当前定区的客户列表
     */
    @RequestMapping("/fixedArea_findHasAssociationFixedAreaCustomers")
    @ResponseBody
    public Collection findHasAssociationFixedAreaCustomers(FixedArea fixedArea) {
        // 使用webClient调用 webService接口
        Collection<? extends Customer> collection
                = WebClient.create("http://localhost:9002/crm_management/services/customerService/associationfixedareacustomers/" + fixedArea.getId())
                .accept(MediaType.APPLICATION_JSON)
                .type(MediaType.APPLICATION_JSON)
                .getCollection(Customer.class);

        return collection;
    }

    /**
     * 关联客户到定区
     */
    @RequestMapping("/fixedArea_associationCustomersToFixedArea")
    public String associationCustomersToFixedArea(String[] customerIds, FixedArea fixedArea) {
        String customerIdStr = StringUtils.join(customerIds, ",");

        WebClient.create("http://localhost:9002/crm_management/services/customerService/associationcustomerstofixedarea?customerIdStr="
                + customerIdStr + "&fixedAreaId=" + fixedArea.getId())
                .put(null);

        return "redirect:/pages/base/fixed_area.html";
    }

    /**
     * 关联快递员 到定区
     */
    @RequestMapping("/fixedArea_associationCourierToFixedArea")
    public String associationCourierToFixedArea(FixedArea fixedArea, Integer courierId, Integer takeTimeId) {
        // 调用业务层， 定区关联快递员
        fixedAreaService.associationCourierToFixedArea(fixedArea, courierId, takeTimeId);

        return "redirect:/pages/base/fixed_area.html";
    }

    // 注入业务层对象
    @Autowired
    private SubAreaService subAreaService;

    /**
     * 查询关联当前定区的分区列表
     */
    @RequestMapping("/fixedArea_findHasAssociationFixedAreaSubArea")
    @ResponseBody
    public Collection findHasAssociationFixedAreaSubArea(FixedArea fixedArea) {
        return subAreaService.findByFixedArea(fixedArea);
    }
}
