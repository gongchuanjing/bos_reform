package cn.itcast.bos.web.action.base;

import cn.itcast.bos.domain.base.Standard;
import cn.itcast.bos.service.base.StandardService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @author ChuanJing
 * @date 2017年9月1日 上午1:04:32
 * @version 1.0
 */
@Controller
public class StandardController {

	// 注入Service对象
	@Autowired
	private StandardService standardService;
	
	// 添加操作
	@RequestMapping("standard_save")
	public String save(Standard standard1) {
		System.out.println("添加收派标准....");
		standardService.save(standard1);
		return "redirect:/pages/base/standard.html";
	}
	
	// 分页列表查询
	@RequestMapping("standard_pageQuery")
	@ResponseBody
	public Map<String, Object>  pageQuery(Integer page,Integer rows) {
		System.out.println("page:"+page+"rows:"+rows);
		// 调用业务层 ，查询数据结果
		Pageable pageable = new PageRequest(page-1, rows);//page从0开始，而页面传过来的是从1开始
		Page<Standard> pageData = standardService.findPageData(pageable);
		
		// 返回客户端数据 需要 total 和 rows
		Map<String, Object> result = new HashMap<>();
		result.put("total", pageData.getNumberOfElements());
		result.put("rows", pageData.getContent());
		
		return result;
	}
	
	// 查询所有收派标准方法
    @RequestMapping("standard_findAll")
    @ResponseBody
	public List<Standard>  findAll() {
		List<Standard> list = standardService.findAll();
		return list;
	}
}
