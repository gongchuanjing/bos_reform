package cn.itcast.bos.web.action.base;

import cn.itcast.bos.domain.base.Area;
import cn.itcast.bos.domain.base.SubArea;
import cn.itcast.bos.service.base.SubAreaService;
import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.persistence.criteria.*;
import java.util.*;

/**
 * 分区相关增 删 改 查
 */
@Controller
public class SubAreaController {

    @Autowired
    private SubAreaService subAreaService;

    @RequestMapping("subarea_pageQuery")
    @ResponseBody
    public Map<String, Object> pageQuery(int page, int rows, SubArea model) {
        // 构造分页查询对象
        Pageable pageable = new PageRequest(page - 1, rows);

        // 构造条件查询对象
        Specification<SubArea> specification = new Specification<SubArea>() {
            @Override
            public Predicate toPredicate(Root<SubArea> root, CriteriaQuery<?> query, CriteriaBuilder cb) {
                // 创建保存条件集合对象
                List<Predicate> list = new ArrayList<Predicate>();

                if(model.getArea()!=null){
                    // 添加条件
                    if (StringUtils.isNotBlank(model.getArea().getProvince())) {
                        // 根据省份查询  模糊
                        Path<Object> objectPath = root.join("area", JoinType.INNER).get("province");
                        Predicate p1 = cb.like(objectPath.as(String.class), "%" + model.getArea().getProvince() + "%");
                        list.add(p1);
                    }

                    if (StringUtils.isNotBlank(model.getArea().getCity())) {
                        // 根据城市查询  模糊
                        Path<Object> objectPath = root.join("area", JoinType.INNER).get("city");
                        Predicate p2 = cb.like(objectPath.as(String.class), "%" + model.getArea().getCity() + "%");
                        list.add(p2);
                    }

                    if (StringUtils.isNotBlank(model.getArea().getDistrict())) {
                        // 根据区域查询  模糊
                        Path<Object> objectPath = root.join("area", JoinType.INNER).get("district");
                        Predicate p3 = cb.like(objectPath.as(String.class), "%" + model.getArea().getDistrict() + "%");
                        list.add(p3);
                    }
                }
                return cb.and(list.toArray(new Predicate[0]));
            }
        };

        // 调用业务层完成查询
        Page<SubArea> pageData = subAreaService.findPageData(specification, pageable);

        // 压入值栈
        Map<String, Object> result = new HashMap<String, Object>();
        result.put("total", pageData.getTotalElements());

        List<SubArea> content = pageData.getContent();
        for (SubArea area : content){
            Area a = area.getArea();
            String areaName_ = getAreaName_(a.getProvince(), a.getCity(), a.getDistrict());
            area.setAreaName(areaName_);
        }
        result.put("rows", pageData.getContent());

        return result;
    }

    /**
     * 生成区域名称处理的方法:
     * @return 选择简码是显示的和简码对应的区域名称
     */
    public String getAreaName_(String province,String city,String district){
        if(StringUtils.isNotBlank(province)){
            province = province.substring(0,province.length() - 1);
        }
        if(StringUtils.isNotBlank(city)){
            city = city.substring(0,city.length() - 1);
        }
        return province + city + district;
    }

    @RequestMapping("subarea_save")
    public String save(SubArea subArea){
        subAreaService.save(subArea);
        return "redirect:./pages/base/sub_area.html";
    }

//    @RequestMapping("subarea_update")
//    public String update(SubArea subArea){
//        subAreaService.update(subArea);
//        return "redirect:./pages/base/sub_area.html";
//    }

    @RequestMapping("subarea_delete")
    public String update(String[] ids){
        subAreaService.delete(ids);
        return "redirect:./pages/base/sub_area.html";
    }

    /**
     * 当下拉框被改变时,动态查询区域名称显示简码下拉框下面
     * @param shortCode 简码参数
     * @return {"areaName":"北京北京东城区"}
     */
    @RequestMapping("findAreaNameByShortCode")
    @ResponseBody
    public  Map<String,Object> findAreaNameByShortCode(String shortCode){
        List<Object[]> subArea = subAreaService.findAreaNameByShortCode(shortCode);
        System.out.println(subArea.toString());
        String areaName_ = null;
        for (Object[] objects : subArea) {
            areaName_ = getAreaName_(objects[1].toString(),objects[2].toString(),objects[3].toString());
            System.out.println(areaName_);
        }
        Map<String,Object> map = new HashMap<>();
        map.put("areaName",areaName_);
        return map;
    }

}
