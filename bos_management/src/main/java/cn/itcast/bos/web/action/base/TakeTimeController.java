package cn.itcast.bos.web.action.base;

import cn.itcast.bos.domain.base.TakeTime;
import cn.itcast.bos.service.base.TakeTimeService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.List;

/**
 * @author ChuanJing
 * @date 2018/6/5 21:44
 */
@Controller
public class TakeTimeController {

    @Autowired
    private TakeTimeService takeTimeService;

    @RequestMapping("/taketime_findAll")
    @ResponseBody
    public List findAll() {
        // 调用业务层，查询所有收派时间
        List<TakeTime> takeTimes = takeTimeService.findAll();

        return takeTimes;
    }
}
