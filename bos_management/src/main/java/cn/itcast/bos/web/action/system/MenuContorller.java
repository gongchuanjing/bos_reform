package cn.itcast.bos.web.action.system;

import cn.itcast.bos.domain.system.Menu;
import cn.itcast.bos.domain.system.User;
import cn.itcast.bos.pagehelper.PageResult;
import cn.itcast.bos.result.Result;
import cn.itcast.bos.service.system.MenuService;
import net.sf.ehcache.transaction.xa.EhcacheXAException;
import org.apache.shiro.SecurityUtils;
import org.apache.shiro.subject.Subject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.ModelAndView;

import java.util.Arrays;
import java.util.List;
import java.util.Map;

/**
 * 菜单的controller
 */
@RestController
@RequestMapping("/menu")
public class MenuContorller {

    @Autowired
    private MenuService menuService;

    /**
     * 查询基础数据下的动态菜单的方法:
     * @return
     */
    @RequestMapping("/menuList.action")
    public List<Menu> menuList(){
        Subject subject = SecurityUtils.getSubject();
        User user = (User) subject.getPrincipal();

        return menuService.menuList(user);
    }

    /**
     * 查询基础数据下的动态菜单的方法:
     * @return
     */
    @RequestMapping("/menuList2.action")
    public List<Menu> menuList2(){
        return menuService.menuList();
    }

    /**
     * 使用的是EasyUI的分页插件和mybatis的pageHelper分页插件
     * @param page 当前页(EasyUI的要自动减一)
     * @param rows 每页显示的条数
     * @param menu 为了条件查询而写的参数,现在还没有用到
     * @return
     */
    @RequestMapping("/findPageMenu.action")
    @ResponseBody
    public PageResult findPageMenu(int page, int rows, Menu menu){
        return menuService.findPageMenu(page,rows,menu);
    }

    /**
     * 根据id查询菜单,进行数据回显
     * @param id
     * @return
     */
    @RequestMapping("/selectMenuById.action")
    public Menu selectMenuById(int id){
        return menuService.selectMenuById(id);
    }

    /**
     * EasyUI的下拉框查询数据,使用map集合接受,其中的key为大写,前台需要对应
     */
    @RequestMapping("/parentMenuList.action")
    public List<Map<String,Object>> parentMenuList(){
        return menuService.parentMenuList();
    }

    /**
     * 保存新添加菜单和修改菜单
     * @param menu
     */
    @RequestMapping(value = "/saveAndUpdate.action",method = RequestMethod.POST)
    public Result saveAndUpdate(Menu menu){
        if(menu != null && menu.getId() != null){
            Result result = new Result(true,"修改成功!");
            try{
                //修改
                System.out.println("--------------------修改菜单-----------------------");
                System.out.println(menu);
                menuService.update(menu);
            }catch (Exception e){
                result = new Result(false,"修改失败!");
                e.printStackTrace();
            }
            return result;
        }else{
            Result result = new Result(true,"保存成功!");
            try {
                //保存
                System.out.println("--------------------新增菜单-----------------------");
                System.out.println(menu);
                menuService.insert(menu);
            }catch (Exception e){
                result = new Result(false,"保存失败!");
                e.printStackTrace();
            }
            return result;
        }
    }

    @RequestMapping("/delete.action")
    public Result delete(String[] ids){
        Result result = new Result(true,"删除成功!");
        try {
            menuService.delete(ids);
        }catch (Exception e){
            e.printStackTrace();
            result = new Result(false,"删除失败!");
        }
        return result;
    }
}
