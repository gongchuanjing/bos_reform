package cn.itcast.bos.web.action.system;

import cn.itcast.bos.domain.system.Permission;
import cn.itcast.bos.pagehelper.PageResult;
import cn.itcast.bos.result.Result;
import cn.itcast.bos.service.system.PermissionService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

/**
 * 权限的Controller
 */
@RestController
@RequestMapping("/permission")
public class PermissionController {

    @Autowired
    private PermissionService permissionService;

    /**
     * 查询所有的权限数据,为角色关联进行使用
     * @return
     */
    @RequestMapping("/permission_list.action")
    public List<Permission> permission_list(){
        return permissionService.permission_list();
    }

    @RequestMapping("/permission_findByPage")
    public PageResult findByPage(int page,int rows){
        return permissionService.findByPage(page,rows);
    }

    @RequestMapping("/permission_selectById")
    public Permission selectById(Integer id){
        return permissionService.selectById(id);
    }

    @RequestMapping("/permission_saveAndUpdate")
    public Result saveAndUpdate(Permission permission){
        if(permission != null && permission.getId() != null){
            Result result = new Result(true,"修改成功!");
            try{
                //修改
                permissionService.update(permission);
            }catch (Exception e){
                result = new Result(false,"修改失败!");
                e.printStackTrace();
            }
            return result;
        }else{
            Result result = new Result(true,"保存成功!");
            try {
                //保存
                permissionService.insert(permission);
            }catch (Exception e){
                result = new Result(false,"保存失败!");
                e.printStackTrace();
            }
            return result;
        }
    }

    @RequestMapping("/permission_delete")
    public Result delete(String[] ids){
        Result result = new Result(true,"删除成功!");
        try{
            permissionService.delete(ids);
        }catch (Exception e){
            result = new Result(false,"删除失败!");
            e.printStackTrace();
        }
        return result;
    }
}
