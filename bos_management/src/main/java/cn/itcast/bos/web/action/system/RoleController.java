package cn.itcast.bos.web.action.system;

import cn.itcast.bos.domain.system.Role;
import cn.itcast.bos.pagehelper.PageResult;
import cn.itcast.bos.result.Result;
import cn.itcast.bos.service.system.RoleService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.Arrays;
import java.util.List;
import java.util.Map;

@RestController
@RequestMapping("/role")
public class RoleController {

    @Autowired
    private RoleService roleService;

    @RequestMapping("/role_list.action")
    public PageResult role_list(int page,int rows){
        return roleService.findPageRole(page,rows);
    }

    @RequestMapping("/selectRoleAll.action")
    public List<Role> selectRoleAll(){
        return roleService.selectRoleAll();
    }

    @RequestMapping("/selectRoleById.action")
    public List<Map<String,Object>> selectRoleById(Integer id){
       List<Map<String,Object>> listMap =  roleService.selectRoleById(id);
       System.out.println("--------------------" + Arrays.asList(listMap));
       return listMap;
    }

    /**
     * 修改和添加角色
     * @param role
     * @param permissionIds
     * @param menuIds
     * @return
     */
    @RequestMapping("/saveAndUpdate.action")
    public Result saveAndUpdate(Role role, @RequestParam(value = "permissionIds",required = false) String permissionIds, @RequestParam("menuIds")String menuIds) {
        if (role != null && role.getId() != null) {
            Result result = new Result(true, "修改成功!");
            try {
                roleService.update(role,permissionIds,menuIds);
            } catch (Exception e) {
                result = new Result(false, "修改失败!");
                e.printStackTrace();
            }
            return result;
        } else {
            Result result = new Result(true, "保存成功!");
            try {
                roleService.save(role,permissionIds,menuIds);
            } catch (Exception e) {
                result = new Result(false, "保存失败!");
                e.printStackTrace();
            }
            return result;
        }
    }

    @RequestMapping("/delete.action")
    public Result delete(String[] ids){
        Result result = new Result(true,"删除成功!");
        try{
            roleService.delete(ids);
        }catch (Exception e){
            result = new Result(false,"删除失败!");
            e.printStackTrace();
        }
        return result;
    }
}
