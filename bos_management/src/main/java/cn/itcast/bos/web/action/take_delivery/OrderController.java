package cn.itcast.bos.web.action.take_delivery;

import cn.itcast.bos.domain.take_delivery.Order;
import cn.itcast.bos.service.take_delivery.OrderService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.HashMap;
import java.util.Map;

/**
 * @author ChuanJing
 * @date 2018/6/5 20:42
 */
@Controller
public class OrderController {

    @Autowired
    private OrderService orderService;

    @RequestMapping("/order_findByOrderNum")
    @ResponseBody
    public Map findByOrderNum(Order model) {
        // 调用业务层，查询Order信息
        Order order = orderService.findByOrderNum(model.getOrderNum());
        Map<String, Object> result = new HashMap<String, Object>();

        if (order == null) {
            // 订单号 不存在
            result.put("success", false);
        } else {
            // 订单号 存在
            result.put("success", true);
            result.put("orderData", order);
        }

        return result;
    }
}
