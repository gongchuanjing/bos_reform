package cn.itcast.bos.web.action.take_delivery;

import cn.itcast.bos.domain.take_delivery.WayBill;
import cn.itcast.bos.pagehelper.PageResult;
import cn.itcast.bos.service.take_delivery.WayBillService;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.HashMap;
import java.util.Map;

/**
 * 运单管理
 *
 * @author ChuanJing
 * @date 2018/6/4 16:59
 */
@Controller
public class WayBillController {
    private static final Logger LOGGER = Logger.getLogger(WayBillController.class);

    @Autowired
    private WayBillService wayBillService;

    @RequestMapping("/waybill_save")
    @ResponseBody
    public Map save(WayBill wayBill) {
        Map<String, Object> result = new HashMap<String, Object>();
        try {
            /*
             * 去除 没有id的order对象，为什么？
             * 假如一个运单是客户直接拿着东西来到网点，之前就没有生成订单，直接在运单快速录入界面生
             * 成运单，那么这时输入运单号，快速录入的运单信息会回显，但是订单号和订单相关的数据是没
             * 有的，这时录入订单相关的一些信息，但还是没有订单号。那么这个运单对象传到此处Action时，
             * model中就会关联一个没有id的对象，即order，那么最终hibernate去保存model时会报错：
             * 		Caused by: org.hibernate.TransientPropertyValueException: object references
             * 				   an unsaved transient instance - save the transient instance before
             * 				   flushing : cn.itcast.bos.domain.take_delivery.WayBill.order -> cn.
             * 							  itcast.bos.domain.take_delivery.Order
             * 说白了就是 WayBill 对象关联了一个没有 id 的 Order 对象。
             * 解决方法：当这种情况发生时，直接把这个 Order 对象去掉，这个if判断就是解决的代码
             */
            if (wayBill.getOrder() != null && (wayBill.getOrder().getId() == null || wayBill.getOrder().getId() == 0)) {
                wayBill.setOrder(null);
                System.out.println("没有订单数据，直接把这个 Order 对象去掉了！！！");
            }

            wayBillService.save(wayBill);

            // 保存成功
            result.put("success", true);
            result.put("msg", "保存运单成功！");
            LOGGER.info("保存运单成功，运单号：" + wayBill.getWayBillNum());
        } catch (Exception e) {
            e.printStackTrace();

            // 保存失败
            result.put("success", false);
            result.put("msg", "保存运单失败！");
            LOGGER.error("保存运单失败，运单号：" + wayBill.getWayBillNum(), e);
        }
        return result;
    }

    @RequestMapping("/waybill_pageQuery")
    @ResponseBody
    public PageResult pageQuery(int page, int rows, WayBill wayBill) {
        // 无条件查询
        Pageable pageable = new PageRequest(page - 1, rows, new Sort( new Sort.Order(Sort.Direction.DESC, "id") ));

        // 调用业务层进行查询
//		Page<WayBill> pageData = wayBillService.findPageData(pageable);
        Page<WayBill> pageData = wayBillService.findPageData(wayBill, pageable);

        PageResult pageResult = new PageResult(pageData.getTotalElements(), pageData.getContent());

        return pageResult;
    }

    @RequestMapping("/waybill_findByWayBillNum")
    @ResponseBody
    public Map findByWayBillNum(WayBill model) {
        // 调用业务层 查询
        WayBill wayBill = wayBillService.findByWayBillNum(model.getWayBillNum());
        Map<String, Object> result = new HashMap<String, Object>();

        if (wayBill == null) {
            // 运单不存在
            result.put("success", false);
        } else {
            // 运单存在
            result.put("success", true);
            result.put("wayBillData", wayBill);
        }

        return result;
    }
}
