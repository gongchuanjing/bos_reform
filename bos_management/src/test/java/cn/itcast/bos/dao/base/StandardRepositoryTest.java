package cn.itcast.bos.dao.base;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.annotation.Rollback;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.transaction.annotation.Transactional;

/**
 * @author ChuanJing
 * @date 2017年9月1日 上午11:25:46
 * @version 1.0
 */

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations="classpath:applicationContext.xml")
public class StandardRepositoryTest {

	@Autowired
	private StandardRepository standardRepository;
	
	@Test
	public void testQuery() {
		System.out.println("查询结果：" + standardRepository.findByName("20-30公斤"));
	}
	
	@Test
	public void testQuery2() {
		System.out.println("查询结果：" + standardRepository.queryName2("20-30公斤"));
	}
	
	@Test
	public void testQuery3() {
		System.out.println("查询结果：" + standardRepository.queryName3("20-30公斤"));
	}
	
	@Test
	@Transactional
	@Rollback(false)
	public void testUpdate() {
		standardRepository.updateMinLength(2, 29);
	}
}
