package cn.itcast.bos.service.take_delivery;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

/**
 * @author ChuanJing
 * @date 2018年2月24日 上午12:03:54
 * @version 1.0
 */

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = "classpath:applicationContext.xml")
public class WayBillServiceTest {

	@Autowired
	private WayBillService wayBillService;
	
	@Test
	public void testSyncIndex() {
		wayBillService.syncIndex();
	}

}
