package cn.itcast.bos.mq;

import org.springframework.stereotype.Service;

import javax.jms.JMSException;
import javax.jms.MapMessage;
import javax.jms.Message;
import javax.jms.MessageListener;

/**
 * @author ChuanJing
 * @date 2018年1月19日 上午9:31:11
 * @version 1.0
 */

@Service("smsConsumer")
public class SmsConsumer implements MessageListener{

	@Override
	public void onMessage(Message message) {
		MapMessage mapMessage= (MapMessage) message;
		
		// 调用SMS服务发送短信
		try {
			String telephone = mapMessage.getString("telephone");
			String msg = mapMessage.getString("msg");
			
			// 调用工具类发送短信
			// String result = SmsUtils.sendSmsByHTTP(telephone, msg);
			String result = "000/xxx";//假设成功
			
			if(result.startsWith("000")) {
				// 发送成功
				System.out.println("【bos_sms】短信发送成功,发往手机号：" + telephone + "，短信内容：" + msg);
			}else {
				// 发送失败
				throw new RuntimeException("【bos_sms】短信发送失败，信息码：" + result);
			}
		} catch (JMSException e) {
			e.printStackTrace();
		}
	}
}