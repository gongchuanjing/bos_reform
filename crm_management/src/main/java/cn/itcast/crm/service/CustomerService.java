package cn.itcast.crm.service;

import cn.itcast.crm.domain.Customer;

import javax.ws.rs.*;
import java.util.List;

/**
 * 客户操作
 * 
 * @author ChuanJing
 * @date 2017年10月12日 上午2:42:07
 * @version 1.0
 */
public interface CustomerService {

	/**
	 * 查询所有未关联客户列表
	 */
	@Path("/noassociationcustomers")
	@GET
	@Produces({"application/xml", "application/json"})
	public List<Customer> findNoAssociationCustomers();
	
	/**
	 * 已经关联到指定定区的客户列表
	 */
	@Path("/associationfixedareacustomers/{fixedareaid}")
	@GET
	@Produces({"application/xml", "application/json"})
	public List<Customer> findHasAssociationFixedAreaCustomers(@PathParam("fixedareaid") String fixedAreaId);
	
	/**
	 * 将客户关联到定区上，将所有客户id 拼成字符串 1,2,3
	 */
	@Path("/associationcustomerstofixedarea")
	@PUT
	public void associationCustomersToFixedArea(
			@QueryParam("customerIdStr") String customerIdStr,
			@QueryParam("fixedAreaId") String fixedAreaId);
	
	/**
	 * 根据电话号码查找一个客户
	 */
	@Path("/customer/telephone/{telephone}")
	@GET
	@Consumes({"application/xml", "application/json"})
	public Customer findByTelephone(@PathParam("telephone") String telephone);

	/**
	 * 根据电话号码去标记这个客户是否绑定邮箱
	 */
	@Path("/customer/updateType/{telephone}")
	@GET
	public void updateType(@PathParam("telephone") String telephone);
	
	/**
	 * 登陆
	 */
	@Path("customer/login")
	@GET
	@Consumes({"application/xml", "application/json"})
	public Customer login(@QueryParam("telephone") String telephone, @QueryParam("password") String password);

	/**
	 * 根据Address去找
	 */
	@Path("customer/findFixedAreaIdByAddress")
	@GET
	@Consumes({"application/xml", "application/json"})
	public String findFixedAreaIdByAddress(@QueryParam("address") String address);
}